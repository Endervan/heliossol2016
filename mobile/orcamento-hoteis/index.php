
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>


<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap.min.css" />
<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap-timepicker.min.css" />
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js"></script>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">



  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-empresa">
        <div class="fundo-branco">
            <h1>
              CONFIRA OS ITENS DO
              <img src="../imgs/seta-titulo.png" alt="">
            </h1>

            <h2>ORÇAMENTO</h2>

        </div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  --> 


  
  <?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){



        //  ENVIANDO A MENSAGEM PARA O CLIENTE
       $texto_mensagem = "
           O seguinte cliente fez uma solicitação pelo site. <br />

           Nome: $_POST[nome] <br />
           Email: $_POST[email] <br />
           Enderenco: $_POST[endereco] <br />
           Complemento: $_POST[complemento] <br />
           Cep: $_POST[cep] <br />
           Fax: $_POST[fax] <br />
           Email: $_POST[email] <br />
           Telefone: $_POST[telefone] <br />
           Bairro: $_POST[bairro] <br />
           Cidade: $_POST[cidade] <br />
           Estado: $_POST[estado] <br />
           Como prefere receber informação: $_POST[receber_infomacoes_heliossol] <br />
           Data agendamento: $_POST[data] <br />
           Em quanto tempo você pretende adquirir seu aquecedor solar? $_POST[Tempo_adquirir_seu_quecedor] <br />
           Como gostaria de efetuar o pagamento: $_POST[efetuar_pagamento] <br />
           Qual o estado do hotel/motel:$_POST[Estado_piscina] <br />
           Se seu hotel ou motel está em construção,em qual etapa está sua obra: $_POST[Construcao_obras] <br />
           Qual cidade está sua obra:$_POST[obras_cidade] <br />
           Tipo de edificação:$_POST[tipo_edificacao] <br />
           Tipo de cobertura:$_POST[tipo_cobertura] <br />
           Já existe ou pretende instalar tubulação para água quente:$_POST[tubulacao] <br />
           Descricao turbulacao:$_POST[Descricao_turbulacao] <br />
           Qual o padrão de pressão da rede hidráulica do hotel/motel:$_POST[padrao_pressao] <br />
           Como é a água que abastece seu hotel/motel:$_POST[abastece_hotel] <br />
           Tensão elétrica no hotel/motel:$_POST[tensao_eletrica] <br />
           Existe tubulação para água quente nos apartamentos:$_POST[exite_tubulacao] <br />
           Número de apartamentos:$_POST[Numero_apartamento] <br />
           Número de leitos::$_POST[Numero_leito] <br />
           Taxa média ocupação:$_POST[Taxa_ocupacao] por $_POST[taxa_ocupacao_media] <br />
           Existe variação significativa na ocupação em algum período do ano/semana? Se positivo, especifique:$_POST[mensagem_ocupacao] <br />
           Locais de utilização de água quente:$_POST[duchas] <br />
           Lavatórios:$_POST[lavatorios] <br />
           Número de banheiras individuais:$_POST[numeros_banheiras] <br />
           Número de banheiras duplas:$_POST[banheiras_duplas] <br />
           Aquecer água da Lavanderia:$_POST[aquecer_agua_lavandeira] <br />
           Aquecer água da cozinha:$_POST[aquecer_agua_cozinha] <br />
           Outros pontos de água quente a acrescentar:$_POST[aguas_quentes] <br />
           Tem interesse em aquecer também água em local diferente de hotel/motel? Especifique:$_POST[local_diferente_aquecer] <br />
           Outros Locais: $_POST[outros_locais] <br />
           Se tem interesse em outros produtos da linha Heliossol, por favor especifique:$_POST[interesse] <br />
           Como conheceu nosso site: <br>
           Internet-$_POST[internet] <br />
           Revista-$_POST[revista] <br />
           Indicação-$_POST[indicacao] <br />
           Outras Indicação-$_POST[indicacao_outros] <br />

           Mensagem: <br />
           ". nl2br($_POST[mensagem]) ." <br />

           ";


           Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
           Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
           Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

 }
 ?>




<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO Hoteis-->
<!--  ==============================================================  -->
<!--  ==============================================================  -->
 <!-- FORMULARIO ENVIE ORCAMENTO-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row formulario">
    <form action="" class="FormContato" id="profileForm" method="post" accept-charset="utf-8">

      <div class="col-xs-6 form-group">      
        <input type="text" name="nome"  placeholder="NOME" class="form-control input-lg">
      </div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="complemento"  placeholder="COMPLEMENTO" class="form-control input-lg">
      </div>


      <div class="col-xs-6 form-group">      
        <input type="text" name="bairro"  placeholder="BAIRRO" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="estado"  placeholder="ESTADO" class="form-control input-lg">
      </div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-lg">
      </div>

     <div class="clearfix"></div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="cep"  placeholder="CEP" class="form-control input-lg">
      </div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="col-xs-6 form-group">      
        <input type="text" name="fax"  placeholder="FAX" class="form-control input-lg">
      </div>


      <div class="col-xs-6 form-group">      
        <input type="text" name="email"  placeholder="EMAIL" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="col-xs-12 top20">
        <h2>Como você prefere receber informações da Heliossol?</h2>

        <div class="top20 form-group">
          <label class="radio-inline">
            <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio1" value="E-mail">E-mail
          </label>
          <label class="radio-inline">
            <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio2" value="Fax">Fax
          </label>
          <label class="radio-inline">
            <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio3" value="Telefone"> Telefone
          </label>
        </div>
      </div>

     <!--  ==============================================================  -->
      <!-- CALENDARIO VALIDADOR-->
      <!--  ==============================================================  -->
      <div class="clearfix"></div>
          <div class="col-xs-4 top25 text-right">
                    <h2>Melhor Horário</h2>
          </div>
            <div class="col-xs-2 top10 bootstrap-timepicker timepicker padding0 form-group">
            <input id="timepicker1" type="text" name="agenda" class="form-control input-lg">
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
              $('#timepicker1').timepicker({
                 showMeridian: false
              });

            });
        </script>
      <!--  ==============================================================  -->
      <!-- CALENDARIO VALIDADOR-->
      <!--  ==============================================================  -->

      <div class="clearfix"></div>

      <div class="col-xs-12 top20">
        <h2>Em quanto tempo você pretende adquirir seu aquecedor solar?</h2>

        <div class="top10 form-group">
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj1" value="Imediatamente">Imediatamente
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj1" value="Antecipado">1 Mês
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj2" value="2 Mês">2 Mês
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj3" value="3 Mês">3 Mês
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj3" value="6 Mês">6 Mês
          </label>

          <div class="clearfix"></div>

          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj3" value="Prazo Indiferente">Prazo Indiferente
          </label>

          <div class="clearfix"></div>

          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj3" value="Não pretendo adquirir em breve, só quero saber o preço.">Não pretendo adquirir em breve, só quero saber o preço.
          </label>
        </div>

        <div class="top20">
          <h2>Como gostaria de efetuar o pagamento?</h2>
        </div>
        <div class="top10 form-group">
          <label class="radio-inline">
            <input type="radio" name="efetuar_pagamento" id="hj3" value="Antecipado">Antecipado
          </label>

          <label class="radio-inline">
            <input type="radio" name="efetuar_pagamento" id="hj3" value="3 Vezes (ato/30/60 dias)">3 Vezes (ato/30/60 dias)
          </label>

          <div class="clearfix"></div>

          <label class="radio-inline">
            <input type="radio" name="efetuar_pagamento" id="hj3" value="4 Vezes (ato/30/60/90 dias)">4 Vezes (ato/30/60/90 dias)
          </label>

          <div class="clearfix"></div>

          <label class="radio-inline">
          <input type="radio" name="efetuar_pagamento" id="hj3" value="OUTROS">outros
          </label>

          <label class="radio-inline">
            <input type="radio" name="efetuar_pagamento" id="hj3" value="Financiamento CEF até 60 meses.">Financiamento CEF até 60 meses.
          </label>

          <div class="clearfix"></div>
        </div>

        <div class="form-group top5">
          <input type="text" name=""  placeholder="OUTROS" class="form-control input-lg">
        </div>

        <p>Ajude-nos a entender um pouco mais a necessidade de água quente em seu hotel/motel:</p>

        <div class="top10">
          <h2>Qual o estado do hotel/motel:?</h2>
        </div>

        <div class="top10 form-group">
          <label class="radio-inline">
            <input type="radio" name="Estado_piscina" id="inlineRadio8" value="Obra Acabada">Obra Acabada
          </label>
          <label class="radio-inline">
            <input type="radio" name="Estado_piscina" id="inlineRadio9" value="Em Construção">Construindo
          </label>
          <label class="radio-inline">
            <input type="radio" name="Estado_piscina" id="inlineRadio9" value="Em Reforma">Em Reforma
          </label>

        </div>

        <div class="top10">
          <h2>Se seu hotel/motel está em construção, em qual etapa está sua obra?</h2>
        </div>

        <div class="top10 form-group">
          <label class="radio-inline">
            <input type="radio" name="Construcao_obras" id="inlineRadio10" value="Fundação">Fundação
          </label>
          <label class="radio-inline">
            <input type="radio" name="Construcao_obras" id="inlineRadio11" value="Alvenaria ou Respaldo">Alvenaria ou Respaldo
          </label>
          <label class="radio-inline">
            <input type="radio" name="Construcao_obras" id="inlineRadio12" value="Cobertura">Cobertura
          </label>

          <div class="clearfix"></div>

          <label class="radio-inline">
            <input type="radio" name="Construcao_obras" id="inlineRadio12" value="Acabamento">Acabamento
          </label>
          <label class="radio-inline">
            <input type="radio" name="Construcao_obras" id="inlineRadio12" value="Concluída">Concluída
          </label>
        </div>

        <div class="top10">
          <h2>Qual cidade está sua obra?</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name="obras_cidade"  placeholder="Cidade" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Tipo de edificação:</h2>
        </div>

        <div class="top10 form-group">
           <label class="radio-inline">
            <input type="radio" name="tipo_edificacao" id="inlineRadio12" value="Térrea – um só pavimento">Térrea – um só pavimento
          </label>
           <label class="radio-inline">
            <input type="radio" name="tipo_edificacao" id="inlineRadio12" value="Sobrado – dois ou mais pavimentos">Sobrado – dois ou mais pavimentos
          </label>

          <div class="clearfix"></div>


           <label class="radio-inline">
            <input type="radio" name="tipo_edificacao" id="inlineRadio12" value="Cobertura de edifício – último pavimento">Cobertura de edifício – último pavimento
          </label>
        </div>


        <div class="top20">
          <h2>Tipo de cobertura:</h2>
        </div>

        <div class="top10 form-group">
           <label class="radio-inline">
            <input type="radio" name="tipo_cobertura" id="inlineRadio12" value="Telhado com forro ou laje">Telhado com forro ou laje
          </label>

          <div class="clearfix"></div>

           <label class="radio-inline">
            <input type="radio" name="tipo_cobertura" id="inlineRadio12" value="Telhado com torre para caixa d'água e reservatório térmico">Telhado com torre para caixa d'água e reservatório térmico
          </label>

          <div class="clearfix"></div>

           <label class="radio-inline">
            <input type="radio" name="tipo_cobertura" id="inlineRadio12" value="Telhado sem forro ou laje">Telhado sem forro ou laje
          </label>
          <label class="radio-inline">
            <input type="radio" name="tipo_cobertura" id="inlineRadio12" value="Laje apenas">Laje apenas
          </label>
        </div>

        <div class="top10">
          <h2>Já existe ou pretende instalar tubulação para água quente?</h2>
        </div>

        <div class="top10 form-group">
           <label class="radio-inline">
            <input type="radio" name="tubulacao" id="inlineRadio12" value="Não">Não
          </label>
           <label class="radio-inline">
            <input type="radio" name="tubulacao" id="inlineRadio12" value="Sim. Tubulação em cobre">Sim. Tubulação em cobre
          </label>
           <label class="radio-inline">
            <input type="radio" name="tubulacao" id="inlineRadio12" value="Sim. Tubulação em CPVC">Sim. Tubulação em CPVC
          </label>
          <div class="clearfix"></div>

          <label class="radio-inline">
            <input type="radio" name="tubulacao" id="inlineRadio12" value="Sim. Outro material:">Sim. Outro material:
          </label>
          <div class="form-group top5">
          <input type="text" name=""  placeholder="Descrição Outros Materiais" class="form-control input-lg">
        </div>
        </div>

        <div class="top10">
          <h2>Qual o padrão de pressão da rede hidráulica do hotel/motel?</h2>
        </div>

        <div class="top10">
           <div class="col-xs-12 form-group">
             <label class="radio">
            <input type="radio" name="padrao_pressao" id="inlineRadio12" value="Normal – distribuição hidráulica por gravidade a partir de caixa d'água elevada">Normal – distribuição hidráulica por gravidade a partir de caixa d'água elevada
          </label>
           <label class="radio">
            <input type="radio" name="padrao_pressao" id="inlineRadio12" value="Pressurizada – distribuição hidráulica a partir de pressurizador eletromecânico (bomba)">Pressurizada – distribuição hidráulica a partir de pressurizador eletromecânico (bomba)
          </label>
           </div>
        </div>

        <div class="top10">
          <h2>Como é a água que abastece seu hotel/motel?</h2>
        </div>

        <div class="top10 form-group">
           <label class="radio-inline">
            <input type="radio" name="abastece_hotel" id="inlineRadio12" value="Água da rede pública – concessionária de água">Água da rede pública – concessionária de água
          </label>
           <label class="radio-inline">
            <input type="radio" name="abastece_hotel" id="inlineRadio12" value="Água tratada">Água tratada
          </label>
          <div class="clearfix"></div>
          <label class="radio-inline">
            <input type="radio" name="abastece_hotel" id="inlineRadio12" value="Água de poço sem tratamento">Água de poço sem tratamento
          </label>
          <label class="radio-inline">
            <input type="radio" name="abastece_hotel" id="inlineRadio12" value="Água de mina sem tratamento">Água de mina sem tratamento
          </label>
        </div>

        <div class="top10">
          <h2>Tensão elétrica no hotel/motel:</h2>
        </div>

        <div class="top20 form-group">
           <label class="radio-inline">
            <input type="radio" name="tensao_eletrica" id="inlineRadio12" value="110 Volts">110 Volts
          </label>
           <label class="radio-inline">
            <input type="radio" name="tensao_eletrica" id="inlineRadio12" value="220 Volts">220 Volts
          </label>
          <label class="radio-inline">
            <input type="radio" name="tensao_eletrica" id="inlineRadio12" value="Sem energia elétrica">Sem energia elétrica
          </label>
        </div>

        <p>Ajude-nos a entender um pouco mais a necessidade de água quente no hotel:</p>
         <div class="top10">
          <h2>Existe tubulação para água quente nos apartamentos?</h2>
        </div>

       <div class="col-xs-6 padding0 form-group">
         <select name="exite_tubulacao" class="form-control input-lg">
         <option value="">Selecione</option>
          <option value="SIM">Sim</option>
          <option value="NAO">Não</option>
        </select>
       </div>

       <div class="clearfix"></div>

       <div class="top10">
          <h2>Número de apartamentos:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name="Numero_apartamento"  placeholder="Quantidade" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Número de leitos:</h2>
        </div>

        <div class="form-group top20">
          <input type="text" name="Numero_leito"  placeholder="Leitos" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Taxa média ocupação:</h2>
        </div>

       <div class="col-xs-4 top10 padding0">
          <div class="form-group">
        <input type="text" name="Taxa_ocupacao"  placeholder="Taxa %" class="form-control input-lg">
        </div>
      </div>

       <div class="col-xs-8 form-group">
         <label class="radio left10">
          <input type="radio" name="taxa_ocupacao_media" id="inlineRadio12" value="Por apartamento">Por apartamento
        </label>
         <label class="radio left10">
          <input type="radio" name="taxa_ocupacao_media" id="inlineRadio12" value="Por leito">Por leito
        </label>
       </div>

      <div class="clearfix"></div>

       <div class="top10">
          <h2>Existe variação significativa na ocupação em algum período do ano/semana? Se positivo, especifique:</h2>
        </div>

        <div class="col-xs-12 form-group top5 bottom10 padding0">
          <textarea name=""  cols="30" rows="9" class="form-control input100"></textarea>
        </div>

        <p>Locais de utilização de água quente:</p>

        <div class="top5">
          <h2>Duchas:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name="duchas"  placeholder="tipos" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Lavatórios:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name="lavatorios"  placeholder="tipos" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Número de banheiras individuais:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name="numeros_banheiras"  placeholder="Quantidades" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Número de banheiras duplas:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name="banheiras_duplas"  placeholder="Quantidades" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Aquecer água da Lavanderia:</h2>
        </div>

       <div class="col-xs-4 padding0 top5 form-group">
         <select name="aquecer_agua_lavandeira" class="form-control input-lg">
         <option value="">Selecione</option>}
          <option value="SIM">Sim</option>
          <option value="NAO">Não</option>
        </select>
       </div>

        <div class="clearfix"></div>

       <div class="top10">
          <h2>Aquecer água da cozinha:</h2>
        </div>

       <div class="col-xs-4 padding0 top5 form-group">
         <select name="aquecer_agua_cozinha" class="form-control input-lg">
          <option value="">Selecione</option>}
          <option>Sim</option>
          <option>Não</option>
        </select>
       </div>

        <div class="clearfix"></div>

       <div class="top10">
          <h2>Outros pontos de água quente a acrescentar:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name=""  placeholder="Especifique Outros pontos" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Tem interesse em aquecer também água em local diferente de hotel/motel? Especifique:</h2>
        </div>
        <div class="top10 form-group">
           <label class="radio-inline">
            <input type="radio" name="local_diferente_aquecer" id="inlineRadio12" value="Residência">Residência
          </label>
           <label class="radio-inline">
            <input type="radio" name="local_diferente_aquecer" id="inlineRadio12" value="Piscina">Piscina
          </label>
          <label class="radio-inline">
            <input type="radio" name="local_diferente_aquecer" id="inlineRadio12" value="Hospital">Hospital
          </label>
          <div class="clearfix"></div>
          <label class="radio-inline">
            <input type="radio" name="local_diferente_aquecer" id="inlineRadio12" value="Edifício residencial a construir">Edifício residencial a construir
          </label>
          <div class="clearfix"></div>
          <label class="radio-inline">
            <input type="radio" name="local_diferente_aquecer" id="inlineRadio12" value="Edifício residencial em construção">Edifício residencial em construção
          </label>
        </div>

        <div class="top10">
          <h2>Outro Local:</h2>
        </div>

        <div class="form-group top5">
          <input type="text" name=""  placeholder="Outros locais" class="form-control input-lg">
        </div>

        <div class="top10">
          <h2>Se tem interesse em outros produtos da linha Heliossol, por favor especifique:</h2>
        </div>

        <div class="form-group top10">
          <input type="text" name=""  placeholder="Especifique" class="form-control input-lg">
        </div>

       <p>Como você chegou ao site da Heliossol?</p>

        <div class="col-xs-12  subtitulo-formulario">
          <div class="top10">
            <h2>Internet</h2>
          </div>
          <div class="top10 form-group">
            <label class="radio-inline">
              <input type="radio" name="internet" id="inlineRadio40" value="Mecanismo de busca na Internet">Mecanismo de busca na Internet
            </label>
            <label class="radio-inline">
              <input type="radio" name="internet" id="inlineRadio19" value="Link Site da Internet">Link Site da Internet
            </label>
          </div>

          <div class="top10">
            <h2>Revista</h2>
          </div>
          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="revista" id="inlineRadio20" value="Arquitetura e Construção">Arquitetura e Construção
            </label>
            <label class="radio-inline">
              <input type="radio" name="revista" id="inlineRadio21" value="Outra revista">Outra revista
            </label>
          </div>


          <div class="top10">
            <h2>Indicação</h2>
          </div>
          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="indicacao" id="inlineRadio22" value="Amigo">Amigo
            </label>
            <label class="radio-inline">
              <input type="radio" name="indicacao" id="inlineRadio23" value="Vendedor">Vendedor
            </label>
            <label class="radio-inline">
              <input type="radio" name="indicacao" id="inlineRadio24" value="Construtor">Construtor
            </label>
            <div class="clearfix"></div>
            <label class="radio-inline">
              <input type="radio" name="indicacao" id="inlineRadio25" value="Engenheiro / Arquiteto">Engenheiro / Arquiteto
            </label>
          </div>

          <div class="top10">
            <h2>Outros Indicação</h2>
          </div>
          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="indicacao_outros" id="inlineRadio26" value="Correio Braziliense">Correio Braziliense
            </label>
            <label class="radio-inline">
              <input type="radio" name="indicacao_outros" id="inlineRadio27" value="Jornal do Encanador">Jornal do Encanador
            </label>
            <label class="radio-inline">
              <input type="radio" name="indicacao_outros" id="inlineRadio28" value="Outro jornal">Outro jornal
            </label>
            <div class="clearfix"></div>
            <label class="radio-inline">
              <input type="radio" name="indicacao_outros" id="inlineRadio29" value="Catálogo">Catálogo
            </label>
            <label class="radio-inline">
              <input type="radio" name="indicacao_outros" id="inlineRadio30" value="Feira">Feira
            </label>
          </div>
          <div class="form-group top20">
            <input type="text" name="outros1"  placeholder="Outra Indicações" class="form-control input-lg">
          </div>
        </div>

        <div class="top20">
          <h2>Este espaço é para você colocar sua dúvida ou comentário:</h2>
        </div>

        <div class="col-xs-12 form-group top20 padding0">
          <textarea name="mensagem"  cols="30" rows="13" class="form-control input100"></textarea>
        </div>

        <div class="text-right top30 bottom25">
          <button type="submit" class="btn btn-formulario" name="btn_contato">
            ENVIAR
          </button>
        </div>

      </div>




    </form>



  </div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO hoteis-->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>






<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      
   
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
     
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
     
   
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      }

    }
  });
});
</script>
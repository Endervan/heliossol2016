
<div class="topo-site">

  <!--  ==============================================================  -->
  <!-- telefones topo-->
  <!--  ==============================================================  -->
  <div class="container top10">
    <div class="row">
      <div class="col-xs-12">
        <div class="pull-right">
          <a class="btn btn-chamar" href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
            CHAMAR
          </a>
        </div>
        <div class="pull-right">
          <div class="telefones">
            <h3><?php Util::imprime($config[telefone1]); ?></h3>
          </div>
        </div>

      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- telefones topo-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- menu-->
  <!--  ==============================================================  -->
  <div class="container top10">
    <div class="row">
      <div class="col-xs-12">
        <div class="menu-topo  effect2">

          <div class="col-xs-3 posicao-img padding0">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
            </a>
          </div>

          <!-- menu-topo -->
          <div class="col-xs-5 padding0">
            <div class=" dropdown top10">
              <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-menu">
                <i class="fa fa-bars right5"></i>
                NOSSO MENU
                <i class="fa fa-chevron-down left5"></i>
              </a>
              <ul class="dropdown-menu sub-menu" aria-labelledby="dLabel">
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/noticias">NOTÍCIAS</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">FALE CONOSCO</a></li>
                <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a></li>
              </ul>
            </div>
          </div>
          <!-- menu-topo -->


          <!-- botao pesquisa -->
          <div class=" col-xs-2 padding0 dropdown">
            <a  class="dropdown-toggle btn btn-amarelo-escuro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-search right10"></i>
              <span class="caret"></span>
            </a>

            <ul class="dropdown-menu form-busca-topo pull-right">
              <form class="navbar-form navbar-left "  method="post" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" role="search">
                <div class="form-group col-xs-8 pull-left">
                  <input type="text" class="form-control" name="busca_topo" placeholder="O que está procurando?">
                </div>
                <button type="submit" class="btn btn-amarelo">Buscar</button>
              </form>
            </ul>
          </div>
          <!-- botao pesquisas -->


          <!-- ======================================================================= -->
          <!-- botao carrinho de compra -->
          <!-- ======================================================================= -->
          <div class=" col-xs-2 ">
            <a href="#" class="dropdown-toggle btn btn-amarelo-escuro" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
              <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-carrinho.png" alt="">
            </a>

            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">
              <?php

              if(count($_SESSION[solicitacoes_produtos]) > 0)
              {
                echo '<h6 class="bottom20">MEUS PRODUTOS('. count($_SESSION[solicitacoes_produtos]) .')</h6>';

                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <div class="lista-itens-carrinho col-xs-12 top10">
                    <div class="col-xs-2">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-produtos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                    </div>
                  </div>
                  <?php
                }
              }

              ?>

              <?php

              if(count($_SESSION[solicitacoes_servicos]) > 0)
              {

                echo '<h6 class="bottom20">MEUS SERVIÇOS('. count($_SESSION[solicitacoes_servicos]) .')</h6>';

                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>
                  <div class="lista-itens-carrinho col-xs-12 top10">
                    <div class="col-xs-2">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                    </div>
                    <div class="col-xs-8">
                      <h1><?php Util::imprime($row[titulo]) ?></h1>
                    </div>
                    <div class="col-xs-1">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-produtos/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                    </div>
                  </div>
                  <?php
                }
              }

              ?>

              <div class="text-right bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-produtos" title="Finalizar" class="btn btn-amarelo" >
                  <h3>FINALIZAR</h3>
                </a>
              </div>
            </div>
          </div>
          <!-- ======================================================================= -->
          <!-- botao carrinho de compra -->
          <!-- ======================================================================= -->


        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- menu-->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!-- menu-->
  <!--  ==============================================================  -->

  <!-- menu-topo -->




</div>

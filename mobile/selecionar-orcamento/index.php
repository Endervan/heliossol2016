<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<body class="orcamento-piscinas">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-noticias descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-piscinas">
        <div class="fundo-branco"></div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-noticias descricao-->
  <!--  ==============================================================  --> 

  <!--  ==============================================================  -->
  <!-- TIPOS DE ORCAMENTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row  ">
     <div class="col-xs-12">
       <div class="tipos-orcamentos pb40">
        <div class="col-xs-6 text-center top25">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-residencia">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-orcamento01.png" alt="">
          </a>
          <div class="top20">
            <h1>ORÇAMENTOS PARA RESIDÊNCIAS EM CONSTRUÇÃO OU CONCLUÍDAS</h1>
          </div>
        </div>

        <div class="col-xs-6 text-center top25">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-construcao">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-orcamento02.png" alt="">
          </a>
          <div class="top20">
            <h1>ORÇAMENTOS PARA HOTÉIS EM CONSTRUÇÃO OU CONCLUÍDOS</h1>
          </div>
        </div>  

        <div class="clearfix"></div>

        <div class="col-xs-6 text-center top25">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-piscinas">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-orcamento03.png" alt="">
          </a>
          <div class="top20">
            <h1>ORÇAMENTOS PARA PISCINAS</h1>
          </div>
        </div>  


         <?php /*
        <div class="col-xs-6 text-center top25">
          <a href="">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-orcamento04.png" alt="">
          </a>
          <div class="top20">
            <h1>ORÇAMENTOS PARA PRODUTOS SELECIONADOS</h1>
          </div>
        </div>  
        */ ?>


          
      </div>
    </div>
  </div>
</div> 
<!--  ==============================================================  -->
  <!-- TIPOS DE ORCAMENTOS-->
  <!--  ==============================================================  --> 



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>



  <!-- FlexSlider 2 -->
  <script type="text/javascript" >
   $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: false,
      itemWidth: 113,
      itemMargin: 1
    });
  });
</script>
<!-- FlexSlider 2 -->

</head>

<body>

  <?php require_once('./includes/topo.php'); ?>


  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row slider-index">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">


        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">



            <?php
            $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
             if(mysql_num_rows($result) > 0){
              $i = 0;
               while ($row = mysql_fetch_array($result)) {
               ?>
                  <div class="item <?php if($i == 0){ echo "active"; } ?>">

                      <?php if (!empty($row[url])): ?>
                            <a href="<?php Util::imprime($row[url]); ?>" title="">
                              <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="">
                            </a>
                      <?php else: ?>
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="">
                      <?php endif; ?>
                </div>

               <?php
               $i++;
               }
             }
             ?>



            </div>


          <!-- Controls -->
          <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>
</div>
</div>
<!-- ======================================================================= -->
<!-- slider -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- CATEGORIAS HOME -->
<!-- ======================================================================= -->
<div class="container ">
  <div class="row">
    <div class="col-xs-12 lista-categorias">
      <div id="slider" class="flexslider">
        <ul class="slides slider-prod ">


          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
          ?>
              <li class="<?php if($i++ % 2 == 0){ echo 'categorias'; }else{ echo 'categorias1'; } ?>">
                 <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                   <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
                   <h2><?php Util::imprime($row[titulo]); ?></h2>
                 </a>
               </li>
          <?php
            }
          }
          ?>



       </ul>
     </div>
   </div>
 </div>
</div>
<!-- ======================================================================= -->
<!-- CATEGORIAS HOME -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- produto home-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">

    <?php
    $result = $obj_site->select("tb_produtos", "order by rand() limit 6");
    if(mysql_num_rows($result) == 0){
       echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
    }else{
      $i = 0;
      while ($row = mysql_fetch_array($result)) {
      ?>
        <div class="col-xs-6">
          <div class="thumbnail produtos-home">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 190, 105, array("class"=>"", "alt"=>"$row[titulo]")) ?>
            </a>
            <div class="caption">
             <h1><?php Util::imprime($row[titulo]); ?></h1>
           </div>
         </div>
       </div>
      <?php
        if($i == 1){
            echo '<div class="clearfix"></div>';
            $i = 0;
        }else{
            $i++;
        }
      }
    }
    ?>




  <div class="clearfix"></div>

  </div>
</div>
<!--  ==============================================================  -->
<!-- produto home-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- SOBRE HELIOSSOL-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <div class="bg-sobre"></div>
    <div class="col-xs-12">
        <div class="top25">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
          <p><?php Util::imprime($row[descricao], 4000); ?></p>
       </div>
       <div class="top10">
         <a class="btn  btn-transparente right25"
         href="<?php echo Util::caminho_projeto() ?>/mobile/empresa" title="MAIS SOBRE">
         MAIS SOBRE
       </a>
       <a class="btn  btn-transparente"
       href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco" title="COMO CHEGAR">
       COMO CHEGAR
     </a>
       </div>

    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- SOBRE HELIOSSOL-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- dicas home-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row top30">

   <div class="col-xs-12 text-right">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bg-nossas-dicas.png" alt="">
    </div>


    <?php
    $result = $obj_site->select("tb_noticias", "order by rand() limit 2");
    if (mysql_num_rows($result) > 0) {
      while ($row = mysql_fetch_array($result)) {
      ?>
      <!-- item 01 -->
        <div class="col-xs-6 top10">
          <div class="thumbnail dicas-home">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 210, 152, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

              <div class="caption">
                 <h1><?php Util::imprime($row[titulo]); ?></h1>
              </div>
            </a>
        </div>
      </div>
      <?php
      }
    }
    ?>

    <div class="clearfix"></div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- dicas home-->
<!--  ==============================================================  -->






<?php require_once('./includes/rodape.php'); ?>





</body>

</html>

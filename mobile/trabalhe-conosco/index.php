<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<body class="bg-fale-conosco">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!-- FORMULARIO CONTATOS E ENDERENCO-->
  <!--  ==============================================================  -->
  <div class="container top270">
    <div class="row contatos">

      <div class="col-xs-7">
        <div class="position-imagem"></div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
         <li  class="trabalhe-conosco" role="presentation">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/fale-conosco">
              <span class="right50"><img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-fale.png" alt=""></span>
              FALE CONOSCO
            </a>
          </li>

           <li role="presentation" class="active fale-conosco top10">
            <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
            <span class="right10"><img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-trabalhe.png" alt=""></span>
              TRABALHE CONOSCO
            </a>
          </li>

        </ul>
      </div>

      <div class="clearfix"></div>


       <!-- contatos -->
      <div class="top15">
        <div class="col-xs-6">
          <div class="pull-right">
            <a class="btn btn-chamar-fale" href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
              CHAMAR
            </a>
          </div>
          <div class="pull-right">
            <div class="telefones-fale">
              <h3><?php Util::imprime($config[telefone1]); ?></h3>
            </div>
          </div>
        </div>



        <?php if (!empty($config[telefone2])): ?>
        <div class="col-xs-6">
          <div class="pull-right">
            <a class="btn btn-chamar-fale" href="tel:+55<?php Util::imprime($config[telefone2]); ?>">
              CHAMAR
            </a>
          </div>
          <div class="pull-right">
            <div class="telefones-fale">
              <h3><?php Util::imprime($config[telefone2]); ?></h3>
            </div>
          </div>

        </div>
        <?php endif ?>

          
        <?php if (!empty($config[telefone3])): ?>
        <div class="col-xs-6 top10">
          <div class="pull-right">
            <a class="btn btn-chamar-fale" href="tel:+55<?php Util::imprime($config[telefone3]); ?>">
              CHAMAR
            </a>
          </div>
          <div class="pull-right">
            <div class="telefones-fale">
              <h3><?php Util::imprime($config[telefone3]); ?></h3>
            </div>
          </div>

        </div>
        <?php endif ?>
        

        <?php if (!empty($config[telefone4])): ?>
        <div class="col-xs-6 top10">
          <div class="pull-right">
            <a class="btn btn-chamar-fale" href="tel:+55<?php Util::imprime($config[telefone4]); ?>">
              CHAMAR
            </a>
          </div>
          <div class="pull-right">
            <div class="telefones-fale">
              <h3><?php Util::imprime($config[telefone4]); ?></h3>
            </div>
          </div>

        </div>
        <?php endif ?>  




        </div>

      
      <div class="clearfix"></div>  

      <div class="col-xs-8 top15">

        <div class="media">
          <div class="media-left media-middle">
            <a href="#">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-home-fale.jpg" alt="">
            </a>
          </div>
          <div class="media-body media-middle ">
           <p></i><?php Util::imprime($config[endereco]); ?></p>
         </div>
       </div>

       </div> 
    
      <div class="col-xs-4 top15 text-right">
        <a class="btn btn-amarelo-fale" href="#bottom">COMO CHEGAR

        </a>
      </div>
    <!-- contatos -->
<?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){



        //  ENVIANDO A MENSAGEM PARA O CLIENTE
         $texto_mensagem = "
           O seguinte cliente fez uma solicitação pelo site. <br />

           Nome: $_POST[nome] <br />
           Email: $_POST[email] <br />
           Telefone: $_POST[telefone] <br />
           ESCOLARIDADE:$_POST[escolaridade] <br />
           Cardo:$_POST[cargo] <br />
           Aréa:$_POST[area] <br />
           CIDADE:$_POST[cidade] <br />
           Estado:$_POST[estado] <br />
           CURRÍCULO:$_POST[curriculo] <br />
           Mensagem: <br />
           ". nl2br($_POST[mensagem]) ." <br />

           ";

           Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
           Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
           Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

 }
 ?>



    <!-- Tab panes -->
    <div class="tab-content">

      <!-- fale conosco -->
        <form class="form-inline FormContato bottom25" role="form" method="post" enctype="multipart/form-data">

            <div class="clearfix"></div>  
            <div class="fundo-formulario top20">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="nome" class="form-control fundo-form1  input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top15"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="email" class="form-control fundo-form1  input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top15"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div> 

              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="telefone" class="form-control fundo-form1  input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top15"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="escolaridade" class="form-control fundo-form1  input-lg" placeholder="ESCOLARIDADE">
                    <span class="glyphicon glyphicon-book form-control-feedback"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div> 

              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group has-feedback">
                    <input type="text" name="cargo" class="form-control fundo-form1  input-lg" placeholder="CARGO" >
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="area" class="form-control fundo-form1  input-lg" placeholder="AREA">
                    <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>        
                  </div>
                </div>
              </div>

               <div class="clearfix"></div> 

              <div class="top20">

                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="cidade" class="form-control fundo-form1  input-lg" placeholder="CIDADE">
                    <span class="glyphicon glyphicon-globe form-control-feedback"></span>        
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group has-feedback ">
                    <input type="text" name="estado" class="form-control fundo-form1  input-lg" placeholder="ESTADO">
                    <span class="glyphicon glyphicon-globe form-control-feedback"></span>        
                  </div>
                </div>

              </div>

        
              <div class="clearfix"></div>

               <div class="top20">
                <div class="col-xs-12">
                  <div class="form-group has-feedback ">
                    <input type="file" name="curriculo" class="form-control fundo-form1  input-lg" placeholder="CURRÍCULO">
                    <span class="glyphicon glyphicon-file form-control-feedback"></span>        
                  </div>
                </div>
                </div>

              <div class="clearfix"></div>        
       
              <div class="top15">
                <div class="col-xs-12">        
                 <div class="form-group has-feedback">
                  <textarea name="mensagem1" cols="56" rows="11" class="form-control fundo-form1" placeholder="ASSUNTO"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span> 
                </div>
              </div>
            </div>
            <!-- formulario orcamento -->

    
          <!--  ==============================================================  -->
          <!-- bg-como chegar-->
          <!--  ==============================================================  -->
          <div class="col-xs-7 top30">
            <div class="bg-como-chegar">
              <h2>SAIBA COMO</h2>
              <h3>CHEGAR</h3>
            </div>
          </div>

          <div class="col-xs-5 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

           </div>
          
          <!--  ==============================================================  -->
          <!-- FORMULARIO CONTATOS E ENDERENCO-->
          <!--  ==============================================================  -->
      </form>
    <!-- fale conosco -->
  </div>
  <!-- Tab panes -->
</div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO CONTATOS E ENDERENCO-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container top50">
  <div class="row">
   <div class="col-xs-12">
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="446" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>
</div>


<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<!-- ======================================================================= -->
<!-- SCROLL ANIMATE   -->
<!-- ======================================================================= -->
<script type="text/javascript">
  $("a[href='#bottom']").click(function() {
  $("html, body").animate({ scrollTop: $(document).height() }, 2000);
  return false;
});
</script>
<!-- ======================================================================= -->
<!-- SCROLL ANIMATE   -->
<!-- ======================================================================= -->


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem1: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();



// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 19);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}



?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>

<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap.min.css" />
<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap-timepicker.min.css" />
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js"></script>

</head>

<body class="orcamento-piscinas">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-piscina descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-piscinas">
        <div class="fundo-branco"></div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-piscina descricao-->
  <!--  ==============================================================  --> 



<form action="" class="FormContato" id="profileForm" method="post" accept-charset="utf-8">

  <?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){

    //  CADASTRO OS PRODUTOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd]); $i++){
      $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

      $mensagem .= "
      <tr>
        <td><p>". $_POST[qtd][$i] ."</p></td>
        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
          $itens .= "
                      <tr>
                        <td><p>". $_POST[qtd][$i] ."</p></td>
                        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                      </tr>
                      ";
    }



      //  CADASTRO OS SERVICOS SOLICITADOS
      for($i=0; $i < count($_POST[qtd_servico]); $i++)
      {         
          $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
          
          $itens .= "
                      <tr>
                          <td><p>". $_POST[qtd_servico][$i] ."</p></td>
                          <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                       </tr>
                      ";
      }
      


    if (count($_POST[categorias]) > 0) {
      foreach($_POST[categorias] as $cat){
        $desc_cat .= $cat . ' , ';
      }
    }


        //  ENVIANDO A MENSAGEM PARA O CLIENTE

     $texto_mensagem = "
                        O seguinte cliente fez uma solicitação pelo site. <br />

                        Nome: $_POST[nome] <br />
                        Email: $_POST[email] <br />
                        Telefone: $_POST[telefone] <br />
                        Celular: $_POST[celular] <br />
                        Endereço: $_POST[endereco] <br />
                        Complemento: $_POST[complemento] <br />
                        Bairro: $_POST[bairro] <br />
                        Cidade: $_POST[cidade] <br />
                        Estado: $_POST[estado] <br />
                        CEP: $_POST[cep] <br />
                        Como você chegou ao site da Heliossol: $_POST[internet] <br />
                        Revista: $_POST[revista] <br />
                        Indicação: $_POST[indicacao] <br />
                        Outras indicações: $_POST[indicacao_outros] <br />
                        Outros: $_POST[outros] <br />

                        Mensagem: <br />
                        ". nl2br($_POST[mensagem]) ." <br />

                        <br />
                        <h2> Produtos selecionados:</h2> <br />

                        <table width='100%' border='0' cellpadding='5' cellspacing='5'>
                          <tr>
                            <td><h4>QTD</h4></td>
                            <td><h4>DESCRIÇÃO</h4></td>
                          </tr>
                          $itens
                        </table>
                        ";



    Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

  }
  ?>
 


 <!--  ==============================================================  -->
  <!-- carrinho-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">

      <div class="col-xs-12 tb-lista-itens">
        <table class="table">

         <thead>
          <tr>
          <th>ITEM</th>
            <th>PRODUTO</th>
            <th class="text-center">QUANTIDADE</th>
            <th class="text-center">DELETAR</th>
          </tr>
        </thead>

        <tbody>

          <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0){
            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
              
                    <tr>
                      <td>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 153, 92, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      </td>
                      <td align="left"><?php Util::imprime($row[titulo]) ?></td>
                      <td class="">
                          <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                          <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                      </td>
                      <td class="text-center">
                        <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                          <i class="fa fa-times-circle fa-2x"></i>
                        </a>
                      </td>
                    </tr>
                  
            <?php 
            }
          }
          ?>
          

          <?php
          if(count($_SESSION[solicitacoes_servicos]) > 0){
            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
              
                    <tr>
                      <td>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 153, 92, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      </td>
                      <td align="left"><?php Util::imprime($row[titulo]) ?></td>
                      <td class="">
                          <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                          <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                      </td>
                      <td class="text-center">
                        <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                          <i class="fa fa-times-circle fa-2x"></i>
                        </a>
                      </td>
                    </tr>
                  
            <?php 
            }
          }
          ?>
            </tbody>

      </table>



      <div class=" top15">

        <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="Continuar orçando" class="btn btn-laranja1">
          Continuar orçando
        </a>
      </div>

    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- carrinho -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO produtos-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row formulario bottom30">
    <div class="top20">


       

        <div class="col-xs-6 form-group">      
          <input type="text" name="nome"  placeholder="NOME" class="form-control input-lg">
        </div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-lg">
        </div>

         <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="complemento"  placeholder="COMPLEMENTO" class="form-control input-lg">
        </div>

        

        <div class="col-xs-6 form-group">      
          <input type="text" name="bairro"  placeholder="BAIRRO" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="estado"  placeholder="ESTADO" class="form-control input-lg">
        </div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="cep"  placeholder="CEP" class="form-control input-lg">
        </div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="celular"  placeholder="CELULAR" class="form-control input-lg">
        </div>



        <div class="col-xs-6 form-group">      
          <input type="text" name="email"  placeholder="EMAIL" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>



        <div class="col-xs-12 ">
          
         

         


          <div class="top20">
            <h2>Como você chegou ao site da Heliossol?</h2>
          </div>

          <div class="col-xs-12  subtitulo-formulario">
            <div class="top10">
              <h2>Internet</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="internet" id="inlineRadio40" value="Mecanismo de busca na Internet">Mecanismo de busca na Internet
              </label>
              <label class="radio-inline">
                <input type="radio" name="internet" id="inlineRadio19" value="Link Site da Internet">Link Site da Internet
              </label>
            </div>

            <div class="top10">
              <h2>Revista</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="revista" id="inlineRadio20" value="Arquitetura e Construção">Arquitetura e Construção
              </label>
              <label class="radio-inline">
                <input type="radio" name="revista" id="inlineRadio21" value="Outra revista">Outra revista
              </label>
            </div>


            <div class="top10">
              <h2>Indicação</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio22" value="Amigo">Amigo
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio23" value="Vendedor">Vendedor
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio24" value="Construtor">Construtor
              </label>

              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio25" value="Engenheiro / Arquiteto">Engenheiro / Arquiteto
              </label>
            </div>

            <div class="top10">
              <h2>Outros Indicação</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio26" value="Correio Braziliense">Correio Braziliense
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio27" value="Jornal do Encanador">Jornal do Encanador
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio28" value="Outro jornal">Outro jornal
              </label>

              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio29" value="Catálogo">Catálogo
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio30" value="Feira">Feira
              </label>
            </div>
            <div class="form-group top10">
              <input type="text" name="outros"  placeholder="Outros:" class="form-control input-lg">
            </div>
          </div>

          <div class="top10">
            <h2>Este espaço é para você colocar sua dúvida ou comentário:</h2>
          </div>

          <div class="col-xs-12 form-group top5 padding0">
            <textarea name="mensagem"  cols="30" rows="13" class="form-control input100"></textarea>
          </div>

          <div class="text-right top30 bottom25">
            <button type="submit" class="btn btn-formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>

        </div>
        





    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO-->
<!--  ==============================================================  -->

</form>

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      celular: {
        validators: {
          notEmpty: {

          }
        }
      },
      
   
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
     
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
     
   
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      }

    }
  });
});
</script>




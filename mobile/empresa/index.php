<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  
  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-empresa">
        <div class="fundo-branco">
            <h1>
              CONHEÇA MAIS
              <img src="../imgs/seta-titulo.png" alt="">
            </h1>

            <h2>A EMPRESA</h2>

        </div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  --> 


  
  <!--  ==============================================================  -->
  <!--  DESCRICAO EMPRESA-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row ">
      <div class="col-xs-12">
      <div class="descricao-empresa pb25">
        <div class="col-xs-12 top10 bottom15">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2) ?>
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>




        <div class="col-xs-12">

         <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
         <?php if (!empty($row[descricao])): ?>
           <div class="media top20">
             <div class="media-left media-middle">
               <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-trabalho.png" alt="">
             </div>
             <div class="media-body">
               <h1><?php Util::imprime($row[descricao]); ?></h1>
             </div>
           </div>
          <?php endif; ?>

         <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
         <?php if (!empty($row[descricao])): ?>
           <div class="media top20">
             <div class="media-left media-middle">
               <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-especializacao.png" alt="">
             </div>
             <div class="media-body">
               <h1><?php Util::imprime($row[descricao]); ?></h1>
             </div>
           </div>
          <?php endif; ?>

         <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
         <?php if (!empty($row[descricao])): ?>
           <div class="media top20">
             <div class="media-left media-middle">
               <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-construcao.png" alt="">
             </div>
             <div class="media-body">
               <h1><?php Util::imprime($row[descricao]); ?></h1>
             </div>
           </div>
          <?php endif; ?>

       </div>




       <div class="col-xs-12 top30">

         <h1><span>Heliossol Energia Para a Vida!</span></h1>
       </div>

       <div class="col-xs-12 top40"> 
         <div class="media-left media-middle">
           <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon-telefone1.png" alt=".">
         </div>
         <div class="media-body">
           <h1>ATENDIMENTO:</h1>
         </div>       
         <div class="top15">
           <h3>
              <?php 
              if (!empty($config[telefone1])): 
                   Util::imprime($config[telefone1]); 
              endif;

              if (!empty($config[telefone2])): 
                   Util::imprime(" / ".$config[telefone2]); 
              endif; 

              if (!empty($config[telefone3])): 
                   Util::imprime(" <br /> ".$config[telefone3]); 
              endif; 

              if (!empty($config[telefone4])): 
                   Util::imprime(" / ".$config[telefone4]); 
              endif; 

              ?>
           </h3>
         </div>
         <a class="btn btn-amarelo1 top20" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos"  title="SOLICITE UM ORÇAMENTO">SOLICITE UM ORÇAMENTO</a>
       </div>


      </div> 
     </div>

   </div>
 </div>  
 <!--  ==============================================================  -->
 <!-- DESCRICAO EMPRESA-->
 <!--  ==============================================================  -->


 <!--  ==============================================================  -->
 <!-- GALERIA EMPRESA-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row top40">
    <div class="col-xs-12 ">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/titulo-empresa.png" alt="">
    </div>
    <div class="col-xs-12 top20 text-center empresa-carroucel">
      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
         
          <?php
          $result = $obj_site->select("tb_galeria_empresa", "and id_empresa = 1 ");
           if(mysql_num_rows($result) > 0){
            $i = 0;
             while ($row = mysql_fetch_array($result)) {
             ?> 
                <div class="item <?php if($i == 0){ echo "active"; } ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 314, 314, array("class"=>"img-circle", "alt"=>"")) ?>
                </div>
             <?php 
              $i++;
             }
          }
          ?>

          
        </div>

        <!-- Controls -->
        <a class="left carousel-control controles-empresa" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control controles-empresa" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div> 
<!--  ==============================================================  -->
<!-- GALERIA EMPRESA-->
<!--  ==============================================================  --> 


  
 




  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>
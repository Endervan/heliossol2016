
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 20) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">



  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-empresa">
        <div class="fundo-branco">
            <h1>
              SELECIONE O TIPO DE ORÇAMENTO
              <img src="../imgs/seta-titulo.png" alt="">
            </h1>

            <h2>DESEJADO</h2>

        </div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  --> 


  
  

  
  <!--  ==============================================================  -->
  <!-- TIPOS DE ORCAMENTOS-->
  <!--  ==============================================================  -->
  <div class="container bottom70">
    <div class="row tipos-orcamentos">

    
      <div class="col-xs-6 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-residencia" title="ORÇAMENTOS PARA RESIDÊNCIAS EM CONSTRUÇÃO OU CONCLUÍDAS">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento01.png" alt="">
          <div class="top20">
            <h1>RESIDÊNCIAS EM CONSTRUÇÃO OU CONCLUÍDAS</h1>
          </div>
        </a>
      </div>

      <div class="col-xs-6 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-hoteis">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento02.png" alt="">
        
          <div class="top20">
            <h1>ORÇAMENTOS PARA HOTÉIS EM CONSTRUÇÃO OU CONCLUÍDOS</h1>
          </div>
        </a>
      </div>  

      <div class="col-xs-6 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-piscinas" title="ORÇAMENTOS PARA PISCINAS">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento03.png" alt="">
          <div class="top20">
            <h1>ORÇAMENTOS PARA PISCINAS</h1>
          </div>
        </a>
      </div>  



      <div class="col-xs-6 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento-produtos" title="ORÇAMENTOS PARA PRODUTOS SELECIONADOS">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento04.png" alt="">
          <div class="top20">
            <h1>ORÇAMENTOS PARA PRODUTOS SELECIONADOS</h1>
          </div>
        </a>
      </div>
     
      


    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- TIPOS DE ORCAMENTOS-->
  <!--  ==============================================================  --> 
  
















<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipoPessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      outros_locais: {
        validators: {
          notEmpty: {

          }
        }
      },
      produtos_diferentes: {
        validators: {
          notEmpty: {

          }
        }
      },
      tubulacao: {
        validators: {
          notEmpty: {

          }
        }
      },
      aguas_quentes: {
        validators: {
          notEmpty: {

          }
        }
      },
      duchas: {
        validators: {
          notEmpty: {

          }
        }
      },
      banheiras_duplas: {
        validators: {
          notEmpty: {

          }
        }
      },
      lavatorios: {
        validators: {
          notEmpty: {

          }
        }
      },
      numeros_banheiras: {
        validators: {
          notEmpty: {

          }
        }
      },
      banheiras: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      outros1: {
        validators: {
          notEmpty: {

          }
        }
      },
      Local: {
        validators: {
          notEmpty: {

          }
        }
      },
      Descricao_turbulacao: {
        validators: {
          notEmpty: {

          }
        }
      },
      Estado_piscina: {
        validators: {
          notEmpty: {

          }
        }
      },
      Numero_apartamento: {
        validators: {
          notEmpty: {

          }
        }
      },
      local_diferente_aquecer: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem_ocupacao: {
        validators: {
          notEmpty: {

          }
        }
      },
      Numero_leito: {
        validators: {
          notEmpty: {

          }
        }
      },
      Taxa_ocupacao: {
        validators: {
          notEmpty: {

          }
        }
      },
      interesse: {
        validators: {
          notEmpty: {

          }
        }
      },
      Comprimento: {
        validators: {
          notEmpty: {

          }
        }
      },
      largura: {
        validators: {
          notEmpty: {

          }
        }
      },
      obras_cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      Maiorprofundidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      Menorprofundidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      AreaTotal: {
        validators: {
          notEmpty: {

          }
        }
      },
      VolumeTotal: {
        validators: {
          notEmpty: {

          }
        }
      },
      outros: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereço: {
        validators: {
          notEmpty: {

          }
        }
      },
      complemento: {
        validators: {
          notEmpty: {

          }
        }
      },
      data: {
        validators: {
          date: {
            format: 'MM/DD/YYYY h:m A',
            message: 'The value is not a valid date'
          }
        }
      },

      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      cep: {
        validators: {
          notEmpty: {

          }
        }
      },
      fax: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>



<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 19);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 21) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-piscina descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-piscinas">
        <div class="fundo-branco"></div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-piscina descricao-->
  <!--  ==============================================================  --> 

  



  <?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
    if(isset($_POST[nome])){



          //  ENVIANDO A MENSAGEM PARA O CLIENTE
          $texto_mensagem = "
                                 O seguinte cliente fez uma solicitação pelo site. <br />

                                 Nome: $_POST[nome] <br />
                                 Email: $_POST[email] <br />
                                 Endereço: $_POST[endereco] <br />
                                 Complemento: $_POST[complemento] <br />
                                 Cep: $_POST[cep] <br />
                                 Fax: $_POST[fax] <br />
                                 Email: $_POST[email] <br />
                                 Telefone: $_POST[telefone] <br />
                                 Bairro: $_POST[bairro] <br />
                                 Cidade: $_POST[cidade] <br />
                                 Estado: $_POST[estado] <br />
                                 Como prefere receber informação: $_POST[receber_infomacoes_heliossol] <br />
                                 Melhor Horário: $_POST[melhor_data] <br />
                                 Em quanto tempo você pretende adquirir seu aquecedor solar: $_POST[tempo_adiquirir] <br />
                                 Como gostaria de efetuar o pagamento: $_POST[pagamento] <br />    
                                 Outros pagamentos: $_POST[pagamento_outros] <br /> 
                                 Localização da piscina: $_POST[localizacao_piscina] <br /> 
                                 Se possui algum tipo de aquecimento, qual: $_POST[tipo_aquecimento] <br /> 


                                 Formato da piscina: $_POST[formato_piscina] <br /> 
                                 Comprimento: $_POST[comprimento] <br /> 
                                 Largura: $_POST[largura] <br /> 
                                 Maior profundidade: $_POST[maior_profundidade] <br /> 
                                 Menor profundidade: $_POST[menor_profundidade] <br /> 
                                 
                                 Se sua piscina tem formato irregular, por favor indique: <br> 
                                 Área total: $_POST[area_total] <br /> 
                                 Volume total: $_POST[volume_total] <br /> 


                                 
                                 




                                 Como conheceu nosso site: <br>
                                 Internet: $_POST[conheceu_internet] <br />
                                 Revista: $_POST[conheceu_revista] <br />
                                 Indicação: $_POST[conheceu_indicacao] <br />
                                 Outras Indicação: $_POST[conheceu_indicacao_outros] <br />

                                 Mensagem: <br />
                                 ". nl2br($_POST[mensagem]) ." <br />

             ";


             Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
             Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
             Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

   }
   ?>

 


 <!--  ==============================================================  -->
  <!-- FORMULARIO ENVIE ORCAMENTO-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row formulario ">
      <form action="" class="FormContato" method="post" >

        <div class="col-xs-12 form-group">      
          <input type="text" name="nome"  placeholder="NOME" class="form-control input-lg">
        </div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-lg">
        </div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="complemento"  placeholder="COMPLEMENTO" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="bairro"  placeholder="BAIRRO" class="form-control input-lg">
        </div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="estado"  placeholder="ESTADO" class="form-control input-lg">
        </div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="cep"  placeholder="CEP" class="form-control input-lg">
        </div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control input-lg">
        </div>

        <div class="col-xs-12 form-group">      
          <input type="text" name="fax"  placeholder="FAX" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>


        <div class="col-xs-12 form-group">      
          <input type="text" name="email"  placeholder="EMAIL" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>


        <div class="col-xs-12 top30">
          <h2>Como você prefere receber informações da Heliossol?</h2>


          <select name="receber_infomacoes_heliossol" class="form-control input-lg">
              <option value="">Selecione</option>
              <option value="E-mail">E-mail</option>
              <option value="Telefone">Telefone</option>
              <option value="Fax">Fax</option>
          </select>

         
        </div>

        <!--  ==============================================================  -->
        <!-- CALENDARIO VALIDADOR-->
        <!--  ==============================================================  -->
        <div class="col-xs-12 top20" id="meetingForm">     
          <div class="form-group">
            
            <div class="top10">
              <h2>Melhor Data</h2>
              <div class="input-group date" id="datetimePicker">
                <input type="text" class="form-control input-lg" name="melhor_data" />
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
              <span class="help-block"></span>
            </div>
          </div>
        </div>
        <!--  ==============================================================  -->
        <!-- CALENDARIO VALIDADOR-->
        <!--  ==============================================================  -->






        <div class="container">
          <div class="row">
            <div class="col-xs-12">
                  <h2 class="top30">Em quanto tempo você pretende adquirir seu aquecedor solar?</h2>
                  
                  <select name="tempo_adiquirir" class="form-control input-lg">
                      <option value="">Selecione</option>
                      <option value="Imediatamente">Imediatamente</option>
                      <option value="1 Mês">1 Mês</option>
                      <option value="2 Meses">2 Meses</option>
                      <option value="3 Meses">3 Meses</option>
                      <option value="6 Meses">6 Meses</option>
                      <option value="Prazo diferente">Prazo diferente</option>
                      <option value="Não pretendo adquirir em breve, só quero saber o preço">Não pretendo adquirir em breve, só quero saber o preço</option>
                  </select>
            </div>
          </div>
        </div>




        <div class="clearfix"></div>

        <div class="col-xs-12 top30">
          <h2>Como gostaria de efetuar o pagamento?</h2>


          <div class="top20">
            <label class="radio-inline">
              <input type="radio" name="pagamento"  value="Antecipado"><p>Antecipado</p>
            </label>
            <label class="radio-inline">
              <input type="radio" name="pagamento"  value="3 Vezes (ato/30/60 dias)"><p>3 Vezes (ato/30/60 dias)</p>
            </label>
            <label class="radio-inline">
              <input type="radio" name="pagamento"  value="4 Vezes (ato/30/60/90 dias)"> <p>4 Vezes (ato/30/60/90 dias)</p>
            </label>
            <label class="radio-inline">
              <input type="radio" name="pagamento"  value="Financiamento CEF até 60 meses"> <p>Financiamento CEF até 60 meses.</p>
            </label>
          </div>

          <div class="form-group top20">
            <input type="text" name="pagamento_outros"  placeholder="OUTROS" class="form-control input-lg">
          </div>


        </div>
      






        <div class="col-xs-12 top30">
          <h2>Localização da piscina</h2>


          <div class="top20">
            <label class="radio-inline">
              <input type="radio" name="localizacao_piscina"  value="Ar livre"><p>Ar livre</p>
            </label>
            <label class="radio-inline">
              <input type="radio" name="localizacao_piscina"  value="Coberta"><p>Coberta</p>
            </label>
            
          </div>

          <div class="form-group top20">
            <input type="text" name="tipo_aquecimento"  placeholder="Se possui algum tipo de aquecimento, qual?" class="form-control input-lg">
          </div>


        </div>








        <div class="top40">
            <h2>Formato da piscina</h2>
          </div>

          <div class="top20">
            <label class="radio-inline">
              <input type="radio" name="formato_piscina"  value="Quadrada/Retangular"><p>Quadrada/Retangular</p>
            </label>
            <label class="radio-inline">
              <input type="radio" name="formato_piscina"  value="Redonda/Oval"><p>Redonda/Oval</p>
            </label>
            <label class="radio-inline">
              <input type="radio" name="formato_piscina"  value="Irregular"><p>Irregular</p>
            </label>
          </div>

          <div class="col-xs-3 form-group top20">
            <input type="text" name="comprimento"  placeholder="Comprimento" class="form-control input-lg">
          </div>

          <div class="col-xs-3 form-group top20">
            <input type="text" name="largura"  placeholder="Largura" class="form-control input-lg">
          </div>


          <div class="col-xs-3 form-group top20">
            <input type="text" name="maior_profundidade"  placeholder="Maior profundidade" class="form-control input-lg">
          </div>


          <div class="col-xs-3 form-group top20">
            <input type="text" name="menor_profundidade"  placeholder="Menor profundidade" class="form-control input-lg">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12 subtitulo-formulario top40">
            <h2>Se sua piscina tem formato irregular, por favor indique:</h2>
            <div class="col-xs-3 form-group top20">
              <input type="text" name="area_total"  placeholder="Área Total*" class="form-control input-lg">
            </div>


            <div class="col-xs-3 form-group top20">
              <input type="text" name="volume_total"  placeholder="Volume Total*" class="form-control input-lg">
            </div>
          </div>

          <div class="clearfix"></div>























        <div class="top40">
            <h2>Como você chegou ao site da Heliossol?</h2>
          </div>

          <div class="col-xs-12  subtitulo-formulario">
            <div class="top10">
              <h2>Internet</h2>
            </div>
            <div class="top10">
              <label class="radio-inline">
                <input type="radio" name="conheceu_internet"  value="Mecanismo de busca na Internet"><p>Mecanismo de busca na Internet</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_internet"  value="Link Site da Internet"><p>Link Site da Internet</p>
              </label>
            </div>

            <div class="top10">
              <h2>Revista</h2>
            </div>
            <div class="top20">
              <label class="radio-inline">
                <input type="radio" name="conheceu_revista"  value="Arquitetura e Construção"><p>Arquitetura e Construção</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_revista"  value="Outra revista"><p>Outra revista</p>
              </label>
            </div>


            <div class="top10">
              <h2>Indicação</h2>
            </div>
            <div class="top20">
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Amigo"><p>Amigo</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Vendedor"><p>Vendedor</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Construtor"><p>Construtor</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Engenheiro / Arquiteto"><p>Engenheiro / Arquiteto</p>
              </label>
            </div>

            <div class="top10">
              <h2>Indicação</h2>
            </div>
            <div class="top20">
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Correio Braziliense"><p>Correio Braziliense</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Jornal do Encanador"><p>Jornal do Encanador</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Outro jornal"><p>Outro jornal</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Catálogo"><p>Catálogo</p>
              </label>
              <label class="radio-inline">
                <input type="radio" name="conheceu_indicacao"  value="Feira"><p>Feira</p>
              </label>
            </div>
            <div class="col-xs-12 form-group top20">
              <input type="text" name="conheceu_indicacao_outros"  placeholder="Outros:" class="form-control input-lg">
            </div>
          </div>

          <div class="top40">
              <h2>Este espaço é para você colocar sua dúvida ou comentário:</h2>
            </div>

            <div class="col-xs-12 form-group top20 padding0">
              <textarea name="mensagem" id="" cols="30" rows="13" class="form-control input100"></textarea>
            </div>

             <div class="text-right top30 bottom25">
            <button type="submit" class="btn btn-formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>
        

         

      </form>



    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- FORMULARIO ENVIE ORCAMENTO-->
  <!--  ==============================================================  --> 



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<script>
  $(document).ready(function() {
    $('#datetimePicker').datetimepicker();

    $('#meetingForm').bootstrapValidator({
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
        meeting: {
          validators: {
            date: {
              format: 'MM/DD/YYYY h:m A',
              message: 'The value is not a valid date'
            }
          }
        }
      }
    });

    $('#datetimePicker')
    .on('dp.change dp.show', function(e) {
            // Validate the date when user change it
            $('#meetingForm')
                // Get the bootstrapValidator instance
                .data('bootstrapValidator')
                // Mark the field as not validated, so it'll be re-validated when the user change date
                .updateStatus('meeting', 'NOT_VALIDATED', null)
                // Validate the field
                .validateField('meeting');
              });
  });
</script>



<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      
   
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
     
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
     
   
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      }

    }
  });
});
</script>

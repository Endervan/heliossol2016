<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>

  <!-- FlexSlider 2 -->
  <script type="text/javascript" >
   $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: false,
      itemWidth: 205,
      itemMargin: 1
    });
  });
</script>
<!-- FlexSlider 2 -->




</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  

 <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-empresa">
        <div class="fundo-branco">
            <h1>
              CONFIRA NOSSOS
              <img src="../imgs/seta-titulo.png" alt="">
            </h1>

            <h2>SERVIÇOS</h2>

        </div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  --> 






  
  <!-- ======================================================================= -->
  <!-- CATEGORIAS SERVICOS -->
  <!-- ======================================================================= -->
  <div class="container ">
    <div class="row">
      <div class="col-xs-12 categorias-servicos">
        <div id="slider" class="flexslider">
         

          <ul class="slides ">

            
            <?php


            $result = $obj_site->select("tb_servicos");
             if(mysql_num_rows($result) > 0){
               while ($row = mysql_fetch_array($result)) {
               ?> 
                  <li class="top15">
                     <div class="media">
                      <div class="media-left media-middle">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">                          
                          <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem_icone]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                        </a>
                      </div>
                      <div class="media-body">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">                          
                          <h1><?php Util::imprime($row[titulo]); ?></h1>
                        </a>
                      </div>
                    </div>  
                  </li>
               <?php 
               }
             }
             ?>

        </ul>   
     </div>
    </div>
  </div>  
</div>
<!-- ======================================================================= -->
<!-- CATEGORIAS SERVICOS -->
<!-- ======================================================================= -->



<!--  ==============================================================  -->
<!-- TIPOS DE SEVICOS-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    

    <?php
    $result = $obj_site->select("tb_servicos");
    if(mysql_num_rows($result) > 0){
      while ($row = mysql_fetch_array($result)) {
      ?> 
          <div class="col-xs-12 position-servico">
              <div class=" top40">
                <div class="tipos-servicos">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                    <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/sombra-servicos.png" alt="">
                  </a>
                </div>
                <div class="col-xs-4 text-right"> 
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 123, 123, array("class"=>"img-circle", "alt"=>"$row[titulo]")) ?>
                  </a>  

                </div>

                <div class="col-xs-8">
                 <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                  <h1><b><?php Util::imprime($row[titulo]); ?></b></h1>
                 </a>
                 <div class="top5">
                   <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                    <p><?php Util::imprime($row[descricao], 300); ?></p>
                   </a>
                </div>
              </div>

            </div>
          </div>
      <?php 
      }
    }
    ?>




  <div class="clearfix"></div>
  <div class="top30"></div>

  



</div> 
</div> 
<!--  ==============================================================  -->
<!-- TIPOS DE SEVICOS-->
<!--  ==============================================================  --> 



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


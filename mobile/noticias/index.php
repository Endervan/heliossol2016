<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 15);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-empresa">
        <div class="fundo-branco">
            <h1>
              CONFIRA NOSSAS
              <img src="../imgs/seta-titulo.png" alt="">
            </h1>

            <h2>NOTÍCIAS</h2>

        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-empresa descricao-->
  <!--  ==============================================================  -->




  <!-- ======================================================================= -->
  <!-- filtro noticias  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row bg-pesquisa">
        <div class="col-xs-2 top15 text-right">
              <h1>FILTRAR NOTÍCIAS:</h1>
        </div>


        <div class="col-xs-10 top15">
          <form action="<?php echo Util::caminho_projeto() ?>/mobile/noticias/" method="post">
           <div class="input-group col-xs-12">
            <input type="text" class="form-control form" name="busca_noticias" placeholder="TERMO DA BUSCA">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
            </span>
          </div>
        </form>

        </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- filtro noticias  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- lista noticias  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">


          <?php
          if(isset($_POST[busca_noticias])){
            $complemento = "and titulo like '%$_POST[busca_noticias]%'";
          }

          $result = $obj_site->select("tb_noticias", $complemento);
          if(mysql_num_rows($result) == 0){
            echo "<h2 class='bg-info top25 ' style='padding: 20px; color:#000;'>Nenhum Notícia(s) encontrado.</h2>";
          }else{
            while ($row = mysql_fetch_array($result)) {
              ?>
              <!-- item01 -->
              <div class="col-xs-12 top15">

                  <div class="thumbnail noticias-thumb">
                    <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 480, 220, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                    </a>
                    <div class="caption top20">
                      <h1><?php Util::imprime($row[titulo]); ?></h1>
                      <div class="noticias-descricao">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                          <p><?php Util::imprime($row[descricao], 300); ?></p>
                        </a>
                     </div>
                   </div>
               </div>
             </div>
             <?php
               if ($i == 1) {
                 echo '<div class="clearfix"></div>';
                 $i = 0;
               }else{
                $i++;
               }

             }
           }
           ?>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- lista noticias  -->
  <!-- ======================================================================= -->









  <?php require_once('../includes/rodape.php'); ?>

</body>

</html>

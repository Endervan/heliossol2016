
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>

<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap.min.css" />
<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap-timepicker.min.css" />
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js"></script>

</head>

<body class="orcamento-piscinas">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-piscina descricao-->
  <!--  ==============================================================  -->
  <div class="container top205">
    <div class="row">
      <div class="bg-descricao-piscinas">
        <div class="fundo-branco"></div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-piscina descricao-->
  <!--  ==============================================================  --> 

  <?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
    if(isset($_POST[nome])){

        //  CADASTRO OS PRODUTOS SOLICITADOS
        for($i=0; $i < count($_POST[qtd]); $i++){
            $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

            $mensagem .= "
                        <tr>
                            <td><p>". $_POST[qtd][$i] ."</p></td>
                            <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                         </tr>
                        ";
        }


        if (count($_POST[categorias]) > 0) {
            foreach($_POST[categorias] as $cat){
                $desc_cat .= $cat . ' , ';
            }
        }


        //  ENVIANDO A MENSAGEM PARA O CLIENTE
        $texto_mensagem = "
                            O seguinte cliente fez uma solicitação pelo site. <br />

                            Nome: $_POST[nome] <br />
                            Enderenco: $_POST[endereco] <br />
                            Complemento: $_POST[complemento] <br />
                            Cep: $_POST[cep] <br />
                            Fax: $_POST[fax] <br />
                            Email: $_POST[email] <br />
                            Telefone: $_POST[telefone] <br />
                            Bairro: $_POST[bairro] <br />
                            Cidade: $_POST[cidade] <br />
                            Estado: $_POST[estado] <br />
                            Como prefere receber informação: $_POST[receber_infomacoes_heliossol] <br />
                            Data agendamento: $_POST[data] <br />
                            Em quanto tempo você pretende adquirir seu aquecedor solar? $_POST[Tempo_adquirir_seu_quecedor] <br />
                            Outros: $_POST[outros] <br />
                            Qual o estado da sua piscina?: $_POST[Estado_piscina] <br />
                            Formato da piscina: $_POST[Formato_piscina] <br />
                            Complemento: $_POST[complemento] <br />
                            Largura: $_POST[largura] <br />
                            Maior Profudidade: $_POST[Maiorprofundidade] <br />
                            Menor profundidade: $_POST[Menorprofundidade] <br />
                            sua piscina tem formato irregular:$_POST[AreaTotal]*$_POST[VolumeTotal] <br />
                            interesse em aquecer também água em local diferente de hotel/motel:$_POST[local_diferente]<br />
                            Outro Local: $_POST[local] <br />
                            Se tem interesse em outros produtos da linha Heliossol, por favor especifique:$_POST[interesse] <br />

                            Como conheceu nosso site: <br>
                            Internet-$_POST[internet] <br />
                            Revista-$_POST[revista] <br />
                            Indicação-$_POST[indicacao] <br />
                            Outras Indicação-$_POST[indicacao_outros] <br />

                            Mensagem: <br />
                            ". nl2br($_POST[mensagem]) ." <br />

                            ";

       

        Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
        Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
        unset($_SESSION[solicitacoes_produtos]);
        unset($_SESSION[solicitacoes_servicos]);
        unset($_SESSION[piscinas_vinil]);
        Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

    }
    ?>
 


 <!--  ==============================================================  -->
  <!-- carrinho-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">

      <div class="col-xs-12 tb-lista-itens">
        <table class="table">

         <thead>
          <tr>
          <th>ITEM</th>
            <th>PRODUTO</th>
            <th class="text-center">QUANTIDADE</th>
            <th class="text-center">DELETAR</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td><img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/orcamentos-selecionavel.jpg" alt=""></td>
            <td>Placas Coletoras Ts Solar</td>
            <td class="text-center">
              <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
              <input name="idproduto[]" type="hidden" value="263"  />
            </td>
            <td class="text-center">
              <a href="" data-toggle="tooltip" data-placement="top" title="Excluir">
                <i class="fa fa-times-circle fa-2x"></i>
              </a>
            </td>
          </tr>
        </tbody>

      </table>



      <div class=" top15">

        <a href="<?php echo Util::caminho_projeto() ?>/produtos" title="Continuar orçando" class="btn btn-laranja1">
          Continuar orçando
        </a>
      </div>

    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- carrinho -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO produtos-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row formulario bottom30">
    <div class="top20">


       <form action="" class="FormContato" id="profileForm" method="post" accept-charset="utf-8">

        <div class="col-xs-6 form-group">      
          <input type="text" name="nome"  placeholder="NOME" class="form-control input-lg">
        </div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-lg">
        </div>

         <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="complemento"  placeholder="COMPLEMENTO" class="form-control input-lg">
        </div>

        

        <div class="col-xs-6 form-group">      
          <input type="text" name="bairro"  placeholder="BAIRRO" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="estado"  placeholder="ESTADO" class="form-control input-lg">
        </div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="cep"  placeholder="CEP" class="form-control input-lg">
        </div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>

        <div class="col-xs-6 form-group">      
          <input type="text" name="fax"  placeholder="FAX" class="form-control input-lg">
        </div>



        <div class="col-xs-6 form-group">      
          <input type="text" name="email"  placeholder="EMAIL" class="form-control input-lg">
        </div>

        <div class="clearfix"></div>


        <div class="col-xs-12 top30">
          <h2>Como você prefere receber informações da Heliossol?</h2>

          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio1" value="E-mail">E-mail
            </label>
            <label class="radio-inline">
              <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio2" value="Fax">Fax
            </label>
            <label class="radio-inline">
              <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio3" value="Telefone"> Telefone
            </label>
          </div>
        </div>

       <!--  ==============================================================  -->
      <!-- CALENDARIO VALIDADOR-->
      <!--  ==============================================================  -->
      <div class="clearfix"></div>
          <div class="col-xs-4 top25 text-right">
                    <h2>Melhor Horário</h2>
          </div>
            <div class="col-xs-6 top10 bootstrap-timepicker timepicker padding0 form-group">
            <input id="timepicker1" type="text" name="agenda" class="form-control input-lg">
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
              $('#timepicker1').timepicker({
                 showMeridian: false
              });

            });
        </script>
      <!--  ==============================================================  -->
      <!-- CALENDARIO VALIDADOR-->
      <!--  ==============================================================  -->

        <div class="clearfix"></div>

        <div class="col-xs-12 top30">
          <h2>Em quanto tempo você pretende adquirir seu aquecedor solar?</h2>

          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj1" value="Antecipado">Antecipado
            </label>
            <label class="radio-inline">
              <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj2" value="3 Vezes (ato/30/60 dias)">3 Vezes (ato/30/60 dias)
            </label>

            <div class="clearfix"></div>
            <label class="radio-inline">
              <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj3" value="4 Vezes (ato/30/60/90 dias)"> 4 Vezes (ato/30/60/90 dias)
            </label>
            <label class="radio-inline">
              <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj4" value=" Financiamento CEF até 60 meses.
            </label>"> Financiamento CEF até 60 meses.
            </label>
          </div>

          <div class="form-group top10">
            <input type="text" name=""  placeholder="OUTROS" class="form-control input-lg">
          </div>

          <div class="top40">
            <h2>Qual o estado da sua piscina?</h2>
          </div>

          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="Estado_piscina" id="inlineRadio8" value="Obra Acabada">Obra Acabada
            </label>
            <label class="radio-inline">
              <input type="radio" name="Estado_piscina" id="inlineRadio9" value="Em Construção">Em Construção
            </label>
          </div>

          <div class="top40">
            <h2>Formato da piscina</h2>
          </div>

          <div class="top20 form-group">
            <label class="radio-inline">
              <input type="radio" name="Formato_piscina" id="inlineRadio10" value="Quadrada/Retangular">Quadrada/Retangular
            </label>
            <label class="radio-inline">
              <input type="radio" name="Formato_piscina" id="inlineRadio11" value="Redonda/Oval">Redonda/Oval
            </label>

            <label class="radio-inline">
              <input type="radio" name="Formato_piscina" id="inlineRadio12" value="Irregular">Irregular
            </label>
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 form-group">
            <input type="text" name="Comprimento"  placeholder="Comprimento" class="form-control input-lg">
          </div>

          <div class="col-xs-6 form-group">
            <input type="text" name="largura"  placeholder="Largura" class="form-control input-lg">
          </div>


          <div class="clearfix"></div>

          <div class="col-xs-6 form-group">
            <input type="text" name="Maiorprofundidade"  placeholder="Maior profundidade" class="form-control input-lg">
          </div>


          <div class="col-xs-6 form-group">
            <input type="text" name="Menorprofundidade"  placeholder="Menor profundidade" class="form-control input-lg">
          </div>

          <div class="clearfix"></div>

          <div class="subtitulo-formulario top20">
            <h2>Se sua piscina tem formato irregular, por favor indique:</h2>
            <div class="col-xs-6 form-group top5">
              <input type="text" name="AreaTotal"  placeholder="Área Total*" class="form-control input-lg">
            </div>


            <div class="col-xs-6 form-group top5">
              <input type="text" name="VolumeTotal"  placeholder="Volume Total*" class="form-control input-lg">
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="top20">
            <h2>Tem interesse em aquecer também água em local diferente de hotel/motel? Especifique:</h2>
          </div>

          <div class="top5 form-group">
            <label class="radio-inline">
              <input type="radio" name="local_diferente" id="inlineRadio13" value="Residência">Residência
            </label>
            <label class="radio-inline">
              <input type="radio" name="local_diferente" id="inlineRadio14" value="Hospital">Hospital
            </label>
            <label class="radio-inline">
              <input type="radio" name="local_diferente" id="inlineRadio15" value="Hotel">Hotel
            </label>
            <label class="radio-inline">
              <input type="radio" name="local_diferente" id="inlineRadio16" value="Motel">Motel
            </label>

            <div class="clearfix"></div>

            <label class="radio-inline">
              <input type="radio" name="local_diferente" id="inlineRadio17" value="Edifício residencial a construir">Edifício residencial a construir
            </label>

            <div class="clearfix"></div>

            <label class="radio-inline">
              <input type="radio" name="local_diferente" id="inlineRadio18" value="Edifício residencial em construção">Edifício residencial em construção
            </label>
          </div>

          <div class="col-xs-12 form-group top10">
            <input type="text" name=""  placeholder="Outro Local:" class="form-control input-lg">
          </div>

          <div class="col-xs-12 form-group top10">
            <input type="text" name=""  placeholder="Se tem interesse em outros produtos da linha Heliossol, por favor especifique:" class="form-control input-lg">
          </div>

          <div class="top20">
            <h2>Como você chegou ao site da Heliossol?</h2>
          </div>

          <div class="col-xs-12  subtitulo-formulario">
            <div class="top10">
              <h2>Internet</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="internet" id="inlineRadio40" value="Mecanismo de busca na Internet">Mecanismo de busca na Internet
              </label>
              <label class="radio-inline">
                <input type="radio" name="internet" id="inlineRadio19" value="Link Site da Internet">Link Site da Internet
              </label>
            </div>

            <div class="top10">
              <h2>Revista</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="revista" id="inlineRadio20" value="Arquitetura e Construção">Arquitetura e Construção
              </label>
              <label class="radio-inline">
                <input type="radio" name="revista" id="inlineRadio21" value="Outra revista">Outra revista
              </label>
            </div>


            <div class="top10">
              <h2>Indicação</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio22" value="Amigo">Amigo
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio23" value="Vendedor">Vendedor
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio24" value="Construtor">Construtor
              </label>

              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="indicacao" id="inlineRadio25" value="Engenheiro / Arquiteto">Engenheiro / Arquiteto
              </label>
            </div>

            <div class="top10">
              <h2>Outros Indicação</h2>
            </div>
            <div class="top10 form-group">
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio26" value="Correio Braziliense">Correio Braziliense
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio27" value="Jornal do Encanador">Jornal do Encanador
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio28" value="Outro jornal">Outro jornal
              </label>

              <div class="clearfix"></div>

              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio29" value="Catálogo">Catálogo
              </label>
              <label class="radio-inline">
                <input type="radio" name="indicacao_outros" id="inlineRadio30" value="Feira">Feira
              </label>
            </div>
            <div class="form-group top10">
              <input type="text" name=""  placeholder="Outros:" class="form-control input-lg">
            </div>
          </div>

          <div class="top10">
            <h2>Este espaço é para você colocar sua dúvida ou comentário:</h2>
          </div>

          <div class="col-xs-12 form-group top5 padding0">
            <textarea name=""  cols="30" rows="13" class="form-control input100"></textarea>
          </div>

          <div class="text-right top30 bottom25">
            <button type="submit" class="btn btn-formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>

        </div>
        



      </form>


    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO-->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      
   
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
     
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
     
   
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      }

    }
  });
});
</script>




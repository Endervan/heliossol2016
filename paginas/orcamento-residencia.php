<!DOCTYPE html>
<html lang="pt-br">

<head>

  <?php require_once('./includes/head.php'); ?>

  <link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap.min.css" />
  <link type="text/css" href="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/css/bootstrap-timepicker.min.css" />
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/Bootstrap-Timepicker/js/bootstrap-timepicker.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.5.1/less.min.js"></script>


</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 11) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-orcamentos enviar-->
  <!--  ==============================================================  -->
  <div class="container top190">
    <div class="row">
      <div class="bg-descricao-orcamentos"></div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-orcamentos enviar-->
  <!--  ==============================================================  --> 



  <!--  ==============================================================  -->
  <!--ENVIE SEU ORCAMENTOS-->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row envie-orcamentos">   
      <div class="col-xs-2 text-center top10 bottom10">
        <a href="" title="">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento03.png" alt="">
        </a>
      </div>  
      <div class=" col-xs-10 top50">
        <h1>ORÇAMENTOS PARA RESIDÊNCIA</h1>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!--ENVIE SEU ORCAMENTOS-->
  <!--  ==============================================================  --> 


  <?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){



        //  ENVIANDO A MENSAGEM PARA O CLIENTE
        $texto_mensagem = "
                   O seguinte cliente fez uma solicitação pelo site. <br />

                   Nome: $_POST[nome] <br />
                   Enderenco: $_POST[endereco] <br />
                   Complemento: $_POST[complemento] <br />
                   Cep: $_POST[cep] <br />
                   Fax: $_POST[fax] <br />
                   Telefone: $_POST[telefone] <br />
                   Bairro: $_POST[bairro] <br />
                   Cidade: $_POST[cidade] <br />
                   Estado: $_POST[estado] <br />
                   Email: $_POST[email] <br />
                   Como prefere receber informação da Heliossol: $_POST[receber_infomacoes_heliossol] <br />
                   Melhor Horário: $_POST[agenda] <br />
                   Em quanto tempo você pretende adquirir seu aquecedor solar?$_POST[adquirir_aquecedor] <br />
                   Como gostaria de efetuar o pagamento? $_POST[Tempo_adquirir_seu_quecedor] <br />
                   Qual o estado da sua Residência?: $_POST[Estado_piscina] <br />
                   Se sua residência está em construção, em qual etapa está sua obra?$_POST[fundacao] <br />
                   Qual cidade está sua obra?$_POST[Qual_cidade] <br />
                   Tipo de edificação:$_POST[tipo_edificacao] <br />
                   Tipo de cobertura:$_POST[tipo_cobertura] <br />
                   Já existe ou pretende instalar tubulação para água quente?$_POST[istalar_tubulacao] <br />
                   Sim. Outro material:$_POST[outros_materiais] <br />
                   Qual o padrão de pressão da rede hidráulica da residência?$_POST[pressao_rede] <br />
                   Como é a água que abastece sua residência?$_POST[abastece_residencia] <br />
                   Tensão elétrica na residência:$_POST[tensao_eletrica] <br />
                   Número de pessoas na residência:$_POST[numero_pessoas] <br />
                   Número de banheiros*:$_POST[numero_banheiras] <br />
                   Quantidade de banhos por dia*:$_POST[quantidade_banhos] <br />
                   Duração média de cada banho*$_POST[duracao_banho] <br />
                   Número de banheiras individuais:$_POST[banheiras_duplas] <br />
                   Aquecer água da Lavanderia*?$_POST[aquecer_agua] <br />
                   Aquecer água da Cozinha?$_POST[aquecer_agua_cozinha] <br />
                   Outros pontos de água quente a acrescentar:$_POST[outros_pontos] <br />
                   Tem interesse em receber orçamento de aquecedores solares compactos entre 100 e 200 litros:$_POST[aquecedores_compactos] <br />
                   interesse em aquecer também água em local diferente de hotel/motel:$_POST[local_diferente]<br />
                   Outro Local: $_POST[local] <br />
                   Se tem interesse em outros produtos da linha Heliossol, por favor especifique:$_POST[interesse] <br />

                   Como conheceu nosso site: <br>
                   Internet-$_POST[internet] <br />
                   Revista-$_POST[revista] <br />
                   Indicação-$_POST[indicacao] <br />
                   Outras Indicação-$_POST[indicacao_outros] <br />

                   Mensagem: <br />
                   ". nl2br($_POST[mensagem]) ." <br />

                   ";



                   Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
                   Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
                   Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

                 }
 ?>




 <!--  ==============================================================  -->
 <!-- FORMULARIO ENVIE ORCAMENTO-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row formulario top20">
    <form action="" class="FormResidencial" method="post" accept-charset="utf-8">

      <div class="col-xs-4 form-group">      
        <input type="text" name="nome"  placeholder="NOME" class="form-control input-lg">
      </div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control input-lg">
      </div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="complemento"  placeholder="COMPLEMENTO" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="bairro"  placeholder="BAIRRO" class="form-control input-lg">
      </div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="cidade"  placeholder="CIDADE" class="form-control input-lg">
      </div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="estado"  placeholder="ESTADO" class="form-control input-lg">
      </div>

      

      <div class="clearfix"></div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="cep"  placeholder="CEP" class="form-control input-lg">
      </div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control input-lg">
      </div>

      <div class="col-xs-4 form-group">      
        <input type="text" name="fax"  placeholder="FAX" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>


      <div class="col-xs-4 form-group">      
        <input type="text" name="email"  placeholder="EMAIL" class="form-control input-lg">
      </div>


      <div class="col-xs-12 top30">
        <h2>Como você prefere receber informações da Heliossol?</h2>

        <div class="top20">
          <label class="radio-inline">
            <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio1" value="E-mail">E-mail
          </label>
          <label class="radio-inline">
            <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio2" value="Fax">Fax
          </label>
          <label class="radio-inline">
            <input type="radio" name="receber_infomacoes_heliossol" id="inlineRadio3" value="Telefone"> Telefone
          </label>
        </div>
      </div>


      <!--  ==============================================================  -->
      <!-- CALENDARIO VALIDADOR-->
      <!--  ==============================================================  -->
      <div class="clearfix"></div>
          <div class="col-xs-2 top25 text-right">
                    <h2>Melhor Horário</h2>
          </div>
            <div class="col-xs-2 top10 bootstrap-timepicker timepicker padding0 form-group">
            <input id="timepicker1" type="text" name="agenda" class="form-control input-lg">
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
              $('#timepicker1').timepicker({
                 showMeridian: false
              });

            });
        </script>
      <!--  ==============================================================  -->
      <!-- CALENDARIO VALIDADOR-->
      <!--  ==============================================================  -->
      
      <div class="clearfix"></div>

      <div class="top40">
        <h2>Em quanto tempo você pretende adquirir seu aquecedor solar?</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio10" value="Imediatamente">Imediatamente
        </label>
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio11" value="1 Mês">1 Mês

        </label>
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio12" value="2 Meses">2 Meses
        </label>
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio12" value="3 Meses">3 Meses
        </label>
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio12" value="6 Meses">6 Meses
        </label>
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio12" value="Prazo diferente">Prazo diferente
        </label>
        <div class="clearfix"></div>
        <label class="radio-inline">
          <input type="radio" name="adquirir_aquecedor" id="inlineRadio12" value="Não pretendo adquirir em breve, só quero saber o preço.">Não pretendo adquirir em breve, só quero saber o preço.
        </label>
      </div>



      <div class="clearfix"></div>

      <div class="col-xs-12 top30">
        <h2>Como gostaria de efetuar o pagamento?</h2>

        <div class="top20 form-group">
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj1" value="Antecipado">Antecipado
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj2" value="3 Vezes (ato/30/60 dias)">3 Vezes (ato/30/60 dias)
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj3" value="4 Vezes (ato/30/60/90 dias)"> 4 Vezes (ato/30/60/90 dias)
          </label>
          <label class="radio-inline">
            <input type="radio" name="Tempo_adquirir_seu_quecedor" id="hj4" value=" Financiamento CEF até 60 meses.
          </label>"> Financiamento CEF até 60 meses.
        </label>
      </div>

      <div class="form-group top20">
        <input type="text" name="outros"  placeholder="OUTROS" class="form-control input-lg">
      </div>

      <div class="top40">
        <h2>Qual o estado da sua Residencia?</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="Estado_piscina" id="inlineRadio8" value="Obra Acabada">Obra Acabada
        </label>
        <label class="radio-inline">
          <input type="radio" name="Estado_piscina" id="inlineRadio9" value="Em Construção">Em Construção
        </label>
      </div>

      <div class="top40">
        <h2>Se sua residência está em construção, em qual etapa está sua obra?</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="fundacao" id="inlineRadio10" value="Fundação">Fundação
        </label>
        <label class="radio-inline">
          <input type="radio" name="fundacao" id="inlineRadio11" value="Redonda/Oval">Alvenaria ou Respaldo
        </label>
        <label class="radio-inline">
          <input type="radio" name="fundacao" id="inlineRadio12" value="Cobertura">Cobertura
        </label>
        <label class="radio-inline">
          <input type="radio" name="fundacao" id="inlineRadio12" value="Acabamento">Acabamento
        </label>
        <label class="radio-inline">
          <input type="radio" name="fundacao" id="inlineRadio12" value="Concluída">Concluída
        </label>
      </div>
      <div class="top20">
        <h2>Qual cidade está sua obra?</h2>
      </div>

      <div class="form-group top20">
        <input type="text" name="Qual_cidade"  placeholder="Cidade" class="form-control input-lg">
      </div>

      <div class="top40">
        <h2>Tipo de edificação:</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="tipo_edificacao" id="inlineRadio10" value="Térrea – um só pavimento">Térrea – um só pavimento
        </label>
        <label class="radio-inline">
          <input type="radio" name="tipo_edificacao" id="inlineRadio11" value="Sobrado – dois ou mais pavimentos">Sobrado – dois ou mais pavimentos
        </label>
        <label class="radio-inline">
          <input type="radio" name="tipo_edificacao" id="inlineRadio12" value="Cobertura de edifício – último pavimento">Cobertura de edifício – último pavimento
        </label>
      </div>

      <div class="top40">
        <h2>Tipo de cobertura:</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="tipo_cobertura" id="inlineRadio10" value="Telhado com forro ou laje">Telhado com forro ou laje
        </label>
        <label class="radio-inline">
          <input type="radio" name="tipo_cobertura" id="inlineRadio11" value="Telhado com torre para caixa d'água e reservatório térmico">Telhado com torre para caixa d'água e reservatório térmico
        </label>
        <label class="radio-inline">
          <input type="radio" name="tipo_cobertura" id="inlineRadio12" value="Telhado sem forro ou laje">Telhado sem forro ou laje
        </label>
        <label class="radio-inline">
          <input type="radio" name="tipo_cobertura" id="inlineRadio12" value="Laje apenas">Laje apenas
        </label>
      </div>

      <div class="top40">
        <h2>Já existe ou pretende instalar tubulação para água quente?</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="istalar_tubulacao" id="inlineRadio10" value="Não">Não
        </label>
        <label class="radio-inline">
          <input type="radio" name="istalar_tubulacao" id="inlineRadio11" value="Sim. Tubulação em cobre">Sim. Tubulação em cobre
        </label>
        <label class="radio-inline">
          <input type="radio" name="istalar_tubulacao" id="inlineRadio12" value="Sim. Tubulação em CPVC">Sim. Tubulação em CPVC
        </label> 
      </div>

      <div class="top20">
        <h2>Sim. Outro material:</h2>
      </div>

      <div class="form-group top20 padding0">
        <input type="text" name="outros_materiais"  placeholder="especifique" class="form-control input-lg">
      </div>

      <div class="top40">
        <h2>Qual o padrão de pressão da rede hidráulica da residência?</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="pressao_rede" id="inlineRadio10" value="Normal – distribuição hidráulica por gravidade a partir de caixa d'água elevada">Normal – distribuição hidráulica por gravidade a partir de caixa d'água elevada
        </label>
        <div class="clearfix"></div>
        <label class="radio-inline">
          <input type="radio" name="pressao_rede" id="inlineRadio11" value="Pressurizada – distribuição hidráulica a partir de pressurizador eletromecânico (bomba)">Pressurizada – distribuição hidráulica a partir de pressurizador eletromecânico (bomba)
        </label>
      </div>

      <div class="top40">
        <h2>Como é a água que abastece sua residência?</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="abastece_residencia" id="inlineRadio10" value="Água da rede pública – concessionária de água">Água da rede pública – concessionária de água
        </label>
        <label class="radio-inline">
          <input type="radio" name="abastece_residencia" id="inlineRadio11" value="Água tratada">Água tratada
        </label>
        <label class="radio-inline">
          <input type="radio" name="abastece_residencia" id="inlineRadio10" value="Água de poço sem tratamento">Água de poço sem tratamento
        </label>
        <label class="radio-inline">
          <input type="radio" name="abastece_residencia" id="inlineRadio11" value="Água de mina sem tratamento">Água de mina sem tratamento
        </label>
      </div>

      <div class="top40">
        <h2>Tensão elétrica na residência:</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="tensao_eletrica" id="inlineRadio10" value="110 Volts">110 Volts
        </label>
        <label class="radio-inline">
          <input type="radio" name="tensao_eletrica" id="inlineRadio11" value="220 Volts">220 Volts
        </label>
        <label class="radio-inline">
          <input type="radio" name="tensao_eletrica" id="inlineRadio10" value="Sem energia elétrica">Sem energia elétrica
        </label>
      </div>

      <div class="top40">
        <h2>Número de pessoas na residência*:</h2>
      </div>
      <div class="col-xs-3 form-group top10">
        <input type="text" name="numero_pessoas"  placeholder="Quantidade" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="top20">
        <h2>Número de banheiros*:</h2>
      </div>
      <div class="col-xs-3 form-group top10">
        <input type="text" name="numero_banheiras"  placeholder="Quantidade" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="top20">
        <h2>Quantidade de banhos por dia*:</h2>
        <p>(considerar banhos de todas as pessoas da residência)</p>
      </div>
      <div class="col-xs-3 form-group top10">
        <input type="text" name="quantidade_banhos"  placeholder="Quantidade" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="top20">
        <h2>Duração média de cada banho*</h2>
      </div>
      <div class="col-xs-3 top10 padding0 form-group">
        <select name="duracao_banho" id="input" class="form-control input-lg">
          <option value="">Selecione</option>
          <option value="05Minutos">05 Minutos</option>
          <option value="10Minutos">10 Minutos</option>
          <option value="15Minutos">15 Minutos</option>
          <option value="25Minutos">25 Minutos</option>
          <option value="35Minutos">35 Minutos</option>
          <option value="40Minutos">40 Minutos</option>
        </select>
      </div>

      <div class="clearfix"></div>

      <div class="top20">
        <h2>Número de banheiras individuais:</h2>
      </div>
      <div class="form-group top10 padding0">
        <input type="text" name="banheiras_duplas"  placeholder="Quantidade" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

      <div class="top20">
        <h2>Aquecer água da Lavanderia*?</h2>
      </div>
      <div class="col-xs-3 top10 padding0 form-group">
        <select name="aquecer_agua" id="input" class="form-control input-lg">
          <option value="">Selecione</option>
          <option value="SIM">SIM</option>
          <option value="NÃO">NÃO</option>
        </select>
      </div>

      <div class="clearfix"></div>


      <div class="top20">
        <h2>Aquecer água da Cozinha?</h2>
      </div>
      <div class="col-xs-3 top10 padding0 form-group">
        <select name="aquecer_agua_cozinha" id="input" class="form-control input-lg">
          <option value="">Selecione</option>
          <option value="SIM">SIM</option>
          <option value="NÃO">NÃO</option>
        </select>
      </div>

      <div class="clearfix"></div>


      <div class="top20">
        <h2>Outros pontos de água quente a acrescentar:</h2>
      </div>
      <div class="form-group top10 padding0">
        <input type="text" name="outros_pontos"  placeholder="Especifique" class="form-control input-lg">
      </div>

      <div class="clearfix"></div>

       <div class="top40">
        <h2>Tem interesse em receber orçamento de aquecedores solares compactos entre 100 e 200 litros:</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="aquecedores_compactos" value="SIM">SIM
        </label>
        <label class="radio-inline">
          <input type="radio" name="aquecedores_compactos" value="NÃO">NÃO
        </label>
      </div>

      <div class="clearfix"></div>

      
      <div class="top40">
        <h2>Tem interesse em aquecer também água em local diferente de hotel/motel? Especifique:</h2>
      </div>

      <div class="top20 form-group">
        <label class="radio-inline">
          <input type="radio" name="local_diferente" value="Residência">Residência
        </label>
        <label class="radio-inline">
          <input type="radio" name="local_diferente"  value="Hospital">Hospital
        </label>
        <label class="radio-inline">
          <input type="radio" name="local_diferente"  value="Hotel">Hotel
        </label>
        <label class="radio-inline">
          <input type="radio" name="local_diferente"  value="Motel">Motel
        </label>
        <label class="radio-inline">
          <input type="radio" name="local_diferente" value="Edifício residencial a construir">Edifício residencial a construir
        </label>
        <label class="radio-inline">
          <input type="radio" name="local_diferente" value="Edifício residencial em construção">Edifício residencial em construção
        </label>
      </div>

      <div class="col-xs-12 form-group top20">
        <input type="text" name="Local"  placeholder="Outro Local:" class="form-control input-lg">
      </div>

      <div class="col-xs-12 form-group top20">
        <input type="text" name="interesse"  placeholder="Se tem interesse em outros produtos da linha Heliossol, por favor especifique:" class="form-control input-lg">
      </div>

      <div class="top40">
        <h2>Como você chegou ao site da Heliossol?</h2>
      </div>

      <div class="col-xs-12  subtitulo-formulario">
        <div class="top10">
          <h2>Internet</h2>
        </div>
        <div class="top10 form-group">
          <label class="radio-inline">
            <input type="radio" name="internet" id="inlineRadio40" value="Mecanismo de busca na Internet">Mecanismo de busca na Internet
          </label>
          <label class="radio-inline">
            <input type="radio" name="internet" id="inlineRadio19" value="Link Site da Internet">Link Site da Internet
          </label>
        </div>

        <div class="top10">
          <h2>Revista</h2>
        </div>
        <div class="top20 form-group">
          <label class="radio-inline">
            <input type="radio" name="revista" id="inlineRadio20" value="Arquitetura e Construção">Arquitetura e Construção
          </label>
          <label class="radio-inline">
            <input type="radio" name="revista" id="inlineRadio21" value="Outra revista">Outra revista
          </label>
        </div>


        <div class="top10">
          <h2>Indicação</h2>
        </div>
        <div class="top20 form-group">
          <label class="radio-inline">
            <input type="radio" name="indicacao" id="inlineRadio22" value="Amigo">Amigo
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao" id="inlineRadio23" value="Vendedor">Vendedor
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao" id="inlineRadio24" value="Construtor">Construtor
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao" id="inlineRadio25" value="Engenheiro / Arquiteto">Engenheiro / Arquiteto
          </label>
        </div>

        <div class="top10">
          <h2>Outros Indicação</h2>
        </div>
        <div class="top20 form-group">
          <label class="radio-inline">
            <input type="radio" name="indicacao_outros" id="inlineRadio26" value="Correio Braziliense">Correio Braziliense
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao_outros" id="inlineRadio27" value="Jornal do Encanador">Jornal do Encanador
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao_outros" id="inlineRadio28" value="Outro jornal">Outro jornal
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao_outros" id="inlineRadio29" value="Catálogo">Catálogo
          </label>
          <label class="radio-inline">
            <input type="radio" name="indicacao_outros" id="inlineRadio30" value="Feira">Feira
          </label>
        </div>
        <div class="col-xs-12 form-group top20">
          <input type="text" name="outros1"  placeholder="Outros:" class="form-control input-lg">
        </div>
      </div>

      <div class="top40">
        <h2>Este espaço é para você colocar sua dúvida ou comentário:</h2>
      </div>

      <div class="col-xs-12 form-group top20 padding0">
        <textarea name="mensagem"  cols="30" rows="13" class="form-control input100"></textarea>
      </div>

      <div class="text-right top30 bottom25">
        <button type="submit" class="btn btn-formulario" >
          ENVIAR
        </button>
      </div>

    </div>




  </form>



</div>
</div> 
<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO-->
<!--  ==============================================================  --> 













<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>



<script>
  $(document).ready(function() {
    $('.FormResidencial').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      
   
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
     
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
     
   
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      }

    }
  });
});
</script>






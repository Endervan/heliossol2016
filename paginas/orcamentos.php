<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 19);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>




<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-orcamentos-->
  <!--  ==============================================================  -->
  <div class="container top190">
    <div class="row">
      <div class="col-xs-12">
        <div class="bg-descricao-orcamentos"></div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-orcamentos-->
  <!--  ==============================================================  --> 



  <!--  ==============================================================  -->
  <!-- TIPOS DE ORCAMENTOS-->
  <!--  ==============================================================  -->
  <div class="container bottom70">
    <div class="row tipos-orcamentos">
      <div class="col-xs-3 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/orcamento-residencia" title="ORÇAMENTOS PARA RESIDÊNCIAS EM CONSTRUÇÃO OU CONCLUÍDAS">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento01.png" alt="">
          <div class="top20">
            <h1>ORÇAMENTOS PARA RESIDÊNCIAS EM CONSTRUÇÃO OU CONCLUÍDAS</h1>
          </div>
        </a>
      </div>

      <div class="col-xs-3 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/orcamento-hoteis">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento02.png" alt="">
        
          <div class="top20">
            <h1>ORÇAMENTOS PARA HOTÉIS EM CONSTRUÇÃO OU CONCLUÍDOS</h1>
          </div>
        </a>
      </div>  

      <div class="col-xs-3 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/orcamento-piscinas" title="ORÇAMENTOS PARA PISCINAS">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento03.png" alt="">
          <div class="top20">
            <h1>ORÇAMENTOS PARA PISCINAS</h1>
          </div>
        </a>
      </div>  



      <div class="col-xs-3 text-center top50 bottom50">
        <a href="<?php echo Util::caminho_projeto() ?>/orcamento-produtos" title="ORÇAMENTOS PARA PRODUTOS SELECIONADOS">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-orcamento04.png" alt="">
          <div class="top20">
            <h1>ORÇAMENTOS PARA PRODUTOS SELECIONADOS</h1>
          </div>
        </a>
      </div>
     
      


    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- TIPOS DE ORCAMENTOS-->
  <!--  ==============================================================  --> 


  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>

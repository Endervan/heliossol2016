<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 18);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>




<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
  <!-- ======================================================================= -->
  <!-- hover nos produtos   -->
  <!-- ======================================================================= -->
  <script type="text/javascript">
    $(document).ready(function() {

      $(".produto-hover").hover(
        function () {
          $(this).stop().animate({opacity:1});
        },
        function () {
          $(this).stop().animate({opacity:0});
        }
        );

    });
  </script>


</head>




<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-produtos descricao-->
  <!--  ==============================================================  -->
  <div class="container top150">
    <div class="row">
      <div class="bg-descricao-produtos"></div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-produtos descricao-->
  <!--  ==============================================================  --> 



  <!--  ==============================================================  -->
  <!--   barra pesquisas e categorias -->
  <!--  ==============================================================  -->
  <?php require_once("./includes/filtro_produtos.php"); ?>
  <!--  ==============================================================  -->
  <!--   barra pesquisas e categorias -->
  <!--  ==============================================================  -->








<!--  ==============================================================  -->
<!-- MEnu lateral-->
<!--  ==============================================================  -->
<div class="container top30">
  <div class="row">

    <div class="col-xs-3 menu-produtos top20">
      
      <?php require_once("./includes/menu_produtos.php") ?>

    </div>
    <!--  ==============================================================  -->
    <!--  MEnu lateral-->
    <!--  ==============================================================  -->





    <!--  ==============================================================  -->
    <!-- produto-->
    <!--  ==============================================================  -->

    <div class="col-xs-9">


      <?php 


      $url1 = Url::getURL(1);
      $url2 = Url::getURL(2);

       //  FILTRA AS CATEGORIAS
       if (isset( $url1 )) {
          $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
          $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
       }


       //  FILTRA AS CATEGORIAS
       if (isset( $url2 )) {
          $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $url2);
          $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
       }



       //  FILTRA PELO TITULO
       if(isset($_POST[id_categoriaproduto]) and !empty($_POST[id_categoriaproduto])):
         $complemento .= "AND id_categoriaproduto = '$_POST[id_categoriaproduto]' ";
       endif;

       //  FILTRA PELO TITULO
       if(isset($_POST[id_subcategoriaproduto]) and !empty($_POST[id_subcategoriaproduto]) ):
         $complemento .= "AND id_subcategoriaproduto = '$_POST[id_subcategoriaproduto]' ";
       endif;


       //  FILTRA PELO TITULO
       if(isset($_POST[busca_topo])):
         $complemento .= "AND titulo LIKE '%$_POST[busca_topo]%'";
       endif;



      $result = $obj_site->select("tb_produtos", $complemento);
      if(mysql_num_rows($result) == 0){
         echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
      }else{
        while ($row = mysql_fetch_array($result)) {
        ?> 
        <!-- item 01 -->
        <div class="col-xs-4">
          <div class="thumbnail produtos-home">

           <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 232, 129, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>


           <div class="produto-hover">
             <div class="col-xs-6 text-center">
               <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                 <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.jpg" alt="" class="input100">
               </a>
             </div>
             <div class="col-xs-6 hover-btn-orcamento">
               <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                 <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.jpg" alt="" class="input100">
               </a>
             </div>
           </div>

           <div class="caption">
             <h1><?php Util::imprime($row[titulo]); ?></h1>
           </div>
         </div>
        </div>
        <?php 
        }
      }
      ?>



</div>

</div>
</div>

<!--  ==============================================================  -->
<!-- produto-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

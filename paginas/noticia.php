<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_noticias", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/noticias");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>




<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-noticias descricao-->
  <!--  ==============================================================  -->
  <div class="container top190">
    <div class="row">
      <div class="bg-descricao-noticias"></div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-noticias descricao-->
  <!--  ==============================================================  --> 



  <!--  ==============================================================  -->
  <!--   barra pesquisas e categorias -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
      <div class="fundo-amarelo">
        <div class="col-xs-3 top25 text-center">
          <h4>FILTRAR NOTÍCIAS:</h4>
        </div>
       

      <div class="col-xs-9 top15">

        <form action="<?php echo Util::caminho_projeto() ?>/noticias/" method="post">
           <div class="input-group input-group-lg">
            <input type="text" class="form-control form" name="busca_noticias" placeholder="TERMO DA BUSCA">
            <span class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
            </span>
          </div>       
        </form>

    </div>

  </div>
</div>
</div>
<!--  ==============================================================  -->
<!--   barra pesquisas e categorias -->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- NOTICIAS PRODUTOS DENTRO-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">

    <!-- item01 -->
    <div class="col-xs-12 top30">

        <div class="thumbnail noticias-thumb-dentro">

          <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 1100, 425, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>
          
          <div class="caption top20">
            <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
            <div class="top40">
              <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
           </div>
         </div>
       </div>  
     

   </div>

   

 </div>
</div> 
<!--  ==============================================================  -->
<!-- NOTICIAS PRODUTOS DENTRO-->
<!--  ==============================================================  --> 



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

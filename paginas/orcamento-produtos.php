<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 19);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-orcamentos enviar-->
  <!--  ==============================================================  -->
  <div class="container top190">
    <div class="row">
      <div class="col-xs-12">
        <div class="bg-descricao-orcamentos"></div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- bg-orcamentos enviar-->
  <!--  ==============================================================  -->





  <?php
  //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){

    //  CADASTRO OS PRODUTOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd]); $i++){
      $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

      $mensagem .= "
      <tr>
      <td><p>". $_POST[qtd][$i] ."</p></td>
      <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
      $itens .= "
      <tr>
      <td><p>". $_POST[qtd][$i] ."</p></td>
      <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
    }



    //  CADASTRO OS SERVICOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd_servico]); $i++)
    {
      $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);

      $itens .= "
      <tr>
      <td><p>". $_POST[qtd_servico][$i] ."</p></td>
      <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
    }



    if (count($_POST[categorias]) > 0) {
      foreach($_POST[categorias] as $cat){
        $desc_cat .= $cat . ' , ';
      }
    }


    //  ENVIANDO A MENSAGEM PARA O CLIENTE

    $texto_mensagem = "
    O seguinte cliente fez uma solicitação pelo site. <br />

    Nome: $_POST[nome] <br />
    Email: $_POST[email] <br />
    Telefone: $_POST[telefone] <br />
    Celular: $_POST[celular] <br />
    Bairro: $_POST[bairro] <br />
    Cidade: $_POST[cidade] <br />
    Estado: $_POST[estado] <br />
    Receber orçamento: $_POST[receber_orcamento] <br />
    Categorias desejadas: $desc_cat <br />
    Como conheceu nosso site: $_POST[como_conheceu] <br />

    Mensagem: <br />
    ". nl2br($_POST[mensagem]) ." <br />

    <br />
    <h2> Produtos selecionados:</h2> <br />

    <table width='100%' border='0' cellpadding='5' cellspacing='5'>
    <tr>
    <td><h4>QTD</h4></td>
    <td><h4>DESCRIÇÃO</h4></td>
    </tr>
    $itens
    </table>
    ";



    Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

  }
  ?>





  <form class="form-inline FormContato" role="form" method="post">


    <!--  ==============================================================  -->
    <!-- carrinho-->
    <!--  ==============================================================  -->
    <div class="container">
      <div class="row">

        <div class="col-xs-12 tb-lista-itens">
          <table class="table">

            <thead>
              <tr>
                <th>PRODUTO</th>
                <th>DESCRIÇÃO</th>
                <th class="text-center">QUANTIDADE</th>
                <th class="text-center">DELETAR</th>
              </tr>
            </thead>

            <tbody>

              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0){
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>

                  <tr>
                    <td>
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 153, 92, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </td>
                    <td class="col-xs-12" align="left"><?php Util::imprime($row[titulo]) ?></td>
                    <td class="text-center">
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-times-circle fa-2x"></i>
                      </a>
                    </td>
                  </tr>

                  <?php
                }
              }
              ?>


              <?php
              if(count($_SESSION[solicitacoes_servicos]) > 0){
                for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
                  $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                  ?>

                  <tr>
                    <td>
                      <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 153, 92, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                    </td>
                    <td class="col-xs-12" align="left"><?php Util::imprime($row[titulo]) ?></td>
                    <td class="text-center">
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                        <i class="fa fa-times-circle fa-2x"></i>
                      </a>
                    </td>
                  </tr>

                  <?php
                }
              }
              ?>
            </tbody>




          </table>



          <div class=" top15">

            <a href="<?php echo Util::caminho_projeto() ?>/produtos" title="Continuar orçando" class="btn btn-amarelo">
              Continuar orçando
            </a>
          </div>

        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!-- carrinho -->
    <!--  ==============================================================  -->



    <!--  ==============================================================  -->
    <!-- FORMULARIO ENVIE ORCAMENTO-->
    <!--  ==============================================================  -->
    <div class="container">
      <div class="row formulario">
        <div class="col-xs-12  top50 bottom30">
          <h3 class="bottom10"><span>SEUS DADOS</span></h3>



          <div class="col-xs-8 padding0">

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="nome"  placeholder="NOME" class="form-control fundo-form1 input100 input-lg" class="input100">
            </div>

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="email"  placeholder="E-MAIL" class="form-control fundo-form1 input100 input-lg">
            </div>


            <div class="clearfix"></div>

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control fundo-form1 input100 input-lg">
            </div>

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="fax"  placeholder="FAX" class="form-control fundo-form1 input100 input-lg">
            </div>

            <div class="clearfix"></div>

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control fundo-form1 input100 input-lg">
            </div>


            <div class="col-xs-6 top15 form-group">
              <input type="text" name="estado"  placeholder="ESTADO" class="form-control fundo-form1 input100 input-lg">
            </div>
            <div class="clearfix"></div>

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="cidade"  placeholder="CIDADE" class="form-control fundo-form1 input100 input-lg">
            </div>

            <div class="col-xs-6 top15 form-group">
              <input type="text" name="tipoPessoa"  placeholder="TIPO DE PESSOA" class="form-control fundo-form1 input100 input-lg">
            </div>

            <div class="clearfix"></div>


            <div class="col-xs-12 form-group top15">
              <textarea name="mensagem"  cols="30" rows="9" class="form-control fundo-form1 input100 input-lg" placeholder="MENSAGEM"></textarea>
            </div>

            <!-- form fale conosco -->
          </div>

          <!-- perguntas frequentes -->
          <div class=" col-xs-4 ">
            <p>Tem interesse em receber orçamento de outros produtos:</p>

            <div class="col-xs-8 form-group">
              <label class="radio-inline">
                <input type="radio" name="receber_orcamento" id="inlineRadio1" value="SIM"> SIM
              </label>
              <label class="radio-inline">
                <input type="radio" name="receber_orcamento" id="inlineRadio2" value="NÃO"> NÃO
              </label>
            </div>
          </div>

          <div class="col-xs-4">
            <div class="top20">
              <p>Se sim, assinale abaixo os produtos</p>
            </div>

            <?php
            $result = $obj_site->select("tb_categorias_produtos");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result))
              {
                ?>
                <div class="checkbox col-xs-12">
                  <label>
                    <input type="checkbox" name="categorias[]" value="<?php Util::imprime($row[titulo]) ?>">
                    <?php Util::imprime($row[titulo]) ?>
                  </label>
                </div>
                <?php
              }
            }
            ?>


            <div class="top20 ">
              <select class="form-control fundo-form1 input100 top5 input-lg" name="como_conheceu" >
                <option value="">Selecione</option>
                <option>Google</option>
                <option>Jornais</option>
                <option>Revistas</option>
                <option>Sites</option>
                <option>Fórum</option>
                <option>Notícias</option>
                <option>Outros</option>
              </select>
            </div>

            <div class="top45">
              <button type="submit" class="btn btn-default btn-formulario pull-right">ENVIAR</button>
            </div>
          </div>
          <!-- perguntas frequentes -->


        </div>
      </div>
    </div>
    <!--  ==============================================================  -->
    <!-- FORMULARIO ENVIE ORCAMENTO-->
    <!--  ==============================================================  -->


  </form>

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>







<script>
$(document).ready(function() {
  $('.FormContato').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'glyphicon glyphicon-ok',
      invalid: 'glyphicon glyphicon-remove',
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },

      receber_orcamento: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipoPessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      outros1: {
        validators: {
          notEmpty: {

          }
        }
      },
      interesse_produtos: {
        validators: {
          notEmpty: {

          }
        }
      },
      Local: {
        validators: {
          notEmpty: {

          }
        }
      },
      interesse: {
        validators: {
          notEmpty: {

          }
        }
      },
      Comprimento: {
        validators: {
          notEmpty: {

          }
        }
      },
      Largura: {
        validators: {
          notEmpty: {

          }
        }
      },
      Maiorprofundidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      Menorprofundidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      AreaTotal: {
        validators: {
          notEmpty: {

          }
        }
      },
      VolumeTotal: {
        validators: {
          notEmpty: {

          }
        }
      },
      outros: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereço: {
        validators: {
          notEmpty: {

          }
        }
      },
      complemento: {
        validators: {
          notEmpty: {

          }
        }
      },
      data: {
        validators: {
          date: {
            format: 'MM/DD/YYYY h:m A',
            message: 'The value is not a valid date'
          }
        }
      },

      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      cep: {
        validators: {
          notEmpty: {

          }
        }
      },

      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      }
    }
  });
});
</script>

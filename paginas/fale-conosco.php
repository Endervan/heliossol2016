  <?php 
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 16);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- FORMULARIO CONTATOS E ENDERENCO-->
  <!--  ==============================================================  -->
  <div class="container top560">
    <div class="row bottom20 contatos">
      <div class="col-xs-6">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active fale-conosco"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-fale1.png" alt="">FALE CONOSCO</a>
          </li>
          <li  class="trabalhe-conosco" role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-trabalhe1.png" alt="">TRABALHE CONOSCO</a></li>
        </ul>
      </div>

      <div class="clearfix"></div>


      

      <!-- ======================================================================= -->
      <!-- enderecos  -->
      <!-- ======================================================================= -->
      <?php require_once("./includes/dados_contato.php"); ?>
    <!-- ======================================================================= -->
    <!-- enderecos  -->
    <!-- ======================================================================= -->





    <!-- Tab panes -->
    <div class="tab-content">

      <!-- fale conosco -->
      <div role="tabpanel" class="tab-pane fade in active" id="home">


      <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if(isset($_POST[nome]))
        {
            $texto_mensagem = "
                                              Nome: ".($_POST[nome])." <br />
                                              Assunto: ".($_POST[assunto])." <br />
                                              Telefone: ".($_POST[telefone])." <br />
                                              Email: ".($_POST[email])." <br />
                                              Mensagem: <br />
                                              ".(nl2br($_POST[mensagem]))."
                                              ";
            Util::envia_email($config[email], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::envia_email($config[email_copia], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
        }
        ?>

        <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
          <div class="top50 bottom25">

            <div class="col-xs-8 fundo-formulario">
              <!-- formulario orcamento -->
              <div class="top20">  
                <div class="top15">
                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                    <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-6 form-group ">
                    <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                    <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                  </div>
                </div>


                <div class="clearfix"></div>   
                <div class="top15">
                  <div class="col-xs-6 form-group">
                    <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                    <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                  </div>

                  <div class="col-xs-6 form-group">
                   <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                   <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="">
                 </div>
               </div>


               <div class="clearfix"></div>    
               <div class="top15">
                <div class="col-xs-12 form-group">
                  <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                  <textarea name="mensagem" id="" cols="30" rows="16" class="form-control input100"></textarea>
                </div>
              </div>

              <div class="clearfix"></div>

              <div class="col-xs-12">

                <div class="pull-right  top30 bottom25">
                  <button type="submit" class="btn btn-formulario" name="btn_contato">
                    ENVIAR
                  </button>
                </div>
              </div>


            </div>
            <!-- formulario orcamento -->

          </div>

          <!--  ==============================================================  -->
          <!-- bg-como chegar-->
          <!--  ==============================================================  -->
          <div class="col-xs-4 ">
            <div class="bg-como-chegar"></div>
          </div>
          <!--  ==============================================================  -->
          <!-- FORMULARIO CONTATOS E ENDERENCO-->
          <!--  ==============================================================  -->

        </div>
      </form>



    </div>
    <!-- fale conosco -->


  </div>
  <!-- Tab panes -->

</div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO CONTATOS E ENDERENCO-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row maps">
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="800" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>


<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>






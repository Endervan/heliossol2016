<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
  <!-- FlexSlider -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 150,
        itemMargin: 8,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 230,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>
  <!-- FlexSlider -->


</head>




<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-servicos descricao-->
  <!--  ==============================================================  -->
  <div class="container top195">
    <div class="row">
      <div class="col-xs-12">
        <div class="bg-descricao-servicos"></div>
      </div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-servicos descricao-->
  <!--  ==============================================================  --> 


  <!-- ======================================================================= -->
  <!-- CATEGORIAS SERVICOS -->
  <!-- ======================================================================= -->
  <div class="container ">
    <div class="row">
      <div class="col-xs-12 categorias-servicos">
        <div id="slider" class="flexslider">
          <ul class="slides ">

           <?php
            $result = $obj_site->select("tb_servicos");
             if(mysql_num_rows($result) > 0){
               while ($row = mysql_fetch_array($result)) {
               ?> 
                  <li class="top15">
                     <div class="media">
                      <div class="media-left media-middle">
                        <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">                          
                          <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem_icone]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                        </a>
                      </div>
                      <div class="media-body">
                        <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">                          
                          <h3><?php Util::imprime($row[titulo]); ?></h3>
                        </a>
                      </div>
                    </div>  
                  </li>
               <?php 
               }
             }
             ?>

        </ul>   
     </div>
    </div>
  </div>  
</div>
<!-- ======================================================================= -->
<!-- CATEGORIAS SERVICOS -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- TIPOS DE SEVICOS DENTRO DETALHES-->
<!--  ==============================================================  -->
<div class="container bottom50">
  <div class="row tipos-servicos-detalhe">
  
      <div class="col-xs-5 top20">
      <h1><b><?php Util::imprime($dados_dentro[titulo]); ?></b></h1>
    </div>

    <div class="col-xs-3">
      
        
      <div class="pull-right">
        <div class="media">
          <div class="media-left media-middle">
            <a>
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone1.png" alt="">
            </a>
          </div>
          <div class="media-body  media-middle ">
            <h2 class="media-heading top10"> <?php Util::imprime($config[telefone1]); ?></h2>
          </div>
        </div>
      </div>

      <div class="pull-right">
        <div class="media">
          <div class="media-left media-middle">
            <a>
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone1.png" alt="">
            </a>
          </div>
          <div class="media-body  media-middle ">
            <h2 class="media-heading top10"> <?php Util::imprime($config[telefone2]); ?></h2>
          </div>
        </div>
      </div>


    </div>

    <div class="col-xs-4">
      <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
        <button type="button" class="btn btn-produtos-dentro">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-carrinho.png" alt="">
          SOLICITE UM ORÇAMENTO          
        </button>
      </a>
    </div>


    <div class="clearfix"></div>

    <div class="col-xs-12">
      <div class="servicos-detalhe">
        <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 1140, 421, array("class"=>"input100 top20", "alt"=>"$row[titulo]")) ?>
      </div>

      <div class="top30 bottom25">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>

      </div>


    <div class="col-xs-4">
      <a href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
        <button type="button" class="btn btn-produtos-dentro">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-carrinho.png" alt="">
          SOLICITE UM ORÇAMENTO          
        </button>
      </a>
    </div>


     <div class="col-xs-3">
        <div class="media">
          <div class="media-left media-middle">
            <a>
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone1.png" alt="">
            </a>
          </div>
          <div class="media-body  media-middle ">
            <h2 class="media-heading top10"> <?php Util::imprime($config[telefone1]); ?></h2>
          </div>
        </div>

        <div class="media">
          <div class="media-left media-middle">
            <a>
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone1.png" alt="">
            </a>
          </div>
          <div class="media-body  media-middle ">
            <h2 class="media-heading top10"> <?php Util::imprime($config[telefone2]); ?></h2>
          </div>
        </div>
     </div>
    

  </div>  
</div> 
<!--  ==============================================================  -->
<!-- TIPOS DE SEVICOS DENTRO DETALHES-->
<!--  ==============================================================  --> 





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

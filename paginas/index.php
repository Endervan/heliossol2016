<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>




<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>




   <script>
    jQuery(document).ready(function($) {
            // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
            // it's recommended to disable them when using autoHeight module
            $('#content-slider-1').royalSlider({
              autoHeight: true,
              arrowsNav: false,
              fadeinLoadedSlide: false,
              controlNavigationSpacing: 0,
              controlNavigation: 'tabs',
              imageScaleMode: 'none',
              imageAlignCenter: false,
              loop: false,
              loopRewind: true,
              numImagesToPreload: 6,
              keyboardNavEnabled: true,
              usePreloader: false,
              autoPlay: {
                    // autoplay options go gere
                    enabled: true,
                    pauseOnHover: true
                  }

                });
          });
        </script>








  <script type="text/javascript">
    $(document).ready(function() {
      $("#carousel-gallery").touchCarousel({
        itemsPerPage:5,
        scrollbar: false,
        scrollbarAutoHide: true,
        scrollbarTheme: "dark",
        pagingNav: true,
        snapToItems: true,
        scrollToLast: false,
        useWebkit3d: true,
        loopItems: true
      });
    });
  </script>
  <!-- XXXX LAYER SLIDER XXXX -->



  <!-- FlexSlider -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 150,
        itemMargin: 8,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 142,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>
  <!-- FlexSlider -->













</head>



<body>








  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->
  <?php require_once("./includes/banner_index.php") ?>
  <!-- ======================================================================= -->
  <!-- slider -->
  <!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- CATEGORIAS HOME -->
<!-- ======================================================================= -->
<div class="container ">
  <div class="row">
    <div class="col-xs-12 lista-categorias">
      <div id="slider" class="flexslider">
        <ul class="slides slider-prod">

          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
          ?>
              <li class="<?php if($i++ % 2 == 0){ echo 'categorias'; }else{ echo 'categorias1'; } ?>">
                 <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                   <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
                   <h3><?php Util::imprime($row[titulo]); ?></h3>
                 </a>
               </li>
          <?php
            }
          }
          ?>

       </ul>
     </div>
   </div>
 </div>
</div>
<!-- ======================================================================= -->
<!-- CATEGORIAS HOME -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- produto home-->
<!--  ==============================================================  -->
<div class="container-fluid bg-produtos-home">
  <div class="row">
    <div class="container">
      <div class="row">

       <div class="col-xs-3 top395 bottom280">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-produtos-descricao.png" alt="">
      </div>

      <div class="col-xs-9">


        <?php
        $result = $obj_site->select("tb_produtos", "order by rand() limit 9");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while ($row = mysql_fetch_array($result)) {
          ?>
            <div class="col-xs-4">
              <div class="thumbnail produtos-home">


               <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 232, 129, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>


               <div class="produto-hover">
                 <div class="col-xs-6 text-center">
                   <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba mais">
                     <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.jpg" alt="" class="input100">
                   </a>
                 </div>
                 <div class="col-xs-6 hover-btn-orcamento">
                   <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                     <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.jpg" alt="" class="input100">
                   </a>
                 </div>
               </div>

               <div class="caption">
                 <h1><?php Util::imprime($row[titulo]); ?></h1>
               </div>
             </div>
           </div>
          <?php
            if($i == 2){
              echo '<div class="clearfix"></div>';
              $i = 0;
            }else{
              $i++;
            }

          }
        }
        ?>


        </div>

      </div>
    </div>
  </div>
</div>

<!--  ==============================================================  -->
<!-- produto home-->
<!--  ==============================================================  -->







<!--  ==============================================================  -->
<!-- SOBRE HELIOSSOL-->
<!--  ==============================================================  -->
<div class="container-fluid bg-sobre">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-xs-8">
          <div class="decricao-sobre top320 ">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
            <p><?php Util::imprime($row[descricao], 4000); ?></p>
         </div>
         <a class="btn btn-default btn-transparente top45 right30" href="<?php echo Util::caminho_projeto() ?>/empresa" alt=""title="">MAIS SOBRE</a>
         <a class="btn btn-default btn-transparente top45 right30" href="<?php echo Util::caminho_projeto() ?>/fale-conosco" alt=""title="">COMO CHEGAR</a>
         <a class="btn btn-default btn-transparente top45 " href="<?php echo Util::caminho_projeto() ?>/orcamento-produtos" alt=""title="">SOLICITAR UM ORÇAMENTO</a>
       </div>
     </div>
   </div>
 </div>
</div>
<!--  ==============================================================  -->
<!-- SOBRE HELIOSSOL-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- dicas home-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row top40">

    <div class="col-xs-8 padding0">

      <?php
      $result = $obj_site->select("tb_noticias", "order by rand() limit 3");
      if (mysql_num_rows($result) > 0) {
        while ($row = mysql_fetch_array($result)) {
        ?>
          <div class="col-xs-4">
            <div class="thumbnail dicas-home">
              <div class="pull-right bottom10"></div>
              <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 230, 166, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption">
                <a href="<?php echo Util::caminho_projeto() ?>/noticia/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                </a>
            </div>
          </div>
        </div>
        <?php
        }
      }
      ?>




  </div>




  <div class="col-xs-4 padding0">
    <div class="col-xs-12 top50">
      <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-nossas-dicas.png" alt="">
    </div>
  </div>



 </div>
</div>
<!--  ==============================================================  -->
<!-- dicas home-->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>




<!-- ======================================================================= -->
  <!-- hover nos produtos   -->
  <!-- ======================================================================= -->
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {

      $(".produto-hover").hover(
        function () {
          $(this).stop().animate({opacity:1});
        },
        function () {
          $(this).stop().animate({opacity:0});
        }
        );

    });
  </script>

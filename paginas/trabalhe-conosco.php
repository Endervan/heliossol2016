<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 17);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- FORMULARIO CONTATOS E ENDERENCO-->
  <!--  ==============================================================  -->
  <div class="container top560">
    <div class="row bottom20 contatos">
      <div class="col-xs-6">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class=" fale-conosco"><a href="<?php echo Util::caminho_projeto() ?>/fale-conosco" ><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-fale1.png" alt="">FALE CONOSCO</a>
          </li>
          <li  class="trabalhe-conosco active" role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-trabalhe1.png" alt="">TRABALHE CONOSCO</a></li>
        </ul>
      </div>


      <div class="clearfix"></div>


    <!-- ======================================================================= -->
    <!-- dados do contato  -->
    <!-- ======================================================================= -->
    <?php require_once("./includes/dados_contato.php") ?>
    <!-- ======================================================================= -->
    <!-- dados do contato  -->
    <!-- ======================================================================= -->


    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {

      if(!empty($_FILES[curriculo][name])):
        $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
        $texto = "Anexo: ";
        $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
        $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
      endif;

            $texto_mensagem = "
                              Nome: ".$_POST[nome]." <br />
                              Telefone: ".$_POST[telefone]." <br />
                              Email: ".$_POST[email]." <br />
                              Escolaridade: ".$_POST[escolaridade]." <br />
                              Cargo: ".$_POST[cargo]." <br />
                              Área: ".$_POST[area]." <br />
                              Cidade: ".$_POST[cidade]." <br />
                              Estado: ".$_POST[estado]." <br />
                              Mensagem: <br />
                              ".nl2br($_POST[mensagem])."

                              <br><br>
                              $texto
                              ";


            Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
            Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
            Util::alert_bootstrap("Obrigado por entrar em contato.");
            unset($_POST);
    }
    ?>



   <!-- Tab panes -->
   <div class="tab-content">

     <!-- trabalhe conosco -->
     <div role="tabpanel" class="tab-pane fade in active" id="profile">

      <form class="form-inline FormContato" role="form" method="post" enctype="multipart/form-data">
        <div class="top50 bottom25">

          <div class="col-xs-8 fundo-formulario">
            <!-- formulario orcamento -->
            <div class="top20">
              <div class="clearfix"></div>
              <div class="">
                <div class="col-xs-6 form-group ">
                  <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                  <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
                </div>

                <div class="col-xs-6 form-group ">
                  <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                  <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
                </div>
              </div>


              <div class="clearfix"></div>
              <div class="top15">
                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                  <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
                </div>

                <div class="col-xs-6 form-group">
                  <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                  <input type="text" name="escolaridade" class="form-control fundo-form1 input100" placeholder="">
                </div>
              </div>

              <div class="clearfix"></div>
              <div class="top15">
               <div class="col-xs-6 form-group">
                <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                <input type="text" name="cargo" class="form-control fundo-form1 input100" placeholder="">
              </div>

              <div class="col-xs-6 form-group">
                <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                <input type="text" name="area" class="form-control fundo-form1 input100" placeholder="">
              </div>
            </div>

            <div class="clearfix"></div>
            <div class="top15">
             <div class="col-xs-6 form-group">
              <label class="glyphicon glyphicon-home"> <span>Cidade</span></label>
              <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
            </div>
            <div class="col-xs-6 form-group">
              <label class="glyphicon glyphicon-globe"> <span>Estado</span></label>
              <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
            </div>
          </div>

          <div class="clearfix"></div>
          <div class="top15">
            <div class="col-xs-12 form-group">
              <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
              <input type="file" name="curriculo" class="form-control fundo-form1 input100" placeholder="">
            </div>
          </div>


          <div class="clearfix"></div>
          <div class="top15">
            <div class="col-xs-12 form-group">
              <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
              <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12">
            <div class="pull-right  top30 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>


        </div>
        <!-- formulario orcamento -->

      </div>
      <!--  ==============================================================  -->
      <!-- bg-como chegar-->
      <!--  ==============================================================  -->
      <div class="col-xs-4 ">
        <div class="bg-como-chegar"></div>
      </div>
      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS E ENDERENCO-->
      <!--  ==============================================================  -->

    </div>
  </form>
</div>
<!-- trabalhe conosco -->


</div>
<!-- Tab panes -->


</div>
</div>



<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
  <div class="row maps">
    <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="800" frameborder="0" style="border:0" allowfullscreen></iframe>
  </div>
</div>
</div>


<!--  ==============================================================  -->
<!-- mapa -->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>





<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

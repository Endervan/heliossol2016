<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>


<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>
  <!-- ======================================================================= -->
  <!-- hover nos produtos   -->
  <!-- ======================================================================= -->
  <script type="text/javascript">
    $(document).ready(function() {

      $(".produto-hover").hover(
        function () {
          $(this).stop().animate({opacity:1});
        },
        function () {
          $(this).stop().animate({opacity:0});
        }
        );

    });
  </script>




</head>




<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
    .bg-interna{
      background: #f3dd92 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
    }
</style>


<body class="bg-interna">

  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!-- bg-produtos descricao-->
  <!--  ==============================================================  -->
  <div class="container top150">
    <div class="row">
      <div class="bg-descricao-produtos"></div>
    </div>
  </div> 
  <!--  ==============================================================  -->
  <!-- bg-produtos descricao-->
  <!--  ==============================================================  --> 



  <!--  ==============================================================  -->
  <!--   barra pesquisas e categorias -->
  <!--  ==============================================================  -->
  <?php require_once("./includes/filtro_produtos.php"); ?>
  <!--  ==============================================================  -->
  <!--   barra pesquisas e categorias -->
  <!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- MEnu lateral-->
<!--  ==============================================================  -->
<div class="container top30">
  <div class="row">

    

    <div class="col-xs-3 menu-produtos top20">
      <?php require_once("./includes/menu_produtos.php") ?>
    </div>
    <!--  ==============================================================  -->
    <!--  MEnu lateral-->
    <!--  ==============================================================  -->




    <!--  ==============================================================  -->
    <!-- produto dentro-->
    <!--  ==============================================================  -->
    <div class="col-xs-9 produto-dentro top15">
      <h5><?php Util::imprime($dados_dentro[titulo]); ?></h5>

      <div class="col-xs-7 top20  padding0">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

          <!-- Indicators -->
          <ol class="carousel-indicators carroucel-produtos">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "and id_produto = '$dados_dentro[idproduto]' ");
             if(mysql_num_rows($result) > 0){
              $i = 0;
               while ($row = mysql_fetch_array($result)) {
                $imagens[] = $row;
               ?> 
                  <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
               <?php 
                $i++;
               }
            }
            ?>
          </ol>

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            
            
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "and id_produto = '$dados_dentro[idproduto]' ");
             if(mysql_num_rows($result) > 0){
              $i = 0;
               while ($row = mysql_fetch_array($result)) {
              ?>
                  <div class="item <?php if($i == 0){ echo 'active'; } ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 494, 331, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
                  </div>
              <?php
                $i++;
              }
            }
            ?>

            
          </div>

          <!-- Controls -->
          <a class="left carousel-control controles-produtos" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control controles-produtos" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>


      <!--  ==============================================================  -->
      <!-- produto dentro descricao-->
      <!--  ==============================================================  -->
      <div class="col-xs-5 detalhes-produtos top20 ">
        <h4>MARCA:<span><?php Util::imprime($dados_dentro[marca]); ?></span></h4>

      

        <div class="top10">
          <h4>CATEGORIA:<span> <?php Util::imprime( Util::troca_value_nome($dados_dentro[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ) ?> </span></h4>
        </div>

        <div class="top10">
          <h4>SUB-CATEGORIA:<span><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_subcategoriaproduto], "tb_subcategorias_produtos", "idsubcategoriaproduto", "titulo") ) ?></span></h4>
        </div>

        <div class="media top15">
          <div class="media-left media-middle">
            <a>
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone.png" alt="">
            </a>
          </div>
          <div class="media-body  media-middle ">
            <h2 class="media-heading top10"> ATENDIMENTO: </h2>
          </div>
        </div>

        <div class="top20">
          <h4>
            <span>
                  <?php Util::imprime($config[telefone1]); ?>
                  
                  <?php if (!empty($config[telefone2])): ?>
                      & <?php Util::imprime($config[telefone2]); ?>
                  <?php endif ?>
                    
            </span>
          </h4>
        </div>

        <div class="top20">
          <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
            <button type="button" class="btn btn-produtos-dentro">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-carrinho.png" alt="">
              SOLICITE UM ORÇAMENTO          
            </button>
          </a>
        </div>

      </div>


      <div class="col-xs-12 descricao-produtos-dentro">
        <div class="top40">
          <h1>DESCRIÇÃO:</h1>
        </div>
        <div class="top15">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
      </div>

     

      

  </div>
  <!--  ==============================================================  -->
  <!-- produto dentro-->
  <!--  ==============================================================  -->
</div>
</div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>

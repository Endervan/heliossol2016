<!-- colocando topo sobre slider home -->
<div class="topo-site">

	<!--  ==============================================================  -->
	<!-- contatos -->
	<!--  ==============================================================  -->
	<div class="container top5">
		<div class="row">
			<div class=" col-xs-12">





				<!-- carrinho -->
				<div class="pull-right left10">
					<!-- botao com 2 linhas texto-->
					<a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="tam-btn-topo btn btn-amarelo">

						<i class="right10 pull-left"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-carrinho.png" alt=""></i>
						<span class="right10 pull-left">MEU <br>ORÇAMENTO</span>
						<i class="fa fa-angle-down fa-2x pull-right" aria-hidden="true"></i>
					</a>
					<!-- botao com 2 linhas texto-->
					<div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

						<h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?>)</h6>


						<?php
						if(count($_SESSION[solicitacoes_produtos]) > 0)
						{
							for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
							{
								$row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
								?>
								<div class="lista-itens-carrinho">
									<div class="col-xs-2">
										<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
									</div>
									<div class="col-xs-8">
										<h1><?php Util::imprime($row[titulo]) ?></h1>
									</div>
									<div class="col-xs-1">
										<a href="<?php echo Util::caminho_projeto() ?>/orcamento-produtos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
									</div>
								</div>
								<?php
							}
						}
						?>


						<?php
						if(count($_SESSION[solicitacoes_servicos]) > 0)
						{
							for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
							{
								$row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
								?>
								<div class="lista-itens-carrinho">
									<div class="col-xs-2">
										<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
									</div>
									<div class="col-xs-8">
										<h1><?php Util::imprime($row[titulo]) ?></h1>
									</div>
									<div class="col-xs-1">
										<a href="<?php echo Util::caminho_projeto() ?>/orcamento-produtos/?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
									</div>
								</div>
								<?php
							}
						}
						?>


						<div class="text-right bottom20" >
							<a href="<?php echo Util::caminho_projeto() ?>/orcamento-produtos" title="Finalizar" class="btn btn-amarelo" >
								FINALIZAR
							</a>
						</div>

					</div>
				</div>
				<!-- carrinho -->


				<div class="telefones_topo left5 top10 pull-right">
					<h1><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h1>
				</div>
				<div class="telefones_topo left5 top10 pull-right">
					<h1><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h1>
				</div>
				<!-- contatos -->
				<div class="pull-right left15">
					<!-- botao com 2 linhas texto-->
					<a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="tam-btn-topo btn btn-amarelo">
						<i class="right10 pull-left"><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone.png" alt=""></i>
						<span class="right10 pull-left">CENTRAL <br> DE ATENDIMENTO</span>
						<i class="fa fa-angle-down fa-2x pull-right" aria-hidden="true"></i>
					</a>
					<!-- botao com 2 linhas texto-->

					<ul class="dropdown-menu contatos-topo">
						<li><h2><?php Util::imprime($config[telefone1]); ?></h2></li>

						<?php if (!empty($config[telefone2])): ?>
							<li><h2><?php Util::imprime($config[telefone2]); ?></h2></li>
						<?php endif ?>

						<?php if (!empty($config[telefone3])): ?>
							<li><h2><?php Util::imprime($config[telefone3]); ?></h2></li>
						<?php endif ?>

						<?php if (!empty($config[telefone4])): ?>
							<li><h2><?php Util::imprime($config[telefone4]); ?></h2></li>
						<?php endif ?>

					</ul>
				</div>
				<!-- contatos -->
				
				<!-- busca avancada -->
				<div class=" dropdown pull-right left15">
					<!-- botao com 2 linhas texto-->
					<a href="#" class=" btn btn-amarelo tam-btn-topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-search fa-2x right10 pull-left"></i>
						<span class="right10 pull-left">BUSCA <br>AVANÇADA</span>
						<i class="fa fa-angle-down fa-2x pull-right" aria-hidden="true"></i>
					</a>
					<!-- botao com 2 linhas texto-->
					<div class="dropdown-menu form-busca-topo">
						<form class="navbar-form navbar-left " role="search" method="post" action="<?php echo Util::caminho_projeto() ?>/produtos/">
							<div class="form-group">
								<input type="text" class="form-control" name="busca_topo" placeholder="O que está procurando?">
							</div>
							<button type="submit" class="btn btn-amarelo">Buscar</button>
						</form>
					</div>
				</div>
				<!-- busca avancada -->

			</div>
		</div>
	</div>
	<!--  ==============================================================  -->
	<!-- contatos -->
	<!--  ==============================================================  -->






	<!--  ==============================================================  -->
	<!-- menu topo -->
	<!--  ==============================================================  -->
	<div class="container bg-topo top10">
		<div class="row">
			<!-- logo -->
			<div class="col-xs-2 posicao-logo">
				<a href="<?php echo Util::caminho_projeto() ?>/" title="">
					<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
				</a>
			</div>
			<!-- logo -->

			<div class="col-xs-10 menu-topo">
				<!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
				<div id="navbar">
					<ul class="nav navbar-nav  navbar-right text-center">
						<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-home.png" alt="">
								<span>HOME</span>
							</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>" >
							<a href="<?php echo Util::caminho_projeto() ?>/empresa" >
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa.png" alt="">
								<span>EMPRESA</span>
							</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?> ">
							<a href="<?php echo Util::caminho_projeto() ?>/produtos">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produtos.png" alt="">
								<span>PRODUTOS</span>
							</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?> ">
							<a href="<?php echo Util::caminho_projeto() ?>/servicos">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-servicos.png" alt="">
								<span>SERVIÇOS</span>
							</a>
						</li>

						<li class="<?php if(Url::getURL( 0 ) == "noticias" or Url::getURL( 0 ) == "noticia"){ echo "active"; } ?> ">
							<a href="<?php echo Util::caminho_projeto() ?>/noticias">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-noticias.png" alt="">
								<span>NOTÍCIAS</span>
							</a>
						</li>


						<li class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/fale-conosco">
								<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-fale.png" alt="">
								<span>FALE CONOSCO</span>
							</a>
						</li>

					</ul>

				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!--  ==============================================================  -->
	<!-- menu topo -->
	<!--  ==============================================================  -->


</div>

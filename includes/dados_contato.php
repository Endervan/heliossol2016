<div class="top30">

        <div class="col-xs-3">
          <div class="media">
            <div class="media-left media-middle">
                <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone-fale.jpg" alt="">
            </div>
            <div class="media-body media-middle">
              <div class="">  
                <h5 class="media-heading"><?php Util::imprime($config[telefone1]); ?></h5>
              </div>

            </div>
          </div>
        </div>
        


        <?php if (!empty($config[telefone2])): ?>
        <div class="col-xs-3">
          <div class="media">
            <div class="media-left media-middle">
                <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone-fale.jpg" alt="">
            </div>
            <div class="media-body media-middle">
              <div class="">  
                <h5 class="media-heading"><?php Util::imprime($config[telefone2]); ?></h5>
              </div>

            </div>
          </div>
        </div>
        <?php endif; ?>

        <div class="clearfix"></div>

        <?php if (!empty($config[telefone3])): ?>
        <div class="col-xs-3 top15">
          <div class="media">
            <div class="media-left media-middle">
                <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone-fale.jpg" alt="">
            </div>
            <div class="media-body media-middle">
              <div class="">  
                <h5 class="media-heading"><?php Util::imprime($config[telefone3]); ?></h5>
              </div>

            </div>
          </div>
        </div>
        <?php endif; ?>



        <?php if (!empty($config[telefone4])): ?>
        <div class="col-xs-3 top15">
          <div class="media">
            <div class="media-left media-middle">
                <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-phone-fale.jpg" alt="">
            </div>
            <div class="media-body media-middle">
              <div class="">  
                <h5 class="media-heading"><?php Util::imprime($config[telefone4]); ?></h5>
              </div>

            </div>
          </div>
        </div>
        <?php endif; ?>






      </div> 
      <div class="clearfix"></div>  

      <div class="col-xs-4 top20">

        <div class="media bottom25 top20">
          <div class="media-left media-middle">
            <a href="#">
              <img class="media-object left10" src="<?php echo Util::caminho_projeto() ?>/imgs/icon-home-fale.jpg" alt="">
            </a>
          </div>
          <div class="media-body media-middle ">
           <p><?php Util::imprime($config[endereco]); ?></p>
         </div>
       </div>
     </div>


     <div class="col-xs-4 top30">
      <a href="javascript:void(0);" class="btn btn-primary btn-amarelo-fale" onclick="$('html,body').animate({scrollTop: $('.maps').offset().top}, 2000);">
        COMO CHEGAR
      </a>
    </div>  
    
    <div class="clearfix"></div>
<div class="clearfix"></div>
<div class="top50"></div>
<div class="container-fluid rodape">
	<div class="row">

		<!-- menu -->
		<div class="container top30">
			<div class="row">
				<div class="col-xs-12 bg-menu-rodape">
					<ul class="menu-rodape">
						<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/">HOME</a>
						</li>
						<li>
							<a  class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>
						<li >
							<a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>
						<li >
							<a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
							</a>
						</li>
						<li >
							<a class="<?php if(Url::getURL( 0 ) == "noticias" or Url::getURL( 0 ) == "noticia"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/noticias">NOTÍCIAS
							</a>
						</li>
						<li >
							<a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO</a>
						</li>
						<li class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>">
							<a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a>
						</li>
					</ul>
				</div>

				<!-- menu -->

				<!-- logo, endereco, telefone -->
				<div class="col-xs-2 top10">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-rodape.png" alt="">
					</a>
				</div>

				<div class="col-xs-8 top30">
					<div class="bottom15">
						<p><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]); ?></p>
					</div>
					<div class="top20 bottom15">
						<p>
							<i class="glyphicon glyphicon-phone"></i>

							<?php Util::imprime($config[telefone1]); ?>

							<?php if (!empty($config[telefone2])): ?>
								/ <?php Util::imprime($config[telefone2]); ?>
							<?php endif ?>

							<?php if (!empty($config[telefone3])): ?>
								/ <?php Util::imprime($config[telefone3]); ?>
							<?php endif ?>

							<?php if (!empty($config[telefone4])): ?>
								/ <?php Util::imprime($config[telefone4]); ?>
							<?php endif ?>

						</p>
					</div>
				</div>



				<div class="col-xs-2 top30 text-right">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" class="pull-left top30" >
							<p class="bottom15"><i class="fa fa-google-plus"></i></p>
						</a>
					<?php } ?>


					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>
				</div>
				<!-- logo, endereco, telefone -->

			</div>
		</div>
	</div>
</div>


<div class="container-fluid rodape-preto">
	<div class="row">
		<div class="col-xs-12 text-center top15 bottom15">
			<h5>© Copyright HELIOSSOL</h5>
		</div>
	</div>
</div>

<div class="container">
    <div class="row">


      <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

          <div class="fundo-amarelo">
              <div class="col-xs-3 top25 text-center">
                <h4>FILTRAR PRODUTOS:</h4>
              </div>
              <div class="col-xs-3 top15 text-left">

                <?php Util::cria_select_bd("tb_categorias_produtos", "idcategoriaproduto", "titulo", "id_categoriaproduto", $dados[id_categoriaproduto], "btn btn-default btn-drop col-xs-12 input-lg") ?>

            </div>

            <div class="col-xs-3 top15 text-left">
              <select name="id_subcategoriaproduto" id="id_subcategoriaproduto" class="btn btn-default btn-drop col-xs-12 input-lg"></select>  
            </div>

            <div class="col-xs-3 top15">
               <div class="input-group input-group-lg">
                <input type="text" class="form-control form" name="busca_topo" placeholder="TERMO DA BUSCA">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                </span>
               </div>       
            </div>

        </div>

      </form>

  </div>
</div>
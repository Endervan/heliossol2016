-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: heliossol2016.mysql.dbaas.com.br
-- Generation Time: 02-Jun-2016 às 20:43
-- Versão do servidor: 5.6.21-69.0-log
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `heliossol2016`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'Mobile Banner 1', '2604201611391114671634..jpg', 'SIM', NULL, '2', 'mobile-banner-1', 'http://www.heliossol.com.br/', 'Com a Heliossol, você não precisa se preocupar, pois sua Piscina de fibra chega em sua casa, pronta para instalação exigindo assim, menos subcontratastes.', NULL),
(2, 'Mobile Banner 2', '2604201611401352856838..jpg', 'SIM', NULL, '2', 'mobile-banner-2', 'http://www.heliossol.com.br/', 'Com a Heliossol, você não precisa se preocupar, pois sua Piscina de fibra chega em sua casa, pronta para instalação exigindo assim, menos subcontratastes.', NULL),
(3, 'Index - banner 1', '0106201606401176798800..jpg', 'SIM', NULL, '1', 'index--banner-1', 'http://www.heliossol.com.br/', 'Com a Heliossol, você não precisa se preocupar, pois sua Piscina de fibra chega em sua casa, pronta para instalação exigindo assim, menos subcontratastes.', NULL),
(4, 'Index banner 2', '1804201603091174790777..jpg', 'SIM', NULL, '1', 'index-banner-2', 'http://www.heliossol.com.br/', 'Com a Heliossol, você não precisa se preocupar, pois sua Piscina de fibra chega em sua casa, pronta para instalação exigindo assim, menos subcontratastes. PreviousNext', NULL),
(5, 'TESTE', '0106201609061223287236..jpg', 'NAO', NULL, '1', 'teste', '', 'TESTE', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '2204201609421117622282.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Produtos', '0206201606055274468730.jpg', NULL, 'produtos', 'SIM', NULL),
(3, 'Produtos dentro', '2204201609481193399127.jpg', NULL, 'produtos-dentro', 'SIM', NULL),
(4, 'Serviços', '2204201609511154167252.jpg', NULL, 'servicos', 'SIM', NULL),
(5, 'Serviços dentro', '2204201609511362802289.jpg', NULL, 'servicos-dentro', 'SIM', NULL),
(6, 'Notícias', '2204201609411234209969.jpg', NULL, 'noticias', 'SIM', NULL),
(7, 'Notícias dentro', '2204201609581333756205.jpg', NULL, 'noticias-dentro', 'SIM', NULL),
(8, 'Fale conosco', '2204201609591224400295.jpg', NULL, 'fale-conosco', 'SIM', NULL),
(9, 'Trabalhe conosco', '2204201610031148732591.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(10, 'Orçamentos', '2204201610491290742857.jpg', NULL, 'orcamentos', 'SIM', NULL),
(11, 'Orçamento de piscinas', '2304201611391193025755.jpg', NULL, 'orcamento-de-piscinas', 'SIM', NULL),
(12, 'Orçamento de produtos', '2304201611441246481720.jpg', NULL, 'orcamento-de-produtos', 'SIM', NULL),
(13, 'Mobile - Empresa', '2704201602511114800555.jpg', NULL, 'mobile--empresa', 'SIM', NULL),
(14, 'Mobile - Notícias', '2704201602421128821976.jpg', NULL, 'mobile--noticias', 'SIM', NULL),
(15, 'Mobile - Notícias dentro', '2704201601151396547091.jpg', NULL, 'mobile--noticias-dentro', 'SIM', NULL),
(16, 'Mobile - Serviços', '2704201604291113456087.jpg', NULL, 'mobile--servicos', 'SIM', NULL),
(17, 'Mobile - Serviços dentro', '2704201604491275952331.jpg', NULL, 'mobile--servicos-dentro', 'SIM', NULL),
(18, 'Mobile - Produtos', '2804201611011206778778.jpg', NULL, 'mobile--produtos', 'SIM', NULL),
(19, 'Mobile - Produtos dentro', '2804201611011134614862.jpg', NULL, 'mobile--produtos-dentro', 'SIM', NULL),
(20, 'Mobile - Orçamento Hoteis', '0205201601371367716652.jpg', NULL, 'mobile--orcamento-hoteis', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'AQUECIMENTO SOLAR', NULL, '1804201603221326960860.png', 'SIM', NULL, 'aquecimento-solar', '', '', ''),
(74, 'AQUECEDORES ELÉTRICOS', NULL, '1804201603211272181447.png', 'SIM', NULL, 'aquecedores-eletricos', '', '', ''),
(76, 'BOMBEAMENTO E PRESSÃO', NULL, '1804201603221235154182.png', 'SIM', NULL, 'bombeamento-e-pressao', '', '', ''),
(77, 'CONSTRUÇÃO DE PISCINAS', NULL, '1804201603231118321922.png', 'SIM', NULL, 'construcao-de-piscinas', NULL, NULL, NULL),
(78, 'PRODUTOS PARA PISCINAS', NULL, '1804201603231279479520.png', 'SIM', NULL, 'produtos-para-piscinas', NULL, NULL, NULL),
(79, 'TRATAMENTO PARA PISCINAS', NULL, '1804201603231356950292.png', 'SIM', NULL, 'tratamento-para-piscinas', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_produtos`
--

CREATE TABLE `tb_comentarios_produtos` (
  `idcomentarioproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL,
  `nome_tb_sitemaps` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `email_gmail` varchar(255) DEFAULT NULL,
  `senha_gmail` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `titulo`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`, `nome_tb_sitemaps`, `google_plus`, `email_gmail`, `senha_gmail`) VALUES
(1, NULL, 'SIM', 0, '', 'Asa Norte - 713 bloco E loja 56 - Brasília - DF', '(61) 3340-9030', '(61) 3349-4768', 'ronilda@heliossol.com.br', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15360.163332500319!2d-47.896246!3d-15.748979!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc9e206fb5ece6dc2!2sHeliossol+Aquecedor+Solar+DF!5e0!3m2!1spt-BR!2sbr!4v1463848717147', NULL, NULL, 'marcio@masmidia.com.br, junior@homewebbrasil.com.br, angela.homeweb@gmail.com, alan@heliossol.com.br,', '', '', NULL, 'https://plus.google.com/+HeliossolAquecedorSolarBras%C3%ADlia', 'cliente.masmidia@gmail.com', 'masmidia102030');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `descricao`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(1, 'João Paulo', '<p>\r\n	Jo&atilde;o Paulo Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'joao-paulo', '1703201603181368911340..jpg'),
(2, 'Ana Júlia', '<p>\r\n	Ana J&uacute;lia&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'ana-julia', '0803201607301356333056..jpg'),
(3, 'Tatiana Alves', '<p>\r\n	Tatiana&nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard ummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to</p>', 'SIM', NULL, 'tatiana-alves', '0803201607301343163616.jpeg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Index', '<p style="text-align: justify;">\r\n	A Heliossol &eacute; a primeira empresa de energia solar de Bras&iacute;lia com o verdadeiro comprometimento com o meio ambiente e a sustentabili dade.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Al&eacute;m de produtos ecologicamente corretos e profissionais qualificados, o cliente Heliossol conta com not&iacute;cias sobre ecologia, link para s ites e informa&ccedil;&atilde;o gratuita. A empresa conta com engenheiro agr&ocirc;nomo, engenheiro civil, colabora&ccedil;&atilde;o de ambientalistas e ecologistas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Desta forma conseguimos oferecer para nossos clientes n&atilde;o s&oacute; pre&ccedil;o, mas tamb&eacute;m solu&ccedil;&otilde;es ecologicamente corretas para o dia a dia da sua fam&iacute;lia, com produtos de alt&iacute;ssima qualidade e efici&ecirc;ncia e que respeitam o meio ambiente.</p>', 'SIM', 0, '', '', '', 'index', NULL, NULL, NULL),
(2, 'Empresa', '<p>\r\n	A Heliossol &eacute; a primeira empresa de energia solar de Bras&iacute;lia com o verdadeiro comprometimento com o meio ambiente e a sustentabili dade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Al&eacute;m de produtos ecologicamente corretos e profissionais qualificados, o cliente Heliossol conta com not&iacute;cias sobre ecologia, link para s ites e informa&ccedil;&atilde;o gratuita. A empresa conta com engenheiro agr&ocirc;nomo, engenheiro civil, colabora&ccedil;&atilde;o de ambientalistas e ecologistas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Desta forma conseguimos oferecer para nossos clientes n&atilde;o s&oacute; pre&ccedil;o, mas tamb&eacute;m solu&ccedil;&otilde;es ecologicamente corretas para o dia a dia da sua fam&iacute;lia, com produtos de alt&iacute;ssima qualidade e efici&ecirc;ncia e que respeitam o meio ambiente.</p>', 'SIM', 0, '', '', '', 'empresa', NULL, NULL, NULL),
(3, 'Empresa - Consultoria', '<p>\r\n	Trabalhamos com consultoria para a sua obra de piscina e aquecimento solar.</p>', 'SIM', 0, '', '', '', 'empresa--consultoria', NULL, NULL, NULL),
(4, 'Empresa - Especialização em Aquecimento Solar', '<p>\r\n	Especializa&ccedil;&atilde;o em Aquecimento Solar</p>', 'SIM', 0, '', '', '', 'empresa--especializacao-em-aquecimento-solar', NULL, NULL, NULL),
(5, 'Empresa - Especialização em Construção de Piscinas', '<p>\r\n	Especializa&ccedil;&atilde;o em Constru&ccedil;&atilde;o de Piscinas</p>', 'SIM', 0, '', '', '', 'empresa--especializacao-em-construcao-de-piscinas', NULL, NULL, NULL),
(6, 'Empresa - Mensagem rodape', '<p>\r\n	Heliossol Energia Para a Vida!</p>', 'SIM', 0, '', '', '', 'empresa--mensagem-rodape', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_especificacoes`
--

CREATE TABLE `tb_especificacoes` (
  `idespecificacao` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `title_google` longtext,
  `keywords_google` longtext,
  `description_google` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_especificacoes`
--

INSERT INTO `tb_especificacoes` (`idespecificacao`, `titulo`, `imagem`, `url_amigavel`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`) VALUES
(1, 'Porta Fixa Por Dentro do Vão', '0803201611341367322959..jpg', 'porta-fixa-por-dentro-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(2, 'Porta Fixa Por Trás do Vão', '0803201611341157385324..jpg', 'porta-fixa-por-tras-do-vao', 'SIM', NULL, NULL, NULL, NULL),
(3, 'Formas de Fixação das Guias', '0803201611341304077829..jpg', 'formas-de-fixacao-das-guias', 'SIM', NULL, NULL, NULL, NULL),
(4, 'Vista Lateral do Rolo da Porta', '0803201611351168393570..jpg', 'vista-lateral-do-rolo-da-porta', 'SIM', NULL, NULL, NULL, NULL),
(5, 'Portinhola Lateral', '0803201611351116878064..jpg', 'portinhola-lateral', 'SIM', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_portifolios`
--

CREATE TABLE `tb_galerias_portifolios` (
  `idgaleriaportifolio` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_portifolio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_portifolios`
--

INSERT INTO `tb_galerias_portifolios` (`idgaleriaportifolio`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_portifolio`) VALUES
(1, '1503201609571300280060.jpg', 'SIM', NULL, NULL, 1),
(2, '1503201609571194123464.jpg', 'SIM', NULL, NULL, 1),
(3, '1503201609571223466219.jpg', 'SIM', NULL, NULL, 1),
(4, '1503201609571319150261.jpg', 'SIM', NULL, NULL, 1),
(5, '1503201609571312788443.jpg', 'SIM', NULL, NULL, 1),
(6, '1503201609571185453289.jpg', 'SIM', NULL, NULL, 2),
(7, '1503201609571385251299.jpg', 'SIM', NULL, NULL, 2),
(8, '1503201609571398241846.jpg', 'SIM', NULL, NULL, 2),
(9, '1503201609571372148996.jpg', 'SIM', NULL, NULL, 2),
(10, '1503201609571203846190.jpg', 'SIM', NULL, NULL, 2),
(11, '1503201609571209439705.jpg', 'SIM', NULL, NULL, 3),
(12, '1503201609571247186947.jpg', 'SIM', NULL, NULL, 3),
(13, '1503201609571183328677.jpg', 'SIM', NULL, NULL, 3),
(14, '1503201609571245061526.jpg', 'SIM', NULL, NULL, 3),
(15, '1503201609571132779946.jpg', 'SIM', NULL, NULL, 3),
(16, '1503201609571208483876.jpg', 'SIM', NULL, NULL, 4),
(17, '1503201609571274489300.jpg', 'SIM', NULL, NULL, 4),
(18, '1503201609571406945852.jpg', 'SIM', NULL, NULL, 4),
(19, '1503201609571220302542.jpg', 'SIM', NULL, NULL, 4),
(20, '1503201609571348685064.jpg', 'SIM', NULL, NULL, 4),
(21, '1503201609571281798209.jpg', 'SIM', NULL, NULL, 5),
(22, '1503201609571119695620.jpg', 'SIM', NULL, NULL, 5),
(23, '1503201609571342930547.jpg', 'SIM', NULL, NULL, 5),
(24, '1503201609571333131668.jpg', 'SIM', NULL, NULL, 5),
(25, '1503201609571184904665.jpg', 'SIM', NULL, NULL, 5),
(26, '1603201602001119086460.jpg', 'SIM', NULL, NULL, 6),
(27, '1603201602001399143623.jpg', 'SIM', NULL, NULL, 6),
(28, '1603201602001370562965.jpg', 'SIM', NULL, NULL, 6),
(29, '1603201602001360716700.jpg', 'SIM', NULL, NULL, 6),
(30, '1603201602001161033394.jpg', 'SIM', NULL, NULL, 6),
(31, '1603201602001294477762.jpg', 'SIM', NULL, NULL, 7),
(32, '1603201602001391245593.jpg', 'SIM', NULL, NULL, 7),
(33, '1603201602001270831865.jpg', 'SIM', NULL, NULL, 7),
(34, '1603201602001379540967.jpg', 'SIM', NULL, NULL, 7),
(35, '1603201602001260348087.jpg', 'SIM', NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(1, '2304201608391132287293.jpg', 'SIM', NULL, NULL, 1),
(2, '2304201608391390468664.jpeg', 'SIM', NULL, NULL, 1),
(3, '2304201608391216243444.jpg', 'SIM', NULL, NULL, 1),
(4, '2304201608391286373039.jpg', 'SIM', NULL, NULL, 1),
(5, '2304201608391196362761.jpg', 'SIM', NULL, NULL, 1),
(6, '2304201608391257217307.jpeg', 'SIM', NULL, NULL, 1),
(7, '2304201608401214716216.jpg', 'SIM', NULL, NULL, 1),
(10, '2304201608401291559253.jpg', 'SIM', NULL, NULL, 1),
(11, '2304201608401328922935.jpg', 'SIM', NULL, NULL, 2),
(12, '2304201608401121401425.jpeg', 'SIM', NULL, NULL, 2),
(13, '2304201608401326053386.jpg', 'SIM', NULL, NULL, 7),
(14, '2304201608401266971012.jpeg', 'SIM', NULL, NULL, 7),
(15, '2304201608401342028212.jpg', 'SIM', NULL, NULL, 2),
(16, '2304201608401334137434.jpg', 'SIM', NULL, NULL, 7),
(17, '2304201608401299135454.jpg', 'SIM', NULL, NULL, 2),
(18, '2304201608401301585086.jpg', 'SIM', NULL, NULL, 7),
(19, '2304201608401164767362.jpg', 'SIM', NULL, NULL, 8),
(20, '2304201608401136114268.jpeg', 'SIM', NULL, NULL, 8),
(21, '2304201608401113833066.jpg', 'SIM', NULL, NULL, 2),
(22, '2304201608401256848592.jpg', 'SIM', NULL, NULL, 7),
(23, '2304201608401397319780.jpg', 'SIM', NULL, NULL, 8),
(24, '2304201608401170596735.jpeg', 'SIM', NULL, NULL, 2),
(25, '2304201608401398902206.jpeg', 'SIM', NULL, NULL, 7),
(26, '2304201608401351779362.jpg', 'SIM', NULL, NULL, 9),
(27, '2304201608401361983171.jpeg', 'SIM', NULL, NULL, 9),
(28, '2304201608401195748917.jpg', 'SIM', NULL, NULL, 8),
(29, '2304201608401343919274.jpg', 'SIM', NULL, NULL, 7),
(30, '2304201608401297893129.jpg', 'SIM', NULL, NULL, 2),
(31, '2304201608401299814369.jpg', 'SIM', NULL, NULL, 9),
(32, '2304201608401322948267.jpg', 'SIM', NULL, NULL, 7),
(33, '2304201608401350785028.jpg', 'SIM', NULL, NULL, 8),
(34, '2304201608401311382441.jpg', 'SIM', NULL, NULL, 2),
(35, '2304201608401400787798.jpg', 'SIM', NULL, NULL, 10),
(36, '2304201608401198274153.jpeg', 'SIM', NULL, NULL, 10),
(37, '2304201608401336804703.jpg', 'SIM', NULL, NULL, 9),
(38, '2304201608401359217121.jpeg', 'SIM', NULL, NULL, 8),
(39, '2304201608401168835486.jpg', 'SIM', NULL, NULL, 10),
(40, '2304201608411310709541.jpg', 'SIM', NULL, NULL, 8),
(41, '2304201608411368046935.jpg', 'SIM', NULL, NULL, 9),
(42, '2304201608411201570478.jpg', 'SIM', NULL, NULL, 11),
(43, '2304201608411120085087.jpeg', 'SIM', NULL, NULL, 11),
(44, '2304201608411250978247.jpg', 'SIM', NULL, NULL, 10),
(45, '2304201608411185044661.jpg', 'SIM', NULL, NULL, 8),
(46, '2304201608411270384475.jpeg', 'SIM', NULL, NULL, 9),
(47, '2304201608411255545885.jpg', 'SIM', NULL, NULL, 11),
(48, '2304201608411390762690.jpg', 'SIM', NULL, NULL, 10),
(49, '2304201608411129692391.jpg', 'SIM', NULL, NULL, 12),
(50, '2304201608411128176896.jpg', 'SIM', NULL, NULL, 9),
(51, '2304201608411232450966.jpeg', 'SIM', NULL, NULL, 12),
(52, '2304201608411112979030.jpg', 'SIM', NULL, NULL, 11),
(53, '2304201608411325840611.jpg', 'SIM', NULL, NULL, 9),
(54, '2304201608411246546794.jpeg', 'SIM', NULL, NULL, 10),
(55, '2304201608411182130584.jpg', 'SIM', NULL, NULL, 12),
(56, '2304201608411160079773.jpg', 'SIM', NULL, NULL, 10),
(57, '2304201608411286189121.jpg', 'SIM', NULL, NULL, 11),
(58, '2304201608411120380868.jpg', 'SIM', NULL, NULL, 13),
(59, '2304201608411308726899.jpeg', 'SIM', NULL, NULL, 13),
(60, '2304201608411219402158.jpg', 'SIM', NULL, NULL, 12),
(61, '2304201608411153269183.jpg', 'SIM', NULL, NULL, 10),
(62, '2304201608411114718367.jpeg', 'SIM', NULL, NULL, 11),
(63, '2304201608411138369015.jpg', 'SIM', NULL, NULL, 13),
(64, '2304201608411223154482.jpg', 'SIM', NULL, NULL, 11),
(65, '2304201608411177957868.jpg', 'SIM', NULL, NULL, 12),
(66, '2304201608411365556899.jpg', 'SIM', NULL, NULL, 14),
(67, '2304201608411293708919.jpg', 'SIM', NULL, NULL, 13),
(68, '2304201608411208373012.jpeg', 'SIM', NULL, NULL, 14),
(69, '2304201608411278302664.jpeg', 'SIM', NULL, NULL, 12),
(70, '2304201608411347220644.jpg', 'SIM', NULL, NULL, 11),
(71, '2304201608411216557624.jpg', 'SIM', NULL, NULL, 13),
(72, '2304201608411249469931.jpg', 'SIM', NULL, NULL, 12),
(73, '2304201608411165591436.jpg', 'SIM', NULL, NULL, 14),
(74, '2304201608411141598748.jpeg', 'SIM', NULL, NULL, 13),
(75, '2304201608411329651229.jpg', 'SIM', NULL, NULL, 14),
(76, '2304201608411394875887.jpg', 'SIM', NULL, NULL, 12),
(77, '2304201608411247300164.jpg', 'SIM', NULL, NULL, 13),
(78, '2304201608411306615434.jpg', 'SIM', NULL, NULL, 14),
(79, '2304201608411286060988.jpg', 'SIM', NULL, NULL, 13),
(80, '2304201608411400061668.jpeg', 'SIM', NULL, NULL, 14),
(81, '2304201608411291974774.jpg', 'SIM', NULL, NULL, 14),
(82, '2304201608411261315228.jpg', 'SIM', NULL, NULL, 14),
(84, '0206201611326620365811.jpg', 'SIM', NULL, NULL, 15),
(85, '0206201611325229881087.jpg', 'SIM', NULL, NULL, 15),
(86, '0206201611336427940970.jpg', 'SIM', NULL, NULL, 15),
(87, '0206201611379223948170.jpg', 'SIM', NULL, NULL, 16),
(88, '0206201611382822705345.jpg', 'SIM', NULL, NULL, 16),
(89, '0206201612309555704804.jpg', 'SIM', NULL, NULL, 18),
(90, '0206201612309489992140.jpg', 'SIM', NULL, NULL, 18),
(92, '0206201612309924007393.jpg', 'SIM', NULL, NULL, 17),
(94, '0206201612303208412395.jpg', 'SIM', NULL, NULL, 17),
(95, '0206201601024105867594.jpg', 'SIM', NULL, NULL, 19),
(96, '0206201601127374056946.jpg', 'SIM', NULL, NULL, 20),
(97, '0206201601168081873800.jpg', 'SIM', NULL, NULL, 22),
(98, '0206201601163404680192.jpg', 'SIM', NULL, NULL, 21),
(99, '0206201601251219099085.jpg', 'SIM', NULL, NULL, 25),
(100, '0206201601252138220974.jpg', 'SIM', NULL, NULL, 25),
(101, '0206201601252286110350.jpg', 'SIM', NULL, NULL, 25),
(102, '0206201601251244724550.jpg', 'SIM', NULL, NULL, 25),
(103, '0206201601252408963123.jpg', 'SIM', NULL, NULL, 25),
(104, '0206201601252964970688.jpg', 'SIM', NULL, NULL, 25),
(105, '0206201601307991498998.jpg', 'SIM', NULL, NULL, 26),
(106, '0206201601308686400784.jpg', 'SIM', NULL, NULL, 26),
(107, '0206201601303606664761.jpg', 'SIM', NULL, NULL, 26),
(108, '0206201601305107334678.jpg', 'SIM', NULL, NULL, 26),
(109, '0206201601322189334974.jpg', 'SIM', NULL, NULL, 27),
(110, '0206201601374482018161.jpg', 'SIM', NULL, NULL, 28),
(111, '0206201601406839979816.jpg', 'SIM', NULL, NULL, 29),
(112, '0206201602101846372923.jpg', 'SIM', NULL, NULL, 31),
(113, '0206201602103361889988.jpg', 'SIM', NULL, NULL, 30),
(114, '0206201602137987859774.jpg', 'SIM', NULL, NULL, 32),
(115, '0206201602155774136280.jpg', 'SIM', NULL, NULL, 33),
(116, '0206201602161543522443.jpg', 'SIM', NULL, NULL, 34),
(117, '0206201602189985618632.jpg', 'SIM', NULL, NULL, 35),
(118, '0206201602205831327438.jpg', 'SIM', NULL, NULL, 36),
(119, '0206201602223381839957.jpg', 'SIM', NULL, NULL, 37),
(120, '0206201602242736037000.jpg', 'SIM', NULL, NULL, 38),
(121, '0206201602276832138159.jpg', 'SIM', NULL, NULL, 39),
(122, '0206201602299070261576.jpg', 'SIM', NULL, NULL, 40),
(123, '0206201602362026210985.jpg', 'SIM', NULL, NULL, 41),
(124, '0206201602382780264338.jpg', 'SIM', NULL, NULL, 42),
(125, '0206201602437603781372.jpg', 'SIM', NULL, NULL, 43),
(126, '0206201602451549329832.jpg', 'SIM', NULL, NULL, 44),
(127, '0206201602469395802227.jpg', 'SIM', NULL, NULL, 44),
(128, '0206201602497901506539.jpg', 'SIM', NULL, NULL, 45),
(129, '0206201602504837076320.jpg', 'SIM', NULL, NULL, 46),
(131, '0206201602541972421150.jpg', 'SIM', NULL, NULL, 47),
(132, '0206201602577671541840.jpg', 'SIM', NULL, NULL, 48),
(133, '0206201602594511810863.jpg', 'SIM', NULL, NULL, 49),
(134, '0206201603017179052763.jpg', 'SIM', NULL, NULL, 50),
(135, '0206201603025264710415.jpg', 'SIM', NULL, NULL, 51),
(136, '0206201603059097701025.jpg', 'SIM', NULL, NULL, 52),
(138, '0206201603155628297453.jpg', 'SIM', NULL, NULL, 53),
(139, '0206201603275895418218.jpg', 'SIM', NULL, NULL, 54),
(140, '0206201603306413597418.jpg', 'SIM', NULL, NULL, 55),
(141, '0206201603321504754991.jpg', 'SIM', NULL, NULL, 56),
(142, '0206201603348001746419.jpg', 'SIM', NULL, NULL, 57),
(143, '0206201603361412590572.jpg', 'SIM', NULL, NULL, 58),
(144, '0206201603406059268866.jpg', 'SIM', NULL, NULL, 59),
(145, '0206201603433701251037.jpg', 'SIM', NULL, NULL, 60),
(146, '0206201603455232895927.jpg', 'SIM', NULL, NULL, 61),
(147, '0206201603482585572877.jpg', 'SIM', NULL, NULL, 62),
(148, '0206201603542536903768.jpg', 'SIM', NULL, NULL, 63),
(149, '0206201603587995182031.jpg', 'SIM', NULL, NULL, 64),
(150, '0206201604013830419050.jpg', 'SIM', NULL, NULL, 65),
(151, '0206201604055441461631.jpg', 'SIM', NULL, NULL, 66),
(152, '0206201604077233814108.jpg', 'SIM', NULL, NULL, 67),
(153, '0206201604104702561178.jpg', 'SIM', NULL, NULL, 68),
(154, '0206201604139733353290.jpg', 'SIM', NULL, NULL, 69),
(155, '0206201604157763534225.jpg', 'SIM', NULL, NULL, 70),
(156, '0206201604188787698356.jpg', 'SIM', NULL, NULL, 71),
(157, '0206201604208798109975.jpg', 'SIM', NULL, NULL, 72),
(158, '0206201604226822688020.jpg', 'SIM', NULL, NULL, 73),
(159, '0206201604252982047001.jpg', 'SIM', NULL, NULL, 74),
(160, '0206201604528915292806.jpg', 'SIM', NULL, NULL, 75),
(161, '0206201604549574944819.jpg', 'SIM', NULL, NULL, 76),
(162, '0206201604565788863922.jpg', 'SIM', NULL, NULL, 77),
(163, '0206201605003892825427.jpg', 'SIM', NULL, NULL, 78),
(164, '0206201605021842758945.jpg', 'SIM', NULL, NULL, 79),
(165, '0206201605048977041554.jpg', 'SIM', NULL, NULL, 80),
(166, '0206201605062398212506.jpg', 'SIM', NULL, NULL, 81),
(167, '0206201605082265592921.jpg', 'SIM', NULL, NULL, 82),
(168, '0206201605105299251307.jpg', 'SIM', NULL, NULL, 83),
(169, '0206201605129033808662.jpg', 'SIM', NULL, NULL, 84),
(170, '0206201605148737539843.jpg', 'SIM', NULL, NULL, 85),
(171, '0206201605201403049627.jpg', 'SIM', NULL, NULL, 86);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galeria_empresa`
--

CREATE TABLE `tb_galeria_empresa` (
  `idgaleriaempresa` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galeria_empresa`
--

INSERT INTO `tb_galeria_empresa` (`idgaleriaempresa`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_empresa`) VALUES
(1, '2104201608411212540503.jpg', 'SIM', NULL, NULL, 1),
(2, '2104201608411136283367.jpeg', 'SIM', NULL, NULL, 1),
(3, '2104201608411386765984.jpg', 'SIM', NULL, NULL, 1),
(4, '2104201608411289021726.jpg', 'SIM', NULL, NULL, 1),
(5, '2104201608411203116128.jpg', 'SIM', NULL, NULL, 1),
(6, '2104201608411215846789.jpeg', 'SIM', NULL, NULL, 1),
(7, '2104201608411132593174.jpg', 'SIM', NULL, NULL, 1),
(8, '2104201608411270911164.jpg', 'SIM', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO',
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `titulo`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`, `url_amigavel`) VALUES
(1, 'Homeweb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM', NULL),
(2, 'Marcio André', '202cb962ac59075b964b07152d234b70', 'SIM', 0, 'marciomas@gmail.com', 'NAO', 'marcio-andre');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:03', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-02-11', '00:50:11', 1),
(3, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:12', 1),
(4, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:18:38', 1),
(5, 'CADASTRO DO CLIENTE ', '', '2016-02-11', '01:19:57', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:15:44', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:16:58', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:20:30', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:21:15', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:14', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:22:27', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:12', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:23:34', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-01', '19:34:29', 1),
(15, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:42:14', 1),
(16, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '20:45:13', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:06:59', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-02', '21:07:22', 1),
(19, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:21:44', 1),
(20, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:22:01', 1),
(21, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:23:41', 1),
(22, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:24:30', 1),
(23, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:52', 1),
(24, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:24:56', 1),
(25, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:25:06', 1),
(26, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:22', 1),
(27, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:25', 1),
(28, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:27:28', 1),
(29, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:27:31', 1),
(30, 'DESATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:19', 1),
(31, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:22', 1),
(32, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:37', 1),
(33, 'ATIVOU O LOGIN 1', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'1\'', '2016-03-02', '22:28:39', 1),
(34, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:28:42', 1),
(35, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:26', 1),
(36, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:31', 1),
(37, 'DESATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'2\'', '2016-03-02', '22:29:57', 1),
(38, 'ATIVOU O LOGIN 2', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'2\'', '2016-03-02', '22:30:01', 1),
(39, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:07', 1),
(40, 'DESATIVOU O LOGIN 3', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(41, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'3\'', '2016-03-02', '22:32:13', 1),
(42, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:32:41', 1),
(43, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:46', 1),
(44, 'ATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:49', 1),
(45, 'DESATIVOU O LOGIN 4', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'4\'', '2016-03-02', '22:32:51', 1),
(46, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'4\'', '2016-03-02', '22:32:54', 1),
(47, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:33:10', 1),
(48, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'5\'', '2016-03-02', '22:34:16', 1),
(49, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:34:35', 1),
(50, 'DESATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'NAO\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:39', 1),
(51, 'ATIVOU O LOGIN 6', 'UPDATE tb_produtos SET ativo = \'SIM\' WHERE idproduto = \'6\'', '2016-03-02', '22:38:44', 1),
(52, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'6\'', '2016-03-02', '22:38:47', 1),
(53, 'CADASTRO DO CLIENTE ', '', '2016-03-02', '22:41:49', 1),
(54, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:40:19', 0),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:03', 0),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:41:35', 0),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:01', 0),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:21', 0),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:34', 0),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:42:50', 0),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:43:06', 0),
(62, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:56:12', 0),
(63, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '21:57:39', 0),
(64, 'DESATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'43\'', '2016-03-07', '21:58:16', 0),
(65, 'ATIVOU O LOGIN 43', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'43\'', '2016-03-07', '21:58:18', 0),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:03', 0),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '21:59:09', 0),
(68, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:34', 0),
(69, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:44', 0),
(70, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:02:56', 0),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:09:35', 0),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:10:27', 0),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:12:58', 0),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:14:20', 0),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-07', '22:15:08', 0),
(76, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:27:15', 0),
(77, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:29:53', 0),
(78, 'CADASTRO DO CLIENTE ', '', '2016-03-07', '22:30:18', 0),
(79, 'DESATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'44\'', '2016-03-08', '00:43:43', 0),
(80, 'ATIVOU O LOGIN 44', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'44\'', '2016-03-08', '00:43:48', 0),
(81, 'DESATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'46\'', '2016-03-08', '00:43:53', 0),
(82, 'DESATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'NAO\' WHERE iddica = \'45\'', '2016-03-08', '00:43:56', 0),
(83, 'ATIVOU O LOGIN 46', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'46\'', '2016-03-08', '00:43:59', 0),
(84, 'ATIVOU O LOGIN 45', 'UPDATE tb_dicas SET ativo = \'SIM\' WHERE iddica = \'45\'', '2016-03-08', '00:44:02', 0),
(85, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:32', 0),
(86, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:43', 0),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:30:56', 0),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:55:31', 0),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '19:56:16', 0),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '20:50:57', 0),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:12', 0),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:42', 0),
(93, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:19:58', 0),
(94, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:14', 0),
(95, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '21:20:44', 0),
(96, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:06', 0),
(97, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-08', '22:22:55', 0),
(98, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:20', 0),
(99, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:35', 0),
(100, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:34:56', 0),
(101, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:17', 0),
(102, 'CADASTRO DO CLIENTE ', '', '2016-03-08', '23:35:39', 0),
(103, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-09', '13:51:08', 0),
(104, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:47:37', 3),
(105, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:04', 3),
(106, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:48:53', 3),
(107, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:49:40', 3),
(108, 'EXCLUSÃO DO LOGIN 3, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'3\'', '2016-03-11', '11:49:47', 3),
(109, 'DESATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'NAO\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:51', 3),
(110, 'ATIVOU O LOGIN 2', 'UPDATE tb_logins SET ativo = \'SIM\' WHERE idlogin = \'2\'', '2016-03-11', '11:49:53', 3),
(111, 'CADASTRO DO LOGIN ', '', '2016-03-11', '11:51:20', 3),
(112, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:35:06', 3),
(113, 'CADASTRO DO LOGIN ', '', '2016-03-11', '12:36:19', 3),
(114, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: contato@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'4\'', '2016-03-11', '12:36:27', 3),
(115, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: contato1@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'5\'', '2016-03-11', '12:36:30', 3),
(116, 'EXCLUSÃO DO LOGIN 6, NOME: , Email: contato2@masmidia.com.br', 'DELETE FROM tb_logins WHERE idlogin = \'6\'', '2016-03-11', '12:36:32', 3),
(117, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:37:48', 3),
(118, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\', id_grupologin = \'\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:15', 3),
(119, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:38:42', 3),
(120, 'ALTERAÇÃO DO LOGIN 2', 'UPDATE tb_logins SET titulo = \'2\', email = \'2\' WHERE idlogin = \'2\'', '2016-03-11', '12:39:26', 3),
(121, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:22:49', 3),
(122, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:23:39', 3),
(123, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:24:16', 3),
(124, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:10', 3),
(125, 'ALTERAÇÃO DO LOGIN 2', '', '2016-03-11', '13:25:22', 3),
(126, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:02', 3),
(127, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:42:36', 3),
(128, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:46:24', 3),
(129, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:47:24', 3),
(130, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '13:57:19', 3),
(131, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-11', '14:32:53', 3),
(132, 'CADASTRO DO CLIENTE ', '', '2016-03-14', '21:25:38', 0),
(133, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-14', '21:41:54', 0),
(134, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:22', 0),
(135, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:45:53', 0),
(136, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:46:22', 0),
(137, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:15', 0),
(138, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:49:46', 0),
(139, 'EXCLUSÃO DO LOGIN 4, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'4\'', '2016-03-15', '21:50:04', 0),
(140, 'EXCLUSÃO DO LOGIN 5, NOME: , Email: ', 'DELETE FROM tb_depoimentos WHERE iddepoimento = \'5\'', '2016-03-15', '21:50:07', 0),
(141, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:50:37', 0),
(142, 'CADASTRO DO CLIENTE ', '', '2016-03-15', '21:51:02', 0),
(143, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-15', '22:40:48', 0),
(144, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:02', 1),
(145, 'CADASTRO DO CLIENTE ', '', '2016-03-16', '14:00:28', 1),
(146, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-17', '15:18:52', 1),
(147, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:35:37', 1),
(148, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-21', '14:37:17', 1),
(149, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-22', '10:24:37', 1),
(150, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:07:25', 1),
(151, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:43', 1),
(152, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:28:59', 1),
(153, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:32', 1),
(154, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-26', '16:29:40', 1),
(155, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '20:43:11', 1),
(156, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '21:54:11', 1),
(157, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:07:44', 1),
(158, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '22:54:16', 1),
(159, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:01:55', 1),
(160, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:03:06', 1),
(161, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_lojas WHERE idloja = \'1\'', '2016-03-28', '23:04:52', 1),
(162, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:05:18', 1),
(163, 'CADASTRO DO CLIENTE ', '', '2016-03-28', '23:05:40', 1),
(164, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:12:34', 1),
(165, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-28', '23:15:26', 1),
(166, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:26:58', 1),
(167, 'EXCLUSÃO DO LOGIN 70, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'70\'', '2016-03-29', '00:36:19', 1),
(168, 'EXCLUSÃO DO LOGIN 73, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'73\'', '2016-03-29', '00:36:30', 1),
(169, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:45:43', 1),
(170, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:48:41', 1),
(171, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '00:49:15', 1),
(172, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:27', 1),
(173, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:35:49', 1),
(174, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:09', 1),
(175, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-29', '13:36:22', 1),
(176, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:36:51', 1),
(177, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:37:26', 1),
(178, 'CADASTRO DO CLIENTE ', '', '2016-03-29', '13:38:26', 1),
(179, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:32:40', 1),
(180, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '02:37:17', 1),
(181, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '03:57:35', 1),
(182, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:32:44', 1),
(183, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:28', 1),
(184, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-30', '13:33:57', 1),
(185, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:18:07', 1),
(186, 'CADASTRO DO CLIENTE ', '', '2016-03-31', '15:21:45', 1),
(187, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '15:21:55', 1),
(188, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:07:11', 1),
(189, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '20:19:04', 1),
(190, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '21:38:40', 1),
(191, 'ALTERAÇÃO DO CLIENTE ', '', '2016-03-31', '23:46:08', 1),
(192, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:00:34', 1),
(193, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:29:59', 1),
(194, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:43:01', 1),
(195, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '00:57:06', 1),
(196, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:03:21', 1),
(197, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:07', 1),
(198, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:04:52', 1),
(199, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-01', '01:13:23', 1),
(200, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:34:47', 1),
(201, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:11', 1),
(202, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:39:40', 1),
(203, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '01:40:19', 1),
(204, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-07', '02:42:58', 1),
(205, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:06:04', 1),
(206, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:09:21', 1),
(207, 'EXCLUSÃO DO LOGIN 71, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'71\'', '2016-04-18', '15:20:51', 1),
(208, 'EXCLUSÃO DO LOGIN 72, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'72\'', '2016-04-18', '15:20:54', 1),
(209, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:21:57', 1),
(210, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:18', 1),
(211, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:22:40', 1),
(212, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:01', 1),
(213, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:38', 1),
(214, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:23:57', 1),
(215, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:24:16', 1),
(216, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '15:29:14', 1),
(217, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:30:48', 1),
(218, 'ATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'SIM\' WHERE idcategoriaproduto = \'81\'', '2016-04-18', '15:31:42', 1),
(219, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:45:42', 1),
(220, 'DESATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:04', 1),
(221, 'DESATIVOU O LOGIN 45', 'UPDATE tb_noticias SET ativo = \'NAO\' WHERE idnoticia = \'45\'', '2016-04-18', '16:46:08', 1),
(222, 'EXCLUSÃO DO LOGIN 45, NOME: , Email: ', 'DELETE FROM tb_noticias WHERE idnoticia = \'45\'', '2016-04-18', '16:46:13', 1),
(223, 'ATIVOU O LOGIN 46', 'UPDATE tb_noticias SET ativo = \'SIM\' WHERE idnoticia = \'46\'', '2016-04-18', '16:46:19', 1),
(224, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:55:46', 1),
(225, 'CADASTRO DO CLIENTE ', '', '2016-04-18', '16:56:52', 1),
(226, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-18', '22:48:24', 1),
(227, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-19', '14:44:06', 1),
(228, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:17', 1),
(229, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:41:59', 1),
(230, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:42:09', 1),
(231, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:08', 1),
(232, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:39', 1),
(233, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:45:57', 1),
(234, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '16:46:13', 1),
(235, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:21', 1),
(236, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '18:43:51', 1),
(237, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:20', 1),
(238, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:16:53', 1),
(239, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:17:12', 1),
(240, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:11', 1),
(241, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:25', 1),
(242, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:18:46', 1),
(243, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:00', 1),
(244, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:19:12', 1),
(245, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '20:25:07', 1),
(246, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:49:42', 1),
(247, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:12', 1),
(248, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:35', 1),
(249, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '21:51:50', 1),
(250, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '21:59:44', 1),
(251, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:11:59', 1),
(252, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:28', 1),
(253, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:40', 1),
(254, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:13:50', 1),
(255, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-21', '22:14:01', 1),
(256, 'CADASTRO DO CLIENTE ', '', '2016-04-21', '22:43:10', 1),
(257, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:41:09', 1),
(258, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:42:55', 1),
(259, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:44:25', 1),
(260, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:48:05', 1),
(261, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:24', 1),
(262, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:51:33', 1),
(263, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:58:45', 1),
(264, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '21:59:34', 1),
(265, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:03:37', 1),
(266, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-22', '22:49:25', 1),
(267, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:10:59', 1),
(268, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '14:11:05', 1),
(269, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:54:02', 1),
(270, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:40', 1),
(271, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:58:52', 1),
(272, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '14:59:03', 1),
(273, 'CADASTRO DO CLIENTE ', '', '2016-04-23', '19:53:05', 1),
(274, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:53:46', 1),
(275, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:10', 1),
(276, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:26', 1),
(277, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:54:56', 1),
(278, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '19:55:07', 1),
(279, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:13:54', 1),
(280, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:45:51', 1),
(281, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:01', 1),
(282, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:07', 1),
(283, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:46:27', 1),
(284, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:49:47', 1),
(285, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '20:50:07', 1),
(286, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:39:50', 1),
(287, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-23', '23:44:25', 1),
(288, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '13:35:07', 1),
(289, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:05:24', 1),
(290, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-25', '14:27:09', 1),
(291, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:39:01', 1),
(292, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-26', '23:40:17', 1),
(293, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '12:33:58', 1),
(294, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:17', 1),
(295, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '13:15:40', 1),
(296, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:42:16', 1),
(297, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '14:51:54', 1),
(298, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:27:30', 1),
(299, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:29:38', 1),
(300, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-27', '16:49:14', 1),
(301, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:16', 1),
(302, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-28', '11:01:35', 1),
(303, 'ALTERAÇÃO DO CLIENTE ', '', '2016-04-29', '12:43:56', 1),
(304, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:25:46', 1),
(305, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '13:37:42', 1),
(306, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:39:40', 1),
(307, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:42:57', 1),
(308, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '13:45:07', 1),
(309, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-21', '18:39:34', 1),
(310, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-22', '14:55:09', 1),
(311, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:27:44', 1),
(312, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:29:49', 1),
(313, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:30:56', 1),
(314, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:20', 1),
(315, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:31:40', 1),
(316, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:04', 1),
(317, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:32:24', 1),
(318, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:33:16', 1),
(319, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-24', '12:35:02', 1),
(320, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:40:12', 1),
(321, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '21:06:08', 1),
(322, 'DESATIVOU O LOGIN 80', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:16:06', 1),
(323, 'DESATIVOU O LOGIN 81', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:16:08', 1),
(324, 'DESATIVOU O LOGIN 82', 'UPDATE tb_categorias_produtos SET ativo = \'NAO\' WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:16:11', 1),
(325, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:25:35', 1),
(326, 'EXCLUSÃO DO LOGIN 80, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'80\'', '2016-06-01', '18:34:17', 1),
(327, 'EXCLUSÃO DO LOGIN 81, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'81\'', '2016-06-01', '18:34:20', 1),
(328, 'EXCLUSÃO DO LOGIN 82, NOME: , Email: ', 'DELETE FROM tb_categorias_produtos WHERE idcategoriaproduto = \'82\'', '2016-06-01', '18:34:42', 1),
(329, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:24', 1),
(330, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:38', 1),
(331, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:35:59', 1),
(332, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_subcategorias_produtos WHERE idsubcategoriaproduto = \'2\'', '2016-06-01', '18:36:05', 1),
(333, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:37:28', 1),
(334, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:38:26', 1),
(335, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:04', 1),
(336, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:39:12', 1),
(337, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:24', 1),
(338, 'CADASTRO DO CLIENTE ', '', '2016-06-01', '18:40:53', 1),
(339, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:41:46', 1),
(340, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:02', 1),
(341, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:42:15', 1),
(342, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-01', '18:43:23', 1),
(343, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '00:22:38', 1),
(344, 'DESATIVOU O LOGIN 5', 'UPDATE tb_banners SET ativo = \'NAO\' WHERE idbanner = \'5\'', '2016-06-02', '04:21:29', 1),
(345, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:19', 1),
(346, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:00:56', 1),
(347, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:01:45', 1),
(348, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:27', 1),
(349, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '11:04:40', 1),
(350, 'EXCLUSÃO DO LOGIN 40, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'40\'', '2016-06-02', '11:05:11', 1),
(351, 'EXCLUSÃO DO LOGIN 42, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'42\'', '2016-06-02', '11:16:00', 1),
(352, 'EXCLUSÃO DO LOGIN 43, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'43\'', '2016-06-02', '11:16:13', 1),
(353, 'EXCLUSÃO DO LOGIN 44, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'44\'', '2016-06-02', '11:16:21', 1),
(354, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:17:51', 1),
(355, 'EXCLUSÃO DO LOGIN 1, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'1\'', '2016-06-02', '11:20:18', 1),
(356, 'EXCLUSÃO DO LOGIN 2, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'2\'', '2016-06-02', '11:20:35', 1),
(357, 'EXCLUSÃO DO LOGIN 7, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'7\'', '2016-06-02', '11:20:42', 1),
(358, 'EXCLUSÃO DO LOGIN 8, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'8\'', '2016-06-02', '11:20:48', 1),
(359, 'EXCLUSÃO DO LOGIN 9, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'9\'', '2016-06-02', '11:20:56', 1),
(360, 'EXCLUSÃO DO LOGIN 10, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'10\'', '2016-06-02', '11:21:03', 1),
(361, 'EXCLUSÃO DO LOGIN 11, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'11\'', '2016-06-02', '11:21:10', 1),
(362, 'EXCLUSÃO DO LOGIN 12, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'12\'', '2016-06-02', '11:21:19', 1),
(363, 'EXCLUSÃO DO LOGIN 13, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'13\'', '2016-06-02', '11:21:25', 1),
(364, 'EXCLUSÃO DO LOGIN 14, NOME: , Email: ', 'DELETE FROM tb_produtos WHERE idproduto = \'14\'', '2016-06-02', '11:22:05', 1),
(365, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:32:03', 1),
(366, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:36:38', 1),
(367, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:47:10', 1),
(368, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '11:48:02', 1),
(369, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:04', 1),
(370, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:22:40', 1),
(371, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:02', 1),
(372, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:23:38', 1),
(373, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:24:38', 1),
(374, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:25:28', 1),
(375, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:26:59', 1),
(376, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '12:29:29', 1),
(377, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:30:38', 1),
(378, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '12:35:28', 1),
(379, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:01:58', 1),
(380, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:02:16', 1),
(381, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:02:32', 1),
(382, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:10:09', 1),
(383, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:10:35', 1),
(384, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:11:16', 1),
(385, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:11:30', 1),
(386, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:14:46', 1),
(387, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:16:27', 1),
(388, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:19:05', 1),
(389, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:20:43', 1),
(390, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:24:29', 1),
(391, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:29:41', 1),
(392, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:32:10', 1),
(393, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:35:32', 1),
(394, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:37:39', 1),
(395, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '13:40:09', 1),
(396, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '13:40:47', 1),
(397, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:05:48', 1),
(398, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:06:15', 1),
(399, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:08:11', 1),
(400, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:10:28', 1),
(401, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:13:04', 1),
(402, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:15:17', 1),
(403, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:16:46', 1),
(404, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:18:26', 1),
(405, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:20:23', 1),
(406, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:22:18', 1),
(407, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:24:26', 1),
(408, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:26:55', 1),
(409, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:29:36', 1),
(410, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:31:48', 1),
(411, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:36:17', 1),
(412, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:38:39', 1),
(413, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:43:09', 1),
(414, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:45:32', 1),
(415, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:48:55', 1),
(416, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:50:40', 1),
(417, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:52:46', 1),
(418, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '14:54:32', 1),
(419, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:56:52', 1),
(420, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '14:58:59', 1),
(421, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:00:49', 1),
(422, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:02:12', 1),
(423, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:05:12', 1),
(424, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:10:55', 1),
(425, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:16:33', 1),
(426, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:27:47', 1),
(427, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:30:01', 1),
(428, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:32:16', 1),
(429, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:34:35', 1),
(430, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:36:36', 1),
(431, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:40:38', 1),
(432, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:42:53', 1),
(433, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:44:40', 1),
(434, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:45:38', 1),
(435, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:04', 1),
(436, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '15:46:21', 1),
(437, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:48:43', 1),
(438, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:51:06', 1),
(439, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:54:10', 1),
(440, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '15:58:40', 1),
(441, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:01:16', 1),
(442, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '16:02:07', 1),
(443, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:02:33', 1),
(444, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:05:07', 1),
(445, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:07:05', 1),
(446, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:10:04', 1),
(447, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:11:23', 1),
(448, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:13:06', 1),
(449, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:15:27', 1),
(450, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:16:24', 1),
(451, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:17:59', 1),
(452, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:20:23', 1),
(453, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:13', 1),
(454, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:22:51', 1),
(455, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:24:49', 1),
(456, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:26:00', 1),
(457, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:51:47', 1),
(458, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:54:07', 1),
(459, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:56:39', 1),
(460, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '16:59:54', 1),
(461, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:02:10', 1),
(462, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:03:59', 1),
(463, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:05:58', 1),
(464, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:08:33', 1),
(465, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:10:38', 1),
(466, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:12:15', 1),
(467, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:14:15', 1),
(468, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:16:56', 1),
(469, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:20:40', 1),
(470, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:28:31', 1),
(471, 'EXCLUSÃO DO LOGIN 41, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'41\'', '2016-06-02', '17:28:55', 1),
(472, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:31:32', 1),
(473, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:32:55', 1),
(474, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:34:01', 1),
(475, 'EXCLUSÃO DO LOGIN 46, NOME: , Email: ', 'DELETE FROM tb_servicos WHERE idservico = \'46\'', '2016-06-02', '17:34:38', 1),
(476, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:38:07', 1),
(477, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:41:36', 1),
(478, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:44:20', 1),
(479, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '17:45:10', 1),
(480, 'CADASTRO DO CLIENTE ', '', '2016-06-02', '17:48:45', 1),
(481, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:00:57', 1),
(482, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:03:36', 1),
(483, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:05:28', 1),
(484, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:49', 1),
(485, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:08:57', 1),
(486, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:09', 1),
(487, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:09:16', 1),
(488, 'ALTERAÇÃO DO CLIENTE ', '', '2016-06-02', '18:11:08', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_lojas`
--

CREATE TABLE `tb_lojas` (
  `idloja` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `link_maps` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_lojas`
--

INSERT INTO `tb_lojas` (`idloja`, `titulo`, `telefone`, `endereco`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `description_google`, `keywords_google`, `link_maps`) VALUES
(2, 'BRASÍLIA', '(61)3456-0987', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', 'SIM', NULL, 'brasilia', '', '', '', 'http://www.google.com'),
(3, 'GOIÂNIA', '(61)3456-0922', '2ª Avenida, Bloco 241, Loja 1 - Goiânia-GO', 'SIM', NULL, 'goiania', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_noticias`
--

CREATE TABLE `tb_noticias` (
  `idnoticia` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_noticias`
--

INSERT INTO `tb_noticias` (`idnoticia`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(49, 'SELO QUALISOL Programa de Qualificação de Fornecedores de Sistemas de Aquecimento Solar', '<p style="text-align: justify;">\r\n	O QUALISOL BRASIL &eacute; o Programa de Qualifica&ccedil;&atilde;o de Fornecedores de Sistemas de Aquecimento Solar, que engloba fabricantes, revendas e instaladoras. Fruto de um conv&ecirc;nio entre a ABRAVA, o INMETRO e o PROCEL/Eletrobras, o programa tem como objetivo garantir ao consumidor qualidade dos fornecedores de sistemas de aquecimento solar, de modo a permitir:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o do conhecimento de fornecedores em rela&ccedil;&atilde;o ao aquecimento solar;</p>\r\n<p style="text-align: justify;">\r\n	A amplia&ccedil;&atilde;o da base de mercado do aquecimento solar e suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	O aumento da qualidade das instala&ccedil;&otilde;es e conseq&uuml;ente satisfa&ccedil;&atilde;o do consumidor final;</p>\r\n<p style="text-align: justify;">\r\n	Uma melhor e mais duradoura reputa&ccedil;&atilde;o e confian&ccedil;a em sistemas de aquecimento solar nas suas diversas aplica&ccedil;&otilde;es;</p>\r\n<p style="text-align: justify;">\r\n	Um crescente interesse e habilidade dos fornecedores na prospec&ccedil;&atilde;o de novos clientes e est&iacute;mulo ao surgimento de novos empreendedores.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Assim como em diversos pa&iacute;ses do mundo, no Brasil, as revendas e instaladores representam uma posi&ccedil;&atilde;o estrat&eacute;gica com rela&ccedil;&atilde;o &agrave; difus&atilde;o do aquecimento solar. Na maioria das vezes est&atilde;o em contato direto com o consumidor no momento de decis&atilde;o de compra e instala&ccedil;&atilde;o e algumas vezes tamb&eacute;m planejam e entregam os equipamentos e s&atilde;o respons&aacute;veis diretos por garantir uma instala&ccedil;&atilde;o qualificada com funcionamento, durabilidade e est&eacute;tica assegurados e comprovados. Al&eacute;m das revendas e instaladoras, as empresas fabricantes de equipamentos solares tamb&eacute;m realizam instala&ccedil;&otilde;es e contratos diretos com consumidores finais e assumem a responsabilidade por todo o processo desde a venda at&eacute; a instala&ccedil;&atilde;o e p&oacute;s-venda.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Portanto, a qualidade dos sistemas de aquecimento solar depende diretamente de:</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	bons produtos, etiquetados;</p>\r\n<p style="text-align: justify;">\r\n	bons projetistas e revendas, qualificadas;</p>\r\n<p style="text-align: justify;">\r\n	bons instaladores, qualificados;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '0206201605386174124988..jpg', 'SIM', NULL, 'selo-qualisol-programa-de-qualificacao-de-fornecedores-de-sistemas-de-aquecimento-solar', '', '', '', NULL),
(50, 'ENERGIA RENOVÁVEL NO BRASIL - NÃO BASTA SOMENTE GERAR, TEMOS QUE SABER USÁ-LA', '<p>\r\n	A gera&ccedil;&atilde;o de energia renov&aacute;vel mundial apresentar&aacute; forte crescimento nos pr&oacute;ximos anos, com expectativa de crescimento de 12,7% no per&iacute;odo entre 2010 a 2013 segundo a International Energy Agency - IEA. As raz&otilde;es principais dessa previs&atilde;o s&atilde;o as metas de redu&ccedil;&atilde;o de emiss&otilde;es de CO2 e mudan&ccedil;as clim&aacute;ticas; melhorias tecnol&oacute;gicas favorecendo novas alternativas; aumento na demanda de energia; ambiente regulat&oacute;rio mais favor&aacute;vel; e incentivos governamentais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Enquanto os benef&iacute;cios de desenvolver-se uma agenda s&oacute;lida para consolida&ccedil;&atilde;o da energia renov&aacute;vel no Brasil s&atilde;o evidentes, &eacute; preciso atentar-se para os riscos associados &agrave; segrega&ccedil;&atilde;o do tema de energias renov&aacute;veis do panorama geral da agenda energ&eacute;tica no Brasil, atualmente levada pela ANP (Ag&ecirc;ncia Nacional do Petr&oacute;leo, G&aacute;s Natural e Biocombust&iacute;veis) e ANEEL (Ag&ecirc;ncia Nacional de Energia El&eacute;trica. &Eacute; preciso existir um incentivo maior &agrave;s pol&iacute;ticas p&uacute;blicas que tornem economicamente vi&aacute;veis melhorias de projetos n&atilde;o s&oacute; voltadas para reduzir os gastos com energia el&eacute;trica, aumentando a efici&ecirc;ncia energ&eacute;tica dos processos, como tamb&eacute;m que possibilitem o uso sustent&aacute;vel dos recursos naturais, diz Ricardo Antonio do Esp&iacute;rito Santo Gomes, consultor em efici&ecirc;ncia energ&eacute;tica do CTE.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para Gomes &eacute; preciso pensar mais na maneira como usamos a energia, n&atilde;o somente na maneira como a geramos: N&atilde;o adianta gerarmos muita energia de forma eficiente se aqui na ponta usamos chuveiro el&eacute;trico, ao inv&eacute;s de aquecedor solar t&eacute;rmico por exemplo. Portanto, a gera&ccedil;&atilde;o distribu&iacute;da &eacute; a maneira mais eficiente de se garantir um crescimento sustent&aacute;vel para a sociedade, diminuindo perdas em transmiss&atilde;o, diminuindo o impacto ambiental de grandes centrais geradoras de energia e produzindo, localmente, as utilidades de que o cliente final precisa, como energia el&eacute;trica, vapor, &aacute;gua quente e &aacute;gua gelada, garantindo que um combust&iacute;vel se transforme ao m&aacute;ximo poss&iacute;vel, em outras formas de energia, causando menor impacto poss&iacute;vel e garantindo o n&iacute;vel de conforto exigido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele acrescenta que na pauta de pesquisas e desenvolvimento est&atilde;o os sistemas que possuem o atrativo de manterem altas taxas de efici&ecirc;ncia energ&eacute;tica, baixa emiss&atilde;o de poluentes e de CO2 e redu&ccedil;&atilde;o de custos de transmiss&atilde;o.</p>\r\n<div>\r\n	&nbsp;</div>', '0206201605489606782930..jpg', 'SIM', NULL, 'energia-renovavel-no-brasil--nao-basta-somente-gerar-temos-que-saber-usala', '', '', '', NULL),
(48, 'BANHO AQUECIDO ATRAVÉS DE AQUECIMENTO SOLAR', '<p>\r\n	Apesar de abundante, a energia solar como energia t&eacute;rmica &eacute; pouco aproveitada no Brasil, com isso o banho quente brasileiro continua, em 67% dos lares brasileiros, a utilizar o chuveiro el&eacute;trico que consome 8% da eletricidade produzida ao inv&eacute;s do aquecedor solar, segundo dados do Instituto Vitae Civillis &quot;Um Banho de Sol para o Brasil&quot;, de D&eacute;lcio Rodrigues e Roberto Matajs.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Esses n&uacute;meros contrastam com a capacidade que o territ&oacute;rio brasileiro tem de produzir energia solar: O potencial de gera&ccedil;&atilde;o &eacute; equivalente a 15 trilh&otilde;es de MW/h, correspondente a 50 mil vezes o consumo nacional de eletricidade.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O projeto Cidade Solares, parceria entre o Vitae Civilis e a DASOL/ABRAVA*, foi criado para discutir, propor e acompanhar a tramita&ccedil;&atilde;o e entrada em vigor de leis que incentivam ou obrigam o uso de sistemas solares de aquecimento de &aacute;gua nas cidades e estados.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A id&eacute;ia &eacute; debater e difundir conhecimento sobre o assunto, al&eacute;m de analisar as possibilidades de implanta&ccedil;&atilde;o de projetos nas cidades. Ao ganhar o t&iacute;tulo de &quot;Solar&quot;, uma cidade servir&aacute; de exemplo para outras, com a difus&atilde;o do tema e das tecnologias na pr&aacute;tica. No site, o projeto lista v&aacute;rias iniciativas de cidades que implantaram com sucesso sistemas solares, como Freiburg- na Alemanha, Graz- na &Aacute;ustria, Portland- nos EUA e Oxford- na Inglaterra.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* DASOL/ ABRAVA: Departamento Nacional de Aquecimento Solar da Associa&ccedil;&atilde;o Brasileira de Refrigera&ccedil;&atilde;o, Ar-condicionado, Ventila&ccedil;&atilde;o e Aquecimento.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fonte: Revista Super Interessante - Editora Abril</p>', '0206201605411524312164..jpg', 'SIM', NULL, 'banho-aquecido-atraves-de-aquecimento-solar', '', '', '', NULL),
(47, 'COMO FUNCIONA UM SISTEMA DE AQUECIMENTO SOLAR DE ÁGUA? VOCÊ SABE?', '<p>\r\n	A mesma energia solar que ilumina e aquece o planeta pode ser usada para esquentar a &aacute;gua dos nossos banhos, acenderem l&acirc;mpadas ou energizar as tomadas de casa. O sol &eacute; uma fonte inesgot&aacute;vel de energia e, quando falamos em sustentabilidade, em economia de recursos e de &aacute;gua, em economia de energia e redu&ccedil;&atilde;o da emiss&atilde;o de g&aacute;s carb&ocirc;nico na atmosfera, nada mais natural do que pensarmos numa maneira mais eficiente de utiliza&ccedil;&atilde;o da energia solar. Esta energia &eacute; totalmente limpa e, principalmente no Brasil, onde temos uma enorme incid&ecirc;ncia solar, os sistemas para o aproveitamento da energia do sol s&atilde;o muito eficientes.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Para ser utilizada, a energia solar deve ser transformada e, para isto, h&aacute; duas maneiras principais de realizar essa transforma&ccedil;&atilde;o: Os pain&eacute;is fotovoltaicos e os aquecedores solares.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os primeiros s&atilde;o respons&aacute;veis pela transforma&ccedil;&atilde;o da energia solar em energia el&eacute;trica. Com esses pain&eacute;is podemos utilizar o sol para acender as l&acirc;mpadas das nossas casas ou para ligar uma televis&atilde;o. A segunda forma &eacute; com o uso de aquecedores solares, que utiliza a energia solar para aquecer a &aacute;gua que ser&aacute; direcionada aos chuveiros ou piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Como funciona um sistema de aquecimento solar?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Basicamente este sistema &eacute; composto por dois elementos: Os coletores solares (pain&eacute;is de capta&ccedil;&atilde;o, que vemos freq&uuml;entemente nos telhados das casas) e o reservat&oacute;rio de &aacute;gua quente, tamb&eacute;m chamado de boiler.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os coletores s&atilde;o formados por uma placa de vidro que isola do ambiente externo aletas de cobre ou alum&iacute;nio pintadas com tintas especiais na cor escura para que absorvam o m&aacute;ximo da radia&ccedil;&atilde;o. Ao absorver a radia&ccedil;&atilde;o, estas aletas deixam o calor passar para tubos em forma de serpentina geralmente feitos de cobre. Dentro desses tubos passa &aacute;gua, que &eacute; aquecida antes de ser levada para o reservat&oacute;rio de &aacute;gua quente. Estas placas coletoras podem ser dispostas sobre telhados e lajes e a quantidade de placas instaladas varia conforme o tamanho do reservat&oacute;rio, o n&iacute;vel de insola&ccedil;&atilde;o da regi&atilde;o e as condi&ccedil;&otilde;es de instala&ccedil;&atilde;o. No hemisf&eacute;rio sul, normalmente as placas ficam inclinadas para a dire&ccedil;&atilde;o norte para receber a maior quantidade poss&iacute;vel de radia&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios s&atilde;o cilindros de alum&iacute;nio, inox e polipropileno com isolantes t&eacute;rmicos que mant&eacute;m pelo maior tempo poss&iacute;vel a &aacute;gua aquecida. Uma caixa de &aacute;gua fria abastece o sistema para que o boiler fique sempre cheio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os reservat&oacute;rios devem ser instalados o mais pr&oacute;ximo poss&iacute;vel das placas coletoras (para evitar perda de efici&ecirc;ncia do sistema), de prefer&ecirc;ncia devem estar sob o telhado (para evitar a perda de calor para a atmosfera) e em n&iacute;vel um pouco elevado. Dessa forma, consegue-se o efeito chamado de termossif&atilde;o, ou seja, conforme a &aacute;gua dos coletores vai esquentando, ela torna-se menos densa e vai sendo empurrada pela &aacute;gua fria. Assim ela sobe e chega naturalmente ao boiler, sem a necessidade de bombeamento. Em casos espec&iacute;ficos, em que o reservat&oacute;rio n&atilde;o possa ser instalado acima das placas coletoras, podem-se utilizar bombas para promover a circula&ccedil;&atilde;o da &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	E nos dias nublados, chuvosos ou &agrave; noite?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Apesar de poderem ser instalados de forma independente, os aquecedores solares normalmente trabalham com um sistema auxiliar de aquecimento da &aacute;gua. Este sistema pode ser el&eacute;trico ou a g&aacute;s, e j&aacute; vem de f&aacute;brica. Quando houver uma seq&uuml;&ecirc;ncia de dias nublados ou chuvosos em que a energia gerada pelo aquecedor solar n&atilde;o seja suficiente para esquentar toda a &aacute;gua necess&aacute;ria para o consumo di&aacute;rio, um aquecedor el&eacute;trico ou a g&aacute;s &eacute; acionado, gerando &aacute;gua quente para as pias e chuveiros. O mesmo fato acontece &agrave; noite quando n&atilde;o houver mais &aacute;gua aquecida no reservat&oacute;rio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Num pa&iacute;s tropical como o nosso, na grande maioria dos dias a &aacute;gua ser&aacute; aquecida com a energia solar e, portanto, os sistemas auxiliares ficar&atilde;o desligados a maior parte do tempo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vale a pena economicamente?</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O custo do sistema &eacute; compensado pela economia mensal na conta de energia el&eacute;trica. O Aquecedor Solar Heliotek reduz at&eacute; 80% dos custos de energia com aquecimento e em poucos anos o sistema se paga, gerando lucro ao longo de sua vida &uacute;til? 20 anos.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os aquecedores solares podem ser instalados em novas obras ou durante reformas. Ao construir ou reformar, estude esta op&ccedil;&atilde;o, que &eacute; boa para o planeta e pode ser &oacute;tima para a sua conta de energia.</p>', '0206201605446692289010..jpg', 'SIM', NULL, 'como-funciona-um-sistema-de-aquecimento-solar-de-agua-voce-sabe', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_portfolios`
--

CREATE TABLE `tb_portfolios` (
  `idportfolio` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `descricao` longtext,
  `title_google` varchar(255) DEFAULT NULL,
  `keywords_google` longtext,
  `description_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `video_youtube` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_portfolios`
--

INSERT INTO `tb_portfolios` (`idportfolio`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `video_youtube`) VALUES
(1, 'Portifólio 1 com titulo grande de duas linhas', '1503201609451124075050..jpg', '<p>\r\n	Port&atilde;o autom&aacute;tico basculante articulado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lider Portão automático basculante articulado', 'keywords Portão automático basculante articulado', 'description Portão automático basculante articulado', 'SIM', NULL, 'portifolio-1-com-titulo-grande-de-duas-linhas', 'https://www.youtube.com/embed/BbagKCkzrWs'),
(2, 'Portão basculante', '1503201609451146973223..jpg', '<p>\r\n	Port&atilde;o basculante&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portão basculante', 'Portão basculante', 'Portão basculante', 'SIM', NULL, 'portao-basculante', NULL),
(3, 'Fabricas portas aço', '1503201609461282883881..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', NULL, 'fabricas-portas-aco', NULL),
(4, 'Portões de madeira', '1503201609501367441206..jpg', '<p>\r\n	Port&otilde;es de madeira&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões de madeira', 'Portões de madeira', 'Portões de madeira', 'SIM', NULL, 'portoes-de-madeira', NULL),
(5, 'Portões automáticos', '1503201609511206108237..jpg', '<p>\r\n	Port&otilde;es autom&aacute;ticos&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Portões automáticos', 'Portões automáticos', 'Portões automáticos', 'SIM', NULL, 'portoes-automaticos', NULL),
(6, 'Portfólio 1', '1603201602001115206967..jpg', '<p>\r\n	Portf&oacute;lio 1</p>', 'Portfólio 1', 'Portfólio 1', 'Portfólio 1', 'SIM', NULL, 'portfolio-1', NULL),
(7, 'Portfólio 2', '1603201602001357146508..jpg', '<p>\r\n	Portf&oacute;lio 2</p>', 'Portfólio 2', 'Portfólio 2', 'Portfólio 2', 'SIM', NULL, 'portfolio-2', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`) VALUES
(15, 'AQUECEDOR SOLAR RESIDENCIAL', '0206201611323183000675..jpg', '<p>\r\n	Sistema de aquecimento solar com reservat&oacute;rio acoplado: suas possibilidades de configura&ccedil;&atilde;o atendem a todos os tipos de projetos, desde aqueles para casas populares at&eacute; os de resid&ecirc;ncias de alto padr&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Os tubos de vidro, acoplados ao reservat&oacute;rio t&eacute;rmico, n&atilde;o exigem o uso de bombas de circula&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O aquecedor solar trabalha em processo de termo sif&atilde;o, que se da pela diferen&ccedil;a de densidade entre a &aacute;gua fornecida ao aquecedor solar (ao sistema) e a &aacute;gua quente proveniente da troca de calor nos tubos de vidro a v&aacute;cuo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&bull; Tanque de armazenamento com corpo interno em a&ccedil;o inoxid&aacute;vel AISI 304 e a&ccedil;o galvanizado pintado no corpo externo.</p>\r\n<p>\r\n	&bull; Isolamento t&eacute;rmico em poliuretano com 50 mm de espessura.</p>\r\n<p>\r\n	&bull; Estrutura de a&ccedil;o galvanizado pintado eletrostaticamente.</p>\r\n<p>\r\n	&bull; Tubos de vidro a v&aacute;cuo em boro silicato, com di&acirc;metro de 58 mm e comprimento de 1800 mm.</p>\r\n<p>\r\n	&bull; V&aacute;lvula misturadora de &aacute;gua quente/fria opcional.</p>\r\n<p>\r\n	&bull; Flexibilidade na composi&ccedil;&atilde;o da solu&ccedil;&atilde;o podendo ser suplementado por um sistema el&eacute;trico ou a g&aacute;s.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	* Considerando banhos de 5 minutos com vaz&atilde;o de 6L/m.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p>\r\n		Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n	<p>\r\n		Heliossol Energia Para a Vida!</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '', '', '', 'SIM', 0, 'aquecedor-solar-residencial', 75, 'AQUAKENT', NULL, NULL, NULL, NULL, 6),
(16, 'AQUECEDOR SOLAR PARA PISCINAS', '0206201611364609846530..jpg', '<p style="text-align: justify;">\r\n	Os coletores para aquecimento solar Aquakent atendem a in&uacute;meras aplica&ccedil;&otilde;es. Projetados para suportar uma grande quantidade de &aacute;gua e elevadas temperaturas, s&atilde;o tamb&eacute;m uma excelente alternativa para aquecimento solar de reservat&oacute;rios de &aacute;gua e at&eacute; piscinas. Esse sistema com coletor solar de tubo &agrave; vacuo de baixa press&atilde;o, &eacute; instado exatamente como as placas planas antigas. Ou seja, a instala&ccedil;&atilde;o voc&ecirc; ja conhece, mas com a perfomance voc&ecirc; se surpreender&aacute;.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Os coletores solares Aquakent da Linha AKP, possuem 4 modelos que permitem o uso em Termo-sif&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	AKP50 - Atende um reservat&oacute;rio de at&eacute; 600 litros ou 26m&sup2; de piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	AKP30 - Atende um reservat&oacute;rio de at&eacute; 600 litros ou 16m&sup2; de piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	AKP20 - Atende um reservat&oacute;rio de at&eacute; 400 litros ou 10m&sup2; de piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	AKP12 - Atende um reservat&oacute;rio de at&eacute; 250 litros ou 6m&sup2; de piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Todos eles considerando uma temperatura m&eacute;dia de 50&deg;C no reservat&oacute;rio t&eacute;rmico.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'aquecedor-solar-para-piscinas', 75, 'AQUAKENT', NULL, NULL, NULL, NULL, 5),
(17, 'RESERVATÓRIO HORIZONTAL DE NÍVEL', '0206201611478911885581..jpg', '<p style="text-align: justify;">\r\n	Os boilers propiciam excelente conserva&ccedil;&atilde;o t&eacute;rmica, pois utilizam isolamento progressivo em espuma de poliuretano. S&atilde;o fabricados em cobre ou a&ccedil;o inox com acabamento externo em alum&iacute;nio e ainda possuem p&eacute;s em material termopl&aacute;stico resistente, que possibilitam a coloca&ccedil;&atilde;o sobre suporte exclusivo, estando isentos de ferrugem. Podem ser instalados ao lado ou embaixo da caixa d&#39;&aacute;gua e em desn&iacute;vel com os coletores solares pois possuem a tecnologia &quot;Horizontal de N&iacute;vel&quot;, patente requerida de uso exclusivo da Soletrol, que facilita a instala&ccedil;&atilde;o em telhados mais baixos.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Esta Linha de Boilers foi desenvolvida para sistemas de aquecimento solar de &aacute;gua em resid&ecirc;ncias, etc. Dispon&iacute;vel com press&atilde;o de &aacute;gua de at&eacute; 2 m.c.a. (metros por coluna d&#39;&aacute;gua) nos modelos com corpo interno em cobre e 5 m.c.a com corpo interno em inox. Poss&iacute;vel instala&ccedil;&atilde;o sobre suportes convencionais de alvenaria ou madeiras e tamb&eacute;m sobre suporte modular para lajes.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Especifica&ccedil;&otilde;es T&eacute;cnicas&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Corpo interno em cobre ou a&ccedil;o inoxid&aacute;vel com isolamento t&eacute;rmico em poliuretano r&iacute;gido expandido sem CFC (n&atilde;o agride a camada de oz&ocirc;nio) para assegurar o m&iacute;nimo de perda de temperatura da &aacute;gua armazenada, capa externa em alum&iacute;nio e suportes (p&eacute;s) em material termopl&aacute;stico para prote&ccedil;&atilde;o contra corros&atilde;o. Este produto possui patentes requeridas de uso exclusivo da Soletrol.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Disposi&ccedil;&atilde;o de instala&ccedil;&atilde;o: Horizontal, podendo ser instalado ao lado ou embaixo da caixa d&#39;&aacute;gua, o que na maioria dos casos evita a constru&ccedil;&atilde;o de torres sobre o telhado.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Press&atilde;o de trabalho: 2 m.c.a (metros de coluna d&#39;&aacute;gua) para cobre e 5 m.c.a para inox.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Caracter&iacute;sticas el&eacute;tricas:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Aquecimento complementar el&eacute;trico do reservat&oacute;rio t&eacute;rmico (resist&ecirc;ncia el&eacute;trica blindada e termostato):&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;&bull; Voltagem: 220 V&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;&bull; Pot&ecirc;ncia: 3500 Watts</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Dimens&otilde;es Gerais:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de entrada de &aacute;gua quente dos coletores: 22 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de entrada de &aacute;gua fria da rede (proveniente da caixa d&#39;&aacute;gua de abastecimento: 28 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de sa&iacute;da de &aacute;gua fria para os coletores: 22 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de sa&iacute;da (consumo) de &aacute;qua quente: 28 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'reservatorio-horizontal-de-nivel', 75, 'TRANSEN', NULL, NULL, NULL, NULL, 7),
(18, 'RESERVATÓRIO HORIZONTAL DE PRESSÃO', '0206201612295391559803..jpg', '<p style="text-align: justify;">\r\n	Reservat&oacute;rio T&eacute;rmico Horizontal Transsen Alta Press&atilde;o</p>\r\n<p style="text-align: justify;">\r\n	MARCA: 7TRANSSEN</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O Boiler AP (alta press&atilde;o) &eacute; fabricado em ligas especiais de a&ccedil;o inoxid&aacute;vel e &eacute; desenvolvido para suportar altas press&otilde;es de &aacute;gua (at&eacute; 40 m.c.a. - metros de coluna d&#39;&aacute;gua). Este modelo de reservat&oacute;rio t&eacute;rmico agrega tecnologias exclusivas e patenteadas da Soletrol, sendo indicado para locais com caixa d&#39;&aacute;gua elevada ou com a exist&ecirc;ncia de pressurizador na rede hidr&aacute;ulica. &Eacute; fabricado apenas com a parte interna apenas em a&ccedil;o inoxid&aacute;vel.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Esta Linha de Boilers foi desenvolvida para sistemas de Aquecimento solar de &aacute;gua em resid&ecirc;ncias, hot&eacute;is, mot&eacute;is, vesti&aacute;rios, etc. onde seja necess&aacute;ria resist&ecirc;ncia do reservat&oacute;rio t&eacute;rmico a altas press&otilde;es de &aacute;gua, suportando at&eacute; 40 m.c.a. (metros por coluna d&#39;&aacute;gua).</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Especifica&ccedil;&otilde;es T&eacute;cnicas&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Corpo interno em liga especial de a&ccedil;o inoxid&aacute;vel com isolamento t&eacute;rmico em poliuretano r&iacute;gido expandido sem CFC (n&atilde;o agride a camada de oz&ocirc;nio) para assegurar o m&iacute;nimo de perda de temperatura da &aacute;gua armazenada, capa externa em alum&iacute;nio liso e suportes (p&eacute;s) em material termopl&aacute;stico para prote&ccedil;&atilde;o contra corros&atilde;o. Este produto possui patentes requeridas de uso exclusivo da Soletrol.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Disposi&ccedil;&atilde;o de instala&ccedil;&atilde;o: Horizontal, sempre instalado embaixo da caixa d&#39;&aacute;gua.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Press&atilde;o de trabalho: at&eacute; 40 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; Caracter&iacute;sticas el&eacute;tricas:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Complementar el&eacute;trico do reservat&oacute;rio t&eacute;rmico (resist&ecirc;ncia el&eacute;trica blindada de duplo circuito e termostato):&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;&bull; Voltagem: 220 V&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;&bull; Pot&ecirc;ncia: 3500 Watts</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Dimens&otilde;es:&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de entrada de &aacute;gua quente dos coletores: 22 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de entrada de &aacute;gua fria da rede (proveniente da caixa d&#39;&aacute;gua de abastecimento: 28 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de sa&iacute;da de &aacute;gua fria para os coletores: 22 mm&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Di&acirc;metro do tubo de sa&iacute;da (consumo) de &aacute;gua quente: 28 mm</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'reservatorio-horizontal-de-pressao', 75, 'TRANSEN', NULL, NULL, NULL, NULL, 7),
(19, 'PLACAS COLETORAS', '0206201601018413710941..jpg', '<p style="text-align: justify;">\r\n	As placas coletoras As placas coletoras da Ts - Solar para piscinas, s&atilde;o fabricadas em m&oacute;dulo com resinas termopl&aacute;sticas de alta qualidade e resistente a a&ccedil;&otilde;es dos raios ultravioleta o que sem d&uacute;vida aumenta significamente o tempo de vida &uacute;til, sendo sua principal vantagem.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Leve, flex&iacute;vel e de f&aacute;cil instala&ccedil;&atilde;o;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Baixo custo de instala&ccedil;&atilde;o e manuten&ccedil;&atilde;o;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Resistente a alta press&atilde;o;</p>\r\n<p style="text-align: justify;">\r\n	&bull; N&atilde;o existe conex&atilde;o entre as placas, evitando vazamento e manuten&ccedil;&atilde;o, basta apenas um encaixe;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Alto desempenho na gera&ccedil;&atilde;o e manuten&ccedil;&atilde;o da &aacute;gua quente.</p>\r\n<p style="text-align: justify;">\r\n	&bull; Maior &aacute;rea de absor&ccedil;&atilde;o por metro quadrado com alto grau de efici&ecirc;ncia;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Embalagem de alta qualidade, garantindo a integridade do produto at&eacute; a instala&ccedil;&atilde;o; &nbsp;&bull; Material at&oacute;xico, sem risco de corros&atilde;o e calcifica&ccedil;&atilde;o;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Os tubos s&atilde;o fundidos &agrave;s placas e n&atilde;o colados, garantindo maior durabilidade e evitando vazamentos.</p>\r\n<p style="text-align: justify;">\r\n	&bull; 10 anos de garantia;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Placas dispon&iacute;veis em 3 tamanhos 3,7m x 0,30m , 3,0m x 0,30m e 2,0m x 0,30m</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;O c&aacute;lculo da quantidade de placas necess&aacute;ria para cada piscina, basicamente &eacute; dado pela &aacute;rea quadrada da piscina, ou seja, para o exemplo de uma piscina de 3m x 6m = 18m&sup2; de &aacute;rea, portanto ser&aacute; necess&aacute;rio 18m&sup2; de placas, exceto para regi&otilde;es muito frias. Para regi&otilde;es frias, acrescentar 20% a esse c&aacute;lculo</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'placas-coletoras', 75, 'TS SOLAR', NULL, NULL, NULL, NULL, 8),
(20, 'LINHA ABOVE GROUND (AG)', '0206201601104829281100..jpg', '<p style="text-align: justify;">\r\n	Maquina digital com compressor SCROLL painel de controle Multi fun&ccedil;&otilde;es, &nbsp;auto diagnostico, timer, &nbsp;conex&atilde;o para outros sistemas, tensor de temperatura &nbsp;externa e dois n&iacute;veis de controle de temperatura.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;Gabinete</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Estampado em chapas de a&ccedil;o galvanizado com pintura eletrost&aacute;tica.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Este trocador &eacute; totalmente protegido contra ferrugem, baixo custo no consumo de energia.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A linha AG foi desenvolvida para atender consumidores que n&atilde;o abrem m&atilde;o da qualidade, mas analisam muito bem a rela&ccedil;&atilde;o custo benef&iacute;cio, optando por um equipamento de custo mais baixo em troca de pequenas comodidades. Lembramos que o custo mais baixo se foca no item conforto e n&atilde;o t&eacute;cnico. &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Seu sistema de opera&ccedil;&atilde;o oferece tr&ecirc;s sensores: &nbsp; &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&bull; Alta Press&atilde;o: quando a press&atilde;o de trabalho do compressor aumenta al&eacute;m do limite, a m&aacute;quina desliga automaticamente para evitar a queima do mesmo. &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; &bull; Baixa Press&atilde;o: n&atilde;o permite que o equipamento entre em funcionamento com press&atilde;o de trabalho inferior &agrave; m&iacute;nima permitida no circuito frigor&iacute;fico. &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; &bull; Pressostato de &Aacute;gua: n&atilde;o permite que a m&aacute;quina entre em funcionamento sem a passagem de &aacute;gua pelo condensador.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp; &bull; Sensor de Temperatura: Mede a temperatura da &aacute;gua de entrada no equipamento. &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A linha AG possui as mesmas capacidades nominais e caracter&iacute;sticas t&eacute;cnicas que a linha Premium, por&eacute;m sua diferen&ccedil;a &eacute; o sistema de opera&ccedil;&atilde;o, n&atilde;o possui display com auto diagn&oacute;stico e automa&ccedil;&atilde;o da moto-bomba.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'linha-above-ground-ag', 74, 'JELLY FISH', NULL, NULL, NULL, NULL, 9),
(21, 'LINHA COMERCIAL (BC-C e BC-CS)', '0206201601143339369019..jpg', '<p style="text-align: justify;">\r\n	Maquina digital com compressor SCROLL painel de controle Multi fun&ccedil;&otilde;es, &nbsp;auto diagnostico, timer, &nbsp;conex&atilde;o para outros sistemas, tensor de temperatura &nbsp;externa e dois n&iacute;veis de controle de temperatura.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Gabinete&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Estampado em chapas de a&ccedil;o galvanizado com pintura eletrost&aacute;tica. Este trocador &eacute; totalmente protegido contra ferrugem, baixo custo no consumo de energia.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A linha C &eacute; constru&iacute;da numa &uacute;nica unidade contendo na parte inferior os compressores, condensadores e circuito hidr&aacute;ulico. A parte superior &eacute; constitu&iacute;da pelos evaporadores e ventiladores.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Sua estrutura &eacute; constru&iacute;da com perfil de alum&iacute;nio anodizado e o fechamento lateral com grades na parte superior, e tampas com tratamento ac&uacute;stico na parte inferior. &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Sua base &eacute; de viga &ldquo;U&rdquo; com olhais de i&ccedil;amento e suportes de apoio para empilhadeiras, possibilitando diversas op&ccedil;&otilde;es para embarque e desembarque do equipamento. &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Todos os modelos trabalham com 4 circuitos independentes (compressor &ndash; evaporador &ndash; condensador), painel de controle com auto-diagn&oacute;stico e seis ventiladores por equipamento.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A linha CS &eacute; constru&iacute;da em duas unidades separadas. Uma unidade &eacute; composta por compressores, condensadores e sistema hidr&aacute;ulico. A outra unidade &eacute; composta por evaporadores e os ventiladores.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A primeira unidade pode ser enclausurada na casa de m&aacute;quinas ou outro local dispon&iacute;vel, &nbsp;ficando a segunda parte exposta ao meio ambiente. Esta vers&atilde;o atende situa&ccedil;&otilde;es de pouco espa&ccedil;o, quest&otilde;es est&eacute;ticas, de ru&iacute;do e economia na instala&ccedil;&atilde;o.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'linha-comercial-bcc-e-bccs', 74, 'JELLY FISH', NULL, NULL, NULL, NULL, 9),
(22, 'LINHA PREMIUM (DG)', '0206201601162345955475..jpg', '<p style="text-align: justify;">\r\n	Maquina digital com compressor SCROLL painel de controle Multi fun&ccedil;&otilde;es, &nbsp;auto diagnostico, timer, &nbsp;conex&atilde;o para outros sistemas, tensor de temperatura &nbsp;externa e dois n&iacute;veis de controle de temperatura.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Gabinete</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Estampado em chapas de a&ccedil;o galvanizado com pintura eletrost&aacute;tica. Este trocador &eacute; totalmente protegido contra ferrugem, baixo custo no consumo de energia.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Linha Premium (DG) A linha DG &eacute; o carro chefe de nossa linha de produ&ccedil;&atilde;o, pois na maioria das vezes muitos consumidores preferem investir um pouco mais, para poder contar com todo conforto que esta linha proporciona. Suas caracter&iacute;sticas t&eacute;cnicas e capacidades nominais s&atilde;o iguais as da linha Above Ground, por&eacute;m seu sistema de opera&ccedil;&atilde;o se d&aacute; atrav&eacute;s de um Controlador L&oacute;gico Digital que possui auto diagn&oacute;stico, e sensores respons&aacute;veis por enviar dados ao C.L.D. &bull; Alta Press&atilde;o: quando a press&atilde;o de trabalho do compressor aumenta al&eacute;m do limite, a m&aacute;quina desliga automaticamente para evitar a queima do mesmo. &bull; Baixa Press&atilde;o: n&atilde;o permite que o equipamento entre em funcionamento com press&atilde;o de trabalho inferior &agrave; m&iacute;nima permitida no circuito frigor&iacute;fico. &bull; Pressostato de &Aacute;gua: n&atilde;o permite que o equipamento entre em funcionamento sem a passagem de &aacute;gua pelo condensador. &bull; Sensor de Temperatura &Aacute;gua: Mede a temperatura da &aacute;gua de entrada no equipamento. &bull; Sensor de Temperatura Evaporador: Mede a temperatura do evaporador, permitindo que o mesmo se &ldquo;auto-descongele&rdquo;em dias mais frio, caso isso ocorra</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'linha-premium-dg', 74, 'JELLY FISH', NULL, NULL, NULL, NULL, 9),
(23, 'LINHA HOT 55/70', '0206201601194795223337..jpg', '<p style="text-align: justify;">\r\n	Maquina digital com compressor SCROLL painel de controle Multi fun&ccedil;&otilde;es, &nbsp;auto diagnostico, timer, &nbsp;conex&atilde;o para outros sistemas, tensor de temperatura &nbsp;externa e dois n&iacute;veis de controle de temperatura.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Gabinete</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Estampado em chapas de a&ccedil;o galvanizado com pintura eletrost&aacute;tica. Este trocador &eacute; totalmente protegido contra ferrugem, baixo custo no consumo de energia.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A HOT 55/70 foi desenvolvida para revolucionar a maneira de aquecer &aacute;gua para consumo. Trata-se de um trocador de calor que possui um custo operacional muito inferior aos sistemas de aquecimento &agrave; g&aacute;s, diesel e el&eacute;trico. Esse equipamento pode ser utilizado em sistemas h&iacute;bridos, conjugados com placas de capta&ccedil;&atilde;o solar, g&aacute;s, diesel ou el&eacute;trico. Sua utiliza&ccedil;&atilde;o &eacute; ideal para hot&eacute;is, mot&eacute;is, clubes, e pr&eacute;dios residenciais que utilizem aquecimento central de &aacute;gua.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'linha-hot-5570', 74, 'JELLY FISH', NULL, NULL, NULL, NULL, 9),
(24, 'HOT 70 WW', '0206201601202253486004..jpg', '<p style="text-align: justify;">\r\n	Maquina digital com compressor SCROLL painel de controle Multi fun&ccedil;&otilde;es, &nbsp;auto diagnostico, timer, &nbsp;conex&atilde;o para outros sistemas, tensor de temperatura &nbsp;externa e dois n&iacute;veis de controle de temperatura.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Gabinete</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Estampado em chapas de a&ccedil;o galvanizado com pintura eletrost&aacute;tica. Este trocador &eacute; totalmente protegido contra ferrugem, baixo custo no consumo de energia.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O Equipamento HOT 70WW foi desenvolvido focando efici&ecirc;ncia no aquecimento de &aacute;gua e tamb&eacute;m na utiliza&ccedil;&atilde;o do rejeito que as Bombas de calor produzem - o ar frio. Nos equipamentos convencionais o fluido refrigerante completa seu ciclo no evaporador (serpentina), numa troca de calor com o meio ambiente, liberando ar frio pelo ventilador. Na Linha WW o ciclo do fluido refrigernate tamb&eacute;m se completa com a troca de calor, no entanto n&atilde;o com o meio ambiente, mas com a &quot;&aacute;gua de condensac&atilde;o do pr&oacute;prio chiller&quot;. As Bombas de Calor com evapora&ccedil;&atilde;o a ar tem sua produ&ccedil;&atilde;o condicionada &agrave; temperatura do meio ambiente. Portanto, para dimension&aacute;-las, deve-se observar as temperaturas m&iacute;nimas nas regi&otilde;es de instala&ccedil;&atilde;o, o que determinar&aacute; a quantidade de m&aacute;quinas necess&aacute;ria para atender a demanda nos per&iacute;odos mais frios do ano. Nas Bombas de Calor da Linha WW a evapora&ccedil;&atilde;o do fluido n&atilde;o depende da temperatuar ambiente, mas sim da temperatura da &aacute;gua de condensac&atilde;o do pr&oacute;prio chiller, que varia puoco durante o ano mantendo-se na faixa de 35&ordm;C, trazendo redu&ccedil;&atilde;o do n&uacute;mero de m&aacute;quinas e alta performance para o sistema. Desta forma, o rendimento no circuito de alta press&atilde;o (aquecimento de &aacute;gua - gerac&atilde;o) &eacute; potencializado, gerando efici&ecirc;ncia e economia. O Circuito de baixa press&atilde;o (rejeito e co-gera&ccedil;&atilde;o) recebe a &aacute;gua de condensacnao do chiller, refriando-a total ou parcialmete e reduzindo sensivelmente o custo de operac&atilde;o das torres de resfriamento.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'hot-70-ww', 74, 'JELLY FISH', NULL, NULL, NULL, NULL, 9),
(25, 'PISCINAS DE FIBRA', '0206201601245770804488..jpg', '<p style="text-align: justify;">\r\n	A Heliossol &nbsp;tamb&eacute;m oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Elas s&atilde;o constru&iacute;das sob condi&ccedil;&otilde;es controladas e n&atilde;o sujeitas a intemp&eacute;ries (problemas de clima, maus projetos, entre outros).&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Como n&atilde;o requer costuras, n&atilde;o tem porosidades e afins, fungos e algas n&atilde;o conseguir&atilde;o se instalar na sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	Com a Heliossol, voc&ecirc; n&atilde;o precisa se preocupar, pois sua piscina de fibra chega em sua casa, pronta para instala&ccedil;&atilde;o, exigindo assim, menos subcontratastes que s&atilde;o necess&aacute;rios para construir um outro tipo de piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'piscinas-de-fibra', 77, 'FIBRASSOL', NULL, NULL, NULL, NULL, 3),
(26, 'PISCINAS DE VINIL', '0206201601292195103809..jpg', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos, sem limite de formato e tamanho, com op&ccedil;&otilde;es de cores e barrados exclusivos. Oferecem &agrave; voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa. Utilizando a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento. Em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o.&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	O &nbsp;melhor Custo Benef&iacute;cio &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Al&eacute;m da rapidez e facilidade de constru&ccedil;&atilde;o, o vinil oferece seguran&ccedil;a, durabilidade e um acabamento nobre. Sem os problemas de transporte, vazamento rejunte fissuras, ondula&ccedil;&otilde;es, pontos de ferrugem, bolhas e tantos outros comuns &agrave;s piscinas de fibra ou azulejos. O vinil est&aacute; dispon&iacute;vel em quatro espessuras: 600 microns, 800 microns, 1,00 microns e 1,50 microns. A piscina de vinil &eacute; constitu&iacute;da, basicamente, de uma estrutura de alvenaria revestida com um bols&atilde;o de vinil, e tem toda a garantia que nos oferece uma constru&ccedil;&atilde;o s&oacute;lida e robusta. S&atilde;o mais econ&ocirc;micas que as piscinas em concreto armado revestidas de azulejos e nos oferecem uma s&eacute;rie de vantagens. &nbsp;Solicite-nos um or&ccedil;amento personalizado, ou um representante para conhecer perfeitamente suas necessidades, e apresentar nossas solu&ccedil;&otilde;es e id&eacute;ias de projetos e acess&oacute;rios.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'piscinas-de-vinil', 77, 'HELIOSSOL', NULL, NULL, NULL, NULL, 1),
(27, 'PISCINAS DE PASTILHAS', '0206201601321670936445..jpg', '<p style="text-align: justify;">\r\n	Nossas piscinas de pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Ela pode ser revestida com azulejos, pastilhas de vidro, pastilhas de porcelana ou qualquer outro material cer&acirc;mico. S&atilde;o executadas obedecendo &agrave;s r&iacute;gidas normas de Engenharia. Executamos em qualquer formato, dimens&otilde;es e em qualquer tipo de terreno.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Oferecemos garantia completa, tanto na constru&ccedil;&atilde;o quanto nos equipamentos.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'piscinas-de-pastilhas', 77, 'HELIOSSOL', NULL, NULL, NULL, NULL, 4),
(28, 'CONTROLADOR DIGITAL MICROSOL R1', '0206201601375428536496..jpg', '<p style="text-align: justify;">\r\n	Com tr&ecirc;s sensores que atua no comando da bomba de circula&ccedil;&atilde;o de &aacute;gua. Possui fun&ccedil;&otilde;es que impedem o superaquecimento e o congelamento da &aacute;gua nas tubula&ccedil;&otilde;es. Disp&otilde;e de duas sa&iacute;das para apoio, que pode ser el&eacute;trico, a g&aacute;s, a diesel ou para programar a filtragem da piscina. Al&eacute;m disso, conta com um programador hor&aacute;rio em tempo real, o qual permite a configura&ccedil;&atilde;o de uma agenda semanal de at&eacute; quatro eventos di&aacute;rios e uma bateria interna permanente para garantir o sincronismo do rel&oacute;gio, mesmo na falta de energia, por muitos anos.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Possui comunica&ccedil;&atilde;o serial para conex&atilde;o com o Sitrad.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Dimens&otilde;es: 71x28x71 mm</p>', '', '', '', 'SIM', 0, 'controlador-digital-microsol-r1', 74, 'FULL GAUGE', NULL, NULL, NULL, NULL, 10),
(29, 'CONTROLADOR ANÁLOGICO ANASOL', '0206201601408371623162..jpg', '<p style="text-align: justify;">\r\n	Vers&atilde;o econ&ocirc;mica do controlador diferencial de temperatura para sistemas de aquecimento solar bombeados. Com uma interface amig&aacute;vel, permite o ajuste dos par&acirc;metros atrav&eacute;s de sinaliza&ccedil;&otilde;es luminosas.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Possui fun&ccedil;&otilde;es que evitam o congelamento e o superaquecimento da &aacute;gua com os valores program&aacute;veis pelo usu&aacute;rio para cada fun&ccedil;&atilde;o, al&eacute;m de rel&eacute; de 16A para o comando direto de bombas de circula&ccedil;&atilde;o com at&eacute; 1HP. Seu gabinete permite que seja utilizado tanto em montagens de sobrepor quanto em montagens com painel em trilho DIN 35mm.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Dimens&otilde;es: 100x73,5x37 mm</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'controlador-analogico-anasol', 74, 'FULL GAUGE', NULL, NULL, NULL, NULL, 10),
(30, 'ELETROBOMBA CIRCULADORA 10/2 CALEFAÇÃO', '0206201602085085764200..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-102-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(31, 'ELETROBOMBA CIRCULADORA 10/1 CALEFAÇÃO', '0206201602103930707201..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entrada esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-101-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(32, 'ELETROBOMBA CIRCULADORA 15/1 CALEFAÇÃO', '0206201602132269168007..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatura at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entrada esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-151-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(33, 'ELETROBOMBA CIRCULADORA 20/1 CALEFAÇÃO', '0206201602159516128272..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-201-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11);
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`) VALUES
(34, 'ELETROBOMBA CIRCULADORA 25/1 CALEFAÇÃO', '0206201602166857338845..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-251-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(35, 'ELETROBOMBA CIRCULADORA 4/1 CALEFAÇÃO', '0206201602183826737734..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-41-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(36, 'ELETROBOMBA CIRCULADORA 5/1 CALEFAÇÃO', '0206201602209137390065..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-51-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(37, 'ELETROBOMBA CIRCULADORA 7/1 CALEFAÇÃO', '0206201602228766713316..jpg', '<p>\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p>\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes. &nbsp; &nbsp;</p>\r\n<p>\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p>\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Motor &nbsp;</p>\r\n<p>\r\n	Totalmente silencioso;</p>\r\n<p>\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p>\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p>\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p>\r\n	Rotor &uacute;mido.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Conex&otilde;es &nbsp;</p>\r\n<p>\r\n	Tomada El&eacute;trica</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Caracter&iacute;sticas &nbsp;</p>\r\n<p>\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p>\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p>\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p>\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p>\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob enc</p>\r\n<p>\r\n	menda 13Kg/cm&sup2;)</p>\r\n<p>\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p>\r\n	Classe de isolamento: F</p>\r\n<p>\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-71-calefacao', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(38, 'ELETROBOMBA CIRCULADORA CALEFAÇÃO RC 5.0/15 MAX', '0206201602241515562645..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-calefacao-rc-5015-max', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(39, 'ELETROBOMBA CIRCULADORA CALEFAÇÃO RC 6.0/15 MAX', '0206201602265375599198..jpg', '<p style="text-align: justify;">\r\n	Aplica&ccedil;&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Recircula&ccedil;&atilde;o de &aacute;gua quente para sistemas de calefa&ccedil;&atilde;o, radiadores, lajes e pisos radiantes.</p>\r\n<p style="text-align: justify;">\r\n	Indicado para temperatua at&eacute; 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Circula&ccedil;&atilde;o de &aacute;gua para refrigera&ccedil;&atilde;o de m&aacute;quinas, etc.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Motor &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Totalmente silencioso;</p>\r\n<p style="text-align: justify;">\r\n	Bobinado protegido contra funcionamento a seco, desligamento autom&aacute;tico;</p>\r\n<p style="text-align: justify;">\r\n	Possui protetor t&eacute;rmico incorporado;</p>\r\n<p style="text-align: justify;">\r\n	N&atilde;o produz golpe de ar&iacute;ete;</p>\r\n<p style="text-align: justify;">\r\n	Rotor &uacute;mido.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Conex&otilde;es &nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Tomada El&eacute;trica</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Caracter&iacute;sticas</p>\r\n<p style="text-align: justify;">\r\n	Tens&otilde;es dispon&iacute;veis: 1 x 220/ 1 x 110/ 3 x 220</p>\r\n<p style="text-align: justify;">\r\n	Temperatura da &aacute;gua: 5&ordm;C - 110&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Para temperatura menor a 5&ordm;C (sob encomenda)</p>\r\n<p style="text-align: justify;">\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima do sistema: 10Kg/cm&sup2; (sob encomenda 13Kg/cm&sup2;)</p>\r\n<p style="text-align: justify;">\r\n	Press&atilde;o m&aacute;xima de entreda esta limitada pela press&atilde;o m&aacute;xima do equipamento</p>\r\n<p style="text-align: justify;">\r\n	Classe de isolamento: F</p>\r\n<p style="text-align: justify;">\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 4 m.c.a.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'eletrobomba-circuladora-calefacao-rc-6015-max', 76, 'ROWA', NULL, NULL, NULL, NULL, 11),
(40, 'SISTEMA DE PRESSÃO TANGOSOLAR 14', '0206201602296327026382..jpg', '<p>\r\n	Aplica&ccedil;&otilde;es</p>\r\n<p>\r\n	Aumento de pres&atilde;o de &aacute;gua en resid&ecirc;ncias em geral, novas ou antigas com tanque elevado e sistemas solares. Recomend&aacute;vel para resid&ecirc;ncia com tubula&ccedil;&otilde;es de 20 anos ou mais.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Motor</p>\r\n<p>\r\n	Totalmente silencioso</p>\r\n<p>\r\n	Bobinado protegido contra funcionamento a seco, desliga-se automaticamente</p>\r\n<p>\r\n	Possui protetor t&eacute;rmico incorporado</p>\r\n<p>\r\n	Eixo induzido &uacute;mido</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Conex&otilde;es</p>\r\n<p>\r\n	Entrada e sa&iacute;da com rosca de 1&rdquo;</p>\r\n<p>\r\n	4 v&aacute;lvulas de esfera com uni&otilde;es duplas (polipropileno)</p>\r\n<p>\r\n	Conex&atilde;o el&eacute;trica direta a rede.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Caracter&iacute;sticas</p>\r\n<p>\r\n	Tens&otilde;es dispon&iacute;veis: 127 / 220V</p>\r\n<p>\r\n	Temperatura m&aacute;xima de &aacute;gua: 70&ordm; (com picos de 90&ordm;)</p>\r\n<p>\r\n	Temperatura ambiente: 40&ordm;C</p>\r\n<p>\r\n	Press&atilde;o m&aacute;xima do sistema: 57 PSI (4Kg/cm)</p>\r\n<p>\r\n	Classe de isolamento: F</p>\r\n<p>\r\n	Perda de carga m&aacute;xima na suc&ccedil;&atilde;o: 5,7psi / 4 m.c.a.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Vantagens</p>\r\n<p>\r\n	N&atilde;o afeta as perdas de &aacute;gua (perdas em torneiras)</p>\r\n<p>\r\n	N&atilde;o pressuriza a instala&ccedil;&atilde;o de forma cont&iacute;nua, somente quando se consume mais de 1 litro por minuto</p>\r\n<p>\r\n	O equipamento n&atilde;o ligar&aacute; se n&atilde;o existir o consumo d&rsquo;&aacute;gua acima de 1,5 litro por minuto</p>\r\n<p>\r\n	Baixo consumo el&eacute;trico por n&atilde;o pressurizar continuamente</p>\r\n<p>\r\n	Seguran&ccedil;a e confiabilidade</p>\r\n<p>\r\n	N&atilde;o produz golpe de ar&iacute;ete</p>\r\n<p>\r\n	A bomba ROWA &eacute; totalmente silenciosa</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Constru&ccedil;&atilde;o</p>\r\n<p>\r\n	Equipamentos compactos</p>\r\n<p>\r\n	Pe&ccedil;as em contato com a &aacute;gua produzidas em material inoxid&aacute;vel.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sistema SRS</p>\r\n<p>\r\n	Este produto foi fabricado de acordo com o Sistema de Repara&ccedil;&atilde;o Simples por kits de reposi&ccedil;&atilde;o ROWA (SRS).</p>\r\n<p>\r\n	O SRS permite realizar qualquer conserto em menos de 15 minutos, no mesmo lugar em que se encontra instalado o equipamento.</p>\r\n<p>\r\n	Os kits de reposi&ccedil;&atilde;o SRS podem ser adquiridos nas lojas autorizadas ROWA.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'sistema-de-pressao-tangosolar-14', 76, 'ROWA', NULL, NULL, NULL, NULL, 12),
(41, 'BOMBA SERIE PF22', '0206201602363416152306..jpg', '<p>\r\n	Carca&ccedil;a e intermedi&aacute;ria (interna e externa) - em ABS refor&ccedil;ado com fibra de vidro, termopl&aacute;stico de engenharia de excelentes qualidades. Rotor - do tipo fechado, injetado em Noryl refor&ccedil;ado com fibra de vidro com ressalto na parte posterior e roscado diretamente na ponta do eixo.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Veda&ccedil;&atilde;o do eixo - por selo mec&acirc;nico - &Oslash; 3/4&quot;, tipo &quot;16&quot;, conjunto de precis&atilde;o, constru&iacute;do com borracha nitr&iacute;lica, mola de a&ccedil;o inox e as faces de veda&ccedil;&atilde;o em grafite e cer&acirc;mica o qual em conjunto com o rotor Impedem, totalmente, o contato do l&iacute;quido com as partes met&aacute;licas.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Motor El&eacute;trico&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Caracter&iacute;sticas:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Norma - Nema MG1-18.326 a MG1 - 18.341 - &quot;Jet Pump&rdquo;</p>\r\n<p>\r\n	- 2 p&oacute;los - 3.450 rpm - 60 Hz</p>\r\n<p>\r\n	- Monof&aacute;sico: 110/220V</p>\r\n<p>\r\n	- Trif&aacute;sico: 220/380V</p>\r\n<p>\r\n	- Grau de Prote&ccedil;&atilde;o: IP 21</p>\r\n<p>\r\n	- lsolamento:Classe &quot;B&rdquo;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Opcionais&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Carca&ccedil;a, intermedi&aacute;ria e rotor: Ferro fundido. Outros materiais sob consulta.&nbsp;</p>\r\n<p>\r\n	Selo mec&acirc;nico: Para bombeamento de &aacute;gua com temperaturas superiores &agrave; 80 graus recomenda-se a utiliza&ccedil;&atilde;o das borrachas em Viton. Nos casos em que haja a presen&ccedil;a de abrasivos recomenda-se o uso do selo de carbeto de sil&iacute;cio.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Motor el&eacute;trico:&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- 50Hz&nbsp;</p>\r\n<p>\r\n	- Alto rendimento&nbsp;</p>\r\n<p>\r\n	- Outras tens&otilde;es&nbsp;</p>\r\n<p>\r\n	- IP55, 56, 65, 66&nbsp;</p>\r\n<p>\r\n	- Isolamento classe F ou H&nbsp;</p>\r\n<p>\r\n	- Eixo em a&ccedil;o inox&nbsp;</p>\r\n<p>\r\n	- Outros opcionais sob consulta</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Bombas mancalizadas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-serie-pf22', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(42, 'FILTROS E BOMBA DANCOR', '0206201602385777693320..jpg', '<p>\r\n	V&aacute;lvula seletora de 06 posi&ccedil;&otilde;es de opera&ccedil;&atilde;o, com man&ocirc;metro de controle e visor de retrolavagem;&nbsp;</p>\r\n<p>\r\n	- Tanque produzido pelo processo de rotomoldagem com parafusos prisioneiros em a&ccedil;o inoxid&aacute;vel;</p>\r\n<p>\r\n	- Sistema drenante e meio filtrante (areia s&iacute;lica) projetados segundo a ABNT;</p>\r\n<p>\r\n	- Todos os componentes funcionais s&atilde;o confeccionados em termopl&aacute;stico de engenharia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'filtros-e-bomba-dancor', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(43, 'BOMBA SERIE PF17', '0206201602432857274959..jpg', '<p>\r\n	Carca&ccedil;a e intermedi&aacute;ria (interna e externa) - em ABS refor&ccedil;ado com fibra de vidro, termopl&aacute;stico de engenharia de excelentes qualidades. Rotor - do tipo fechado, injetado em Noryl refor&ccedil;ado com fibra de vidro com ressalto na parte posterior e roscado diretamente na ponta do eixo.</p>\r\n<p>\r\n	Veda&ccedil;&atilde;o do eixo - por selo mec&acirc;nico - &Oslash; 3/4&quot;, tipo &quot;16&quot;, conjunto de precis&atilde;o, constru&iacute;do com borracha nitr&iacute;lica, mola de a&ccedil;o inox e as faces de veda&ccedil;&atilde;o em grafite e cer&acirc;mica o qual em conjunto com o rotor Impedem, totalmente, o contato do l&iacute;quido com as partes met&aacute;lica&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Motor El&eacute;trico</p>\r\n<p>\r\n	&nbsp; Caracter&iacute;sticas:</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- Norma - Nema MG1-18.326 a MG1 - 18.341 - &quot;Jet Pump&rdquo;</p>\r\n<p>\r\n	- 2 p&oacute;los - 3.450 rpm - 60 Hz</p>\r\n<p>\r\n	- Monof&aacute;sico: 110/220V</p>\r\n<p>\r\n	- Trif&aacute;sico: 220/380V</p>\r\n<p>\r\n	- Grau de Prote&ccedil;&atilde;o: IP 21</p>\r\n<p>\r\n	- lsolamento:Classe &quot;B&rdquo;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Opcionais</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Carca&ccedil;a, intermedi&aacute;ria e rotor: Ferro fundido. Outros materiais sob consulta.&nbsp;</p>\r\n<p>\r\n	Selo mec&acirc;nico: Para bombeamento de &aacute;gua com temperaturas superiores &agrave; 80 graus recomenda-se a utiliza&ccedil;&atilde;o das borrachas em Viton. Nos casos em que haja a presen&ccedil;a de abrasivos recomenda-se o uso do selo de carbeto de sil&iacute;cio.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Motor el&eacute;trico:&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- 50Hz&nbsp;</p>\r\n<p>\r\n	- Alto rendimento&nbsp;</p>\r\n<p>\r\n	- Outras tens&otilde;es&nbsp;</p>\r\n<p>\r\n	- IP55, 56, 65, 66&nbsp;</p>\r\n<p>\r\n	- Isolamento classe F ou H&nbsp;</p>\r\n<p>\r\n	- Eixo em a&ccedil;o inox&nbsp;</p>\r\n<p>\r\n	- Outros opcionais sob consulta</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Bombas mancalizadas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-serie-pf17', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(44, 'BOMBA DANCOR PRATIKA', '0206201602455485651509..jpg', '<p>\r\n	Bombas com Mancal Dancor PRATIKA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-dancor-pratika', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(45, 'BOMBA SERIE CAM INCÊNDIO', '0206201602488274685260..jpg', '<p>\r\n	Bombas com Mancal S&eacute;rie CAM &nbsp;- &nbsp;Inc&ecirc;ndio&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-serie-cam-incendio', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(46, 'BOMBA COM MANCAL SERIE CAP', '0206201602504368489988..jpg', '<p>\r\n	Bombas com Mancal S&eacute;rie CAP Dancor &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-com-mancal-serie-cap', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(47, 'BOMBA COM MANCAL SERIE CAM', '0206201602548799860568..jpg', '<p>\r\n	Bombas com Mancal S&eacute;rie CAM</p>\r\n<p>\r\n	Dancor</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-com-mancal-serie-cam', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(48, 'BOMBA COM MANCAL SERIE MS', '0206201602564862908890..jpg', '<p>\r\n	Bomba com Mancal S&eacute;rie MS</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-com-mancal-serie-ms', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(49, 'BOMBA COM MANCAL SERIE CHS', '0206201602585793716808..jpg', '<p>\r\n	Bomba com Mancal S&eacute;rie CHS</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'bomba-com-mancal-serie-chs', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(50, 'BOMBA COM MANCAL SERIE PF', '0206201603008681936901..jpg', '<p>\r\n	Bomba com Mancal S&eacute;rie PF</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-com-mancal-serie-pf', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(51, 'BOMBA COM MANCAL SERIE AAE', '0206201603024155577363..jpg', '<p>\r\n	Bomba com Mancal S&eacute;rie AAE &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'bomba-com-mancal-serie-aae', 78, 'DANCOR', NULL, NULL, NULL, NULL, 13),
(52, 'REFIL PARA FILTRO DE PISCINA 2.200l/h', '0206201603054818544251..jpg', '<p>\r\n	REFIL PARA FILTRO DE PISCINA 2.200l/h Para um bom funcionamento do filtro, seu refil deve ser trocado a cada tr&ecirc;s meses quando usado constantemente. E eles s&atilde;o lav&aacute;veis! Portanto, quando estiverem sujos, basta lav&aacute;-los com a press&atilde;o da &aacute;gua da mangueira que estar&atilde;o prontos para serem reutilizados.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'refil-para-filtro-de-piscina-2200lh', 78, 'HIDROALL', NULL, NULL, NULL, NULL, 13),
(53, 'FILTRO NAULITUS F300P', '0206201603161576764968..jpg', '<p>\r\n	FILTRO NAULITUS F300P&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da mais completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais.&nbsp;</p>\r\n<p>\r\n	Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que os tornam muito mais resistentes a tens&otilde;es residuais.&nbsp;</p>\r\n<p>\r\n	O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute; grandes piscinas de uso p&uacute;blico, tais como em escolas de nata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas.&nbsp;</p>\r\n<p>\r\n	Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de rosca que facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da mais completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais. Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que ostornam muito mais resistentes a tens&otilde;es residuais. O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o. Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute;grandes piscinas de uso p&uacute;blico, tais como em escolas de nata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas. Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de roscaque facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-naulitus-f300p', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 13),
(54, 'FILTRO NAULITUS F350P', '0206201603275965745684..jpg', '<p>\r\n	FILTRO NAULITUS F350P&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da mais completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais. Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que ostornam muito mais resistentes a tens&otilde;es residuais. O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o. Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute;grandes piscinas de uso p&uacute;blico, tais como em escolas denata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas. Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de roscaque facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'filtro-naulitus-f350p', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 13),
(55, 'FILTRO NAULITUS F450P', '0206201603305051143554..jpg', '<p>\r\n	FILTRO NAULITUS F450P&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da mais completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais. Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que ostornam muito mais resistentes a tens&otilde;es residuais. O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o. Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute;grandes piscinas de uso p&uacute;blico, tais como em escolas denata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas. Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de roscaque facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'filtro-naulitus-f450p', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 13),
(56, 'FILTRO NAULITUS F550P', '0206201603323317907758..jpg', '<p>\r\n	FILTRO NAULITUS F550P&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da ma</p>\r\n<p>\r\n	is completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais. Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que ostornam muito mais resistentes a tens&otilde;es residuais. O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o. Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute;grandes piscinas de uso p&uacute;blico, tais como em escolas denata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas. Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de roscaque facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-naulitus-f550p', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 13),
(57, 'FILTRO NAULITUS F650P', '0206201603349467441981..jpg', '<p>\r\n	FILTRO NAULITUS F650P&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da mais completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais. Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que ostornam muito mais resistentes a tens&otilde;es residuais. O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o. Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute;grandes piscinas de uso p&uacute;blico, tais como em escolas denata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas. Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de roscaque facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-naulitus-f650p', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 13),
(58, 'FILTRO NAULITUS F750P', '0206201603361994778993..jpg', '<p>\r\n	FILTRO NAULITUS F750P&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A Nautilus &eacute; pioneira na fabrica&ccedil;&atilde;o da mais completa linha de filtros de areia, produzidos em polietileno, para piscinas residenciais e comerciais. Os tanques rotomoldados em material &agrave; prova de corros&atilde;o s&atilde;o produzidos em uma &uacute;nica pe&ccedil;a, sem emendas, que ostornam muito mais resistentes a tens&otilde;es residuais. O manuseio &eacute; simplificado atrav&eacute;s de v&aacute;lvula seletora produzida em pl&aacute;stico ABS resistente &agrave; corros&atilde;o. Sua capacidade varia de acordo com seu tamanho comportando desde pequenas piscinas residenciais, at&eacute;grandes piscinas de uso p&uacute;blico, tais como em escolas denata&ccedil;&atilde;o, clubes, hot&eacute;is, mot&eacute;is com uma frequ&ecirc;ncia muito grande de banhistas. Sua manuten&ccedil;&atilde;o &eacute; facilitada gra&ccedil;as aos plugues de roscaque facilitam a carga (plugue superior) e nos modelos a partir do F450P a descarga (plugue inferior) do material filtrante.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-naulitus-f750p', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 13),
(59, 'FILTRO E  BOMBA + AREIA 12 FIT M JACUZZI', '0206201603406159051317..jpg', '<p>\r\n	Filtro e Moto Bomba + Areia - Modelo 12 Fit-M (Jacuzzi)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Filtro de alta vaz&atilde;o com taxa de filtra&ccedil;&atilde;o - 1.450 m3/ m2/ dia*&nbsp;</p>\r\n<p>\r\n	&bull; Tanque em polietileno rotomoldado - totalmente &agrave; prova de corros&atilde;o&nbsp;</p>\r\n<p>\r\n	&bull; Motor com grau de prote&ccedil;&atilde;o IP55 - completa prote&ccedil;&atilde;o contra toques, ac&uacute;mulo de poeiras e jatos de &aacute;gua em todas as dire&ccedil;&otilde;es&nbsp;</p>\r\n<p>\r\n	&bull; Bombas Jacuzzi&reg; em termopl&aacute;stico, autoescorvantes e com pr&eacute;-filtro incorporado da s&eacute;rie F&nbsp;</p>\r\n<p>\r\n	&bull; Tens&atilde;o: 220V monof&aacute;sico&nbsp;</p>\r\n<p>\r\n	&bull; Meio filtrante: areia Jacuzzi&reg;&nbsp;</p>\r\n<p>\r\n	&bull; V&aacute;lvula seletora de seis posi&ccedil;&otilde;es:&nbsp;</p>\r\n<p>\r\n	Filtrar - Retrolavar - Drenar - Recircular - Enxaguar - Testar M&aacute;xima taxa de filtra&ccedil;&atilde;o permitida pelas normas t&eacute;cnicas para filtros de areia</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-e--bomba--areia-12-fit-m-jacuzzi', 78, 'JACUZZI', NULL, NULL, NULL, NULL, 13),
(60, 'BOMBA E BOMBA + AREIA 15FIT-M JACUZZI', '0206201603425288921457..jpg', '<p>\r\n	Filtro e Moto Bomba + Areia - Modelo 15 Fit-M (Jacuzzi)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	LINHA FIT: Filtro de alta vaz&atilde;o com taxa de filtra&ccedil;&atilde;o - 1.450 m3/ m2/ dia*&nbsp;</p>\r\n<p>\r\n	&bull; Tanque em polietileno rotomoldado - totalmente &agrave; prova de corros&atilde;o&nbsp;</p>\r\n<p>\r\n	&bull; Motor com grau de prote&ccedil;&atilde;o IP55 - completa prote&ccedil;&atilde;o contra toques, ac&uacute;mulo de poeiras e jatos de &aacute;gua em todas as dire&ccedil;&otilde;es&nbsp;</p>\r\n<p>\r\n	&bull; Bombas Jacuzzi&reg; em termopl&aacute;stico, autoescorvantes e com pr&eacute;-filtro incorporado da s&eacute;rie F&nbsp;</p>\r\n<p>\r\n	&bull; Tens&atilde;o: 220V monof&aacute;sico&nbsp;</p>\r\n<p>\r\n	&bull; Meio filtrante: areia Jacuzzi&reg;&nbsp;</p>\r\n<p>\r\n	&bull; V&aacute;lvula seletora de seis posi&ccedil;&otilde;es:&nbsp;</p>\r\n<p>\r\n	Filtrar - Retrolavar - Drenar - Recircular - Enxaguar - Testar M&aacute;xima taxa de filtra&ccedil;&atilde;o permitida pelas normas t&eacute;cnicas para filtros de areia &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'bomba-e-bomba--areia-15fitm-jacuzzi', 78, 'JACUZZI', NULL, NULL, NULL, NULL, 13),
(61, 'FILTRO E BOMBA + AREIA 19FIT-M JACUZZI', '0206201603444008985240..jpg', '<p>\r\n	Filtro e Moto Bomba + Areia - Modelo 19 Fit-M (Jacuzzi)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	LINHA FIT: Filtro de alta vaz&atilde;o com taxa de filtra&ccedil;&atilde;o - 1.450 m3/ m2/ dia*&nbsp;</p>\r\n<p>\r\n	&bull; Tanque em polietileno rotomoldado - totalmente &agrave; prova de corros&atilde;o&nbsp;</p>\r\n<p>\r\n	&bull; Motor com grau de prote&ccedil;&atilde;o IP55 - completa prote&ccedil;&atilde;o contra toques, ac&uacute;mulo de poeiras e jatos de &aacute;gua em todas as dire&ccedil;&otilde;es&nbsp;</p>\r\n<p>\r\n	&bull; Bombas Jacuzzi&reg; em termopl&aacute;stico, autoescorvantes e com pr&eacute;-filtro incorporado da s&eacute;rie F&nbsp;</p>\r\n<p>\r\n	&bull; Tens&atilde;o: 220V monof&aacute;sico&nbsp;</p>\r\n<p>\r\n	&bull; Meio filtrante: areia Jacuzzi&reg;&nbsp;</p>\r\n<p>\r\n	&bull; V&aacute;lvula seletora de seis posi&ccedil;&otilde;es:&nbsp;</p>\r\n<p>\r\n	Filtrar - Retrolavar - Drenar - Recircular - Enxaguar - Testar M&aacute;xima taxa de filtra&ccedil;&atilde;o permitida pelas normas t&eacute;cnicas para filtros de areia&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-e-bomba--areia-19fitm-jacuzzi', 78, 'JACUZZI', NULL, NULL, NULL, NULL, 13),
(62, 'FILTRO PARA PISCINA 2.200L/h', '0206201603489735300714..jpg', '<p>\r\n	Filtro para Piscina 2.200l/h&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Possuem boa capacidade de filtragem por hora, o que reduz o gasto de energia tornando mais r&aacute;pido e eficaz o tratamento da &aacute;gua da piscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'filtro-para-piscina-2200lh', 78, 'HIDROALL', NULL, NULL, NULL, NULL, 13),
(63, 'ASPIRADOR ASA DELTA INOXPOOL', '0206201603548600800947..jpg', '<p>\r\n	Aspirador Asa Delta (Inoxpool)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'aspirador-asa-delta-inoxpool', 78, 'INOXPOOL', NULL, NULL, NULL, NULL, 14),
(64, 'ASPIRADOR COM ESCOVA AZUL INOXPOOL', '0206201603582105927169..jpg', '<p>\r\n	Aspirador com Escova Azul (Inoxpool)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'aspirador-com-escova-azul-inoxpool', 78, 'INOXPOOL', NULL, NULL, NULL, NULL, 14),
(65, 'ASPIRADOR PLÁSTICO COM 3 RODAS AZUL INOXPOOL', '0206201604012597552111..jpg', '<p>\r\n	Aspirador Pl&aacute;stico com 3 rodas Azul (Inoxpool)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'aspirador-plastico-com-3-rodas-azul-inoxpool', 78, 'INOXPOOL', NULL, NULL, NULL, NULL, 14),
(66, 'CLORADOR DE PASTILHAS NAUTILUS', '0206201604055889401270..jpg', '<p>\r\n	CLORADOR DE PASTILHAS NAUTILUS</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	O Clorador de Pastilhas Nautilus, constitui-se de um reservat&oacute;rio com capacidade de at&eacute; 9 tabletes de 200g de tri-cloro ou 1,900kg de pastilhas de 25g de tri-cloro. Jamais se utilize desse equipamento para a clora&ccedil;&atilde;o com outro tipo de cloro que n&atilde;o o recomendado - tricloro, nem para mistura de diversos tipos de cloro.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Suas aberturas de entrada e sa&iacute;da s&atilde;o em 50mm (1.1/2&quot;) e o equipamento t&ecirc;m capacidade nominal de at&eacute; 6.500 ltrps/hora, recomendando-se a instala&ccedil;&atilde;o de sistema &quot;by pass&quot; nas instala&ccedil;&otilde;es em que a vaz&atilde;o da bomba seja superior a esse volume.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'clorador-de-pastilhas-nautilus', 78, 'NAULITUS', NULL, NULL, NULL, NULL, 15),
(67, 'CLORADOR STYLUS', '0206201604072578904939..jpg', '<p>\r\n	CLORADOR STYLUS&nbsp;</p>\r\n<p>\r\n	Flutuador Clorador para pastilhas de cloro, clora&ccedil;&atilde;o continua para suapiscina.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'clorador-stylus', 78, 'STYLUS', NULL, NULL, NULL, NULL, 15),
(68, 'DISPOSITIVO DE ASPIRAÇÃO', '0206201604102363863293..jpg', '<p style="text-align: justify;">\r\n	DISPOSITIVO DE ASPIRA&Ccedil;&Atilde;O (1.1/2&quot; BSP)</p>\r\n<p style="text-align: justify;">\r\n	ASPIRA&Ccedil;&Atilde;O 1.1/2&quot; BSP Descri&ccedil;&atilde;o: Dispositivo de aspira&ccedil;&atilde;o em metal cromado</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'dispositivo-de-aspiracao', 78, 'VARIAS MARCAS', NULL, NULL, NULL, NULL, 14),
(69, 'ESCOVA CURVA NYLON INOXPOOL', '0206201604132178079816..jpg', '<p>\r\n	Escova Curva Nylon (Inoxpool)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'escova-curva-nylon-inoxpool', 78, 'INOXPOOL', NULL, NULL, NULL, NULL, 16),
(70, 'ESCOVA CURVA NYLON LUXO INOXPOOL', '0206201604152306291812..jpg', '<p>\r\n	Escova Curva Nylon Luxo 44cm (Inoxpool)</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'escova-curva-nylon-luxo-inoxpool', 78, 'INOXPOOL', NULL, NULL, NULL, NULL, 16),
(71, 'ILUMINAÇÃO LED POOL', '0206201604171791345779..jpg', '<p>\r\n	Ilumina&ccedil;&atilde;o em Led Veico composto por 72 l&acirc;mpadas e mais de 10 comandos de cores e quadro de comando com controle remoto.</p>\r\n<p>\r\n	A linha de ilumina&ccedil;&atilde;o Heliossol ir&aacute; transformar sua piscina em uma grande festa, al&eacute;m torn&aacute;-la mais sofisticada e bonita. &Eacute; imposs&iacute;vel n&atilde;o ter.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'iluminacao-led-pool', 78, 'LED VEICO', NULL, NULL, NULL, NULL, 17),
(72, 'REFLETOR LED 42 VEICO', '0206201604206262402534..jpg', '<p>\r\n	Refletor LED 42 l&acirc;mpadas-azul</p>\r\n<p>\r\n	Refletor LED 42 l&acirc;mpads-branco</p>\r\n<p>\r\n	Refletor LED 42 l&acirc;mpadas-lil&aacute;s</p>\r\n<p>\r\n	Refletor LED 42 l&acirc;mpadas-verde</p>\r\n<p>\r\n	Refletor LED cromado 42 l&acirc;mpadas-azul</p>\r\n<p>\r\n	Refletor LED cromado 42 l&acirc;mpadas-branco</p>\r\n<p>\r\n	Refletor LED cromado 42 l&acirc;mpadas-lil&aacute;s</p>\r\n<p>\r\n	Refletor LED cromado 42 l&acirc;mpadas-verde</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>\r\n<div>\r\n	&nbsp;</div>', '', '', '', 'SIM', 0, 'refletor-led-42-veico', 78, 'VEICO', NULL, NULL, NULL, NULL, 17),
(73, 'REFLETOR LED 72', '0206201604223072401670..jpg', '<p>\r\n	REFLETOR LED 72</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	- 10190 - Refletor LED 72 l&acirc;mpadas</p>\r\n<p>\r\n	- 10190/01 - Refletor LED cromado 72 l&acirc;mpadas - Tens&atilde;o 12cc</p>\r\n<p>\r\n	- Corpo em ABS e frontal em policarbonato</p>\r\n<p>\r\n	- Utiliza&ccedil;&atilde;o em concreto, fibra e vinil</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'refletor-led-72', 78, 'LED VEICO', NULL, NULL, NULL, NULL, 17),
(74, 'DEGRAU EM AÇO INOX PARA ESCADAS', '0206201604246421111176..jpg', '<p>\r\n	Degrau em A&ccedil;o Inox para escadas de piscinas</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'degrau-em-aco-inox-para-escadas', 78, 'VARIAS MARCAS', NULL, NULL, NULL, NULL, 18);
INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`) VALUES
(75, 'HIDROSAN', '0206201604518175377841..jpg', '<p>\r\n	Hidrosan &eacute; o Dicloro Org&acirc;nico exclusivo da HidroAll, fabricado de acordo com as caracter&iacute;sticas f&iacute;sico-qu&iacute;micas das &aacute;guas brasileiras.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A f&oacute;rmula &uacute;nica (Patente Requerida) de Hidrosan o diferencia dos demais dicloros fabricados para aplica&ccedil;&otilde;es industriais e que s&atilde;o vendidos para tratamento de piscinas.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Hidrosan n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan permanecem com &aacute;gua balanceada, enquanto as tratadas com outros produtos se estragam ap&oacute;s 2 ou 3 meses de aplica&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(76, 'HCL 200 PENTA', '0206201604548075510619..jpg', '<p>\r\n	HCL PENTA cont&eacute;m estabilizador em sua f&oacute;rmula que proteje o cloro livre da &aacute;gua evitando a perda causada pelos raios solares. O HCL Penta em tabletes pode tamb&eacute;m ser utilizado em cloradores flutuantes, sendo que o teor de cloro &eacute; controlado atrav&eacute;s da adi&ccedil;&atilde;o de um n&uacute;mero maior ou menos de tabletes no clorador. &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR</p>\r\n<p>\r\n	Inicialmente verifique a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.</p>\r\n<p>\r\n	Ajuste o pH entre 7,2 e 7,6</p>\r\n<p>\r\n	Em piscinas com &aacute;gua verde evidenciando a presen&ccedil;a de algas, fa&ccedil;a uma dosagem de Algicida segundo instru&ccedil;&otilde;es do r&oacute;tulo de cada produto.</p>\r\n<p>\r\n	Ap&oacute;s verificar a alcalinidade e o ph da &aacute;gua da piscina inicie o tratamento com HCL 200 PENTA. Em piscinas com &aacute;gua limpa mantenha o residual de cloro livre entre 1 e 3 ppm. Como dosagem inicial utilize 1 tablete de 200 gramas para cada 30.000 litros de &aacute;gua.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Obs: S&oacute; reutilize a piscina quando o teor de cloro livre estiver inferior a 3 ppm (partes por milh&atilde;o) Obs2: Cuidado! Esses tabletes n&atilde;o devem entrar em contato direto com as superf&iacute;cies de fibra/vinil, pois podem causar manchas. Quando utilizado em flutuadores, n&atilde;o deix&aacute;-los em contato com as bordas, pois isso pode causar manchas. Os cloradores/flutuadores precisam ser muito bem inspecionados para que n&atilde;o abram acidentalmente. Uma vez que caso os tabletes atinjam o fundo, causar&atilde;o descoramento na superf&iacute;cie.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hcl-200-penta', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(77, 'HCL HYPO', '0206201604569250549752..jpg', '<p>\r\n	HCL HYPO foi especialmente desenvolvido para aplica&ccedil;&atilde;o em &aacute;guas de piscinas, possui alto desempenho e a&ccedil;&atilde;o imediata. &Eacute; indicado para tratar qualquer tipo de piscina. &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&middot;Alto poder bactericida</p>\r\n<p>\r\n	&middot;A&ccedil;&atilde;o instant&acirc;nea&nbsp;</p>\r\n<p>\r\n	&middot;Reage rapidamente com mat&eacute;ria org&acirc;nica&nbsp;</p>\r\n<p>\r\n	&middot;F&aacute;cil aplica&ccedil;&atilde;o&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USO:&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajudado Kit Teste de Alcalinidade e o produto pH Est&aacute;vel. Ajuste a Dureza C&aacute;lcica entre 100 e 250 ppm utilizando o produto Hidroc&aacute;lcio.&nbsp;</p>\r\n<p>\r\n	2. Mantenha o pH da &aacute;gua sempre entre 7,0 e 7,2.&nbsp;</p>\r\n<p>\r\n	3. Adicione 4 gramas de hcl Hypo por metro c&uacute;bico de &aacute;gua dia sim, dia n&atilde;o, preferencialmente &agrave; noite.&nbsp;</p>\r\n<p>\r\n	4. Dissolva hcl Hypo em um recipiente pl&aacute;stico, limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Coloque o filtro na posi&ccedil;&atilde;o &ldquo;recircular&rdquo; por 1 hora para homogeneizar o produto.&nbsp;</p>\r\n<p>\r\n	5. Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio do Kit de tiras PoolTest e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Aten&ccedil;&atilde;o: N&atilde;o misture com outros produtos, inclusive produtos para piscinas. &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hcl-hypo', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(78, 'HCL TAB 3 EM 1', '0206201604596277839781..jpg', '<p>\r\n	O HCL TAB 3 em 1 (tabletes) foi desenvolvido para oferecer um tratamento pr&aacute;tico, seguro e com excelente rela&ccedil;&atilde;o custo x benef&iacute;cio.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ele &eacute; &nbsp;um Tricloro Org&acirc;nico aditivado com clarificante e algicida, desempenhando as seguintes fun&ccedil;&otilde;es: &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Cloro Estabilizado: A desinfec&ccedil;&atilde;o &eacute; a etapa mais importante no tratamento das piscinas pois elimina microorganismos existentes na &aacute;gua que podem causar doen&ccedil;as. Al&eacute;m disso, HCL Tab 3 em 1 cont&eacute;m estabilizador em sua f&oacute;rmula, o que protege o cloro livre da &aacute;gua evitando a perda causada pelos raios solares.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Clarificante: HCL Tab 3 em 1 evita a turbidez da &aacute;gua da piscina, decantando material s&oacute;lido e mat&eacute;ria org&acirc;nica morta em suspens&atilde;o, mantendo-a sempre l&iacute;mpida e cristalina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Algicida: Ferramenta fundamental para a sa&uacute;de de uma piscina, o algicida contido na f&oacute;rmula de HCL Tab 3 em 1 evita o aparecimento de algas que deixam a &aacute;gua turva e com colora&ccedil;&atilde;o geralmente verde.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	Inicialmente verifique a Alcalinidade Total da &aacute;gua. Ajuste entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Ajuste o pH entre 7,2 e 7,6&nbsp;</p>\r\n<p>\r\n	Em seguida inicie o tratamento com HCL Tab. Em piscinas com &aacute;gua limpa mantenha o residual de cloro livre entre 1 e 3 ppm. Como dosagem inicial utilize 1 tablete de 200 gramas para cada 30.000 litros de &aacute;gua.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor de cloro livre for inferior a 3 ppm (partes por milh&atilde;o).&nbsp;</p>\r\n<p>\r\n	HCL TAB 3 em 1 em tabletes pode ser colocado em cloradores flutuantes, sendo que o teor de cloro &eacute; controlado atrav&eacute;s da adi&ccedil;&atilde;o de um n&uacute;mero maior ou menor de tabletes no clorador. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	SEGURAN&Ccedil;A&nbsp;</p>\r\n<p>\r\n	Mantenha afastado de crian&ccedil;as e animais dom&eacute;sticos.&nbsp;</p>\r\n<p>\r\n	Mantenha o produto em sua embalagem original.&nbsp;</p>\r\n<p>\r\n	Evite contato com a pele, olhos e roupa. Evite inala&ccedil;&atilde;o do p&oacute;.&nbsp;</p>\r\n<p>\r\n	Conserve a embalagem fechada em local fresco e seco.&nbsp;</p>\r\n<p>\r\n	N&atilde;o misture com outros produtos, inclusive produtos para piscina (principalmente Hipoclorito de C&aacute;lcio) pois podem reagir violentamente produzindo explos&otilde;es e desprendimento de cloro.</p>\r\n<p>\r\n	Lave os objetos ou utens&iacute;lios usados como medida antes de reutiliz&aacute;-los.&nbsp;</p>\r\n<p>\r\n	Depois de utilizar este produto lave e seque as m&atilde;os.&nbsp;</p>\r\n<p>\r\n	Evite a contamina&ccedil;&atilde;o do solo e cursos d&#39;&aacute;gua.&nbsp;</p>\r\n<p>\r\n	Informar os &oacute;rg&atilde;os ambientais respons&aacute;veis. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hcl-tab-3-em-1', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(79, 'HIDROSAN PENTA 10KG', '0206201605027244368427..jpg', '<p>\r\n	O Hidrosan Penta &eacute; o Dicloro Org&acirc;nico aditivado com clarificante, algost&aacute;tico, estabilizador de pH e eliminador de mat&eacute;ria org&acirc;nica.Hidrosan Penta n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan Penta permanecem com &aacute;gua balanceada.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Primeiramente ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda de um Kit Teste. Ap&oacute;s ajustada a alcalinidade voc&ecirc; dever&aacute; ajustar a Dureza C&aacute;lcica entre 100 e 250 ppm utilizando o Kit teste (de tiras).&nbsp;</p>\r\n<p>\r\n	Mantenha o pH da &aacute;gua sempre entre 7,2 e 7,6.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 4 gramas de Hidrosan Penta por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan Penta em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio de um kit de testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s 3 ou 4 semanas, o cloro residual medido ao amanhecer dever&aacute; ser superior a 1ppm revelando a estabiliza&ccedil;&atilde;o da &aacute;gua, sendo poss&iacute;vel ent&atilde;o, a redu&ccedil;&atilde;o da clora&ccedil;&atilde;o para aproximadamente 1,5 gramas por metro c&uacute;bico de &aacute;gua, conforme descrito em: Dosagem de Rotina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE ROTINA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda do Kit Teste de Alcalinidade e o produto pH Est&aacute;vel.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 1,5 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan Penta em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina, ou se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual no dia seguinte com o aux&iacute;lio do Kit de testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE CHOQUE&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda do Kit Teste de Alcalinidade e o produto pH Est&aacute;vel.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s longos per&iacute;odos sem clora&ccedil;&atilde;o ou quando a &aacute;gua estiver colorida, turva ou com odor desagrad&aacute;vel, ajuste o pH entre 7,2 e 7,6 e adicione 15 gramas de Hidrosan Penta por metro c&uacute;bico de &aacute;gua. Repita a opera&ccedil;&atilde;o se necess&aacute;rio. Utilize preferencialmente OxiAll para oxidar a mat&eacute;ria org&acirc;nica. OxiAll substitui com vantagens a super clora&ccedil;&atilde;o, reduzindo o consumo de cloro e melhorando a desinfec&ccedil;&atilde;o da piscina.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor residual de cloro for inferior a 3 ppm. Utilizando OxiAll para oxidar a mat&eacute;ria org&acirc;nica pode-se utilizar a piscina imediatamente. &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan-penta-10kg', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(80, 'HIDROSAN PENTA 2,5KG', '0206201605035983923089..jpg', '<p>\r\n	O Hidrosan Penta &eacute; o Dicloro Org&acirc;nico aditivado com clarificante, algost&aacute;tico, estabilizador de pH e eliminador de mat&eacute;ria org&acirc;nica.Hidrosan Penta n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan Penta permanecem com &aacute;gua balanceada.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Primeiramente ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda de um Kit Teste. Ap&oacute;s ajustada a alcalinidade voc&ecirc; dever&aacute; ajustar a Dureza C&aacute;lcica entre 100 e 250 ppm utilizando o Kit teste (de tiras).&nbsp;</p>\r\n<p>\r\n	Mantenha o pH da &aacute;gua sempre entre 7,2 e 7,6.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 4 gramas de Hidrosan Penta por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan Penta em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio de um kit de testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s 3 ou 4 semanas, o cloro residual medido ao amanhecer dever&aacute; ser superior a 1ppm revelando a estabiliza&ccedil;&atilde;o da &aacute;gua, sendo poss&iacute;vel ent&atilde;o, a redu&ccedil;&atilde;o da clora&ccedil;&atilde;o para aproximadamente 1,5 gramas por metro c&uacute;bico de &aacute;gua, conforme descrito em: Dosagem de Rotina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE ROTINA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda do Kit Teste de Alcalinidade e o produto pH Est&aacute;vel.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 1,5 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan Penta em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina, ou se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual no dia seguinte com o aux&iacute;lio do Kit de testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE CHOQUE&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda do Kit Teste de Alcalinidade e o produto pH Est&aacute;vel.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s longos per&iacute;odos sem clora&ccedil;&atilde;o ou quando a &aacute;gua estiver colorida, turva ou com odor desagrad&aacute;vel, ajuste o pH entre 7,2 e 7,6 e adicione 15 gramas de Hidrosan Penta por metro c&uacute;bico de &aacute;gua. Repita a opera&ccedil;&atilde;o se necess&aacute;rio. Utilize preferencialmente OxiAll para oxidar a mat&eacute;ria org&acirc;nica. OxiAll substitui com vantagens a super clora&ccedil;&atilde;o, reduzindo o consumo de cloro e melhorando a desinfec&ccedil;&atilde;o da piscina.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor residual de cloro for inferior a 3 ppm. Utilizando OxiAll para oxidar a mat&eacute;ria org&acirc;nica pode-se utilizar a piscina imediatamente. &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan-penta-25kg', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(81, 'HIDROSAN PENTA 900G', '0206201605056097904530..jpg', '<p>\r\n	O Hidrosan Penta &eacute; o Dicloro Org&acirc;nico aditivado com clarificante, algost&aacute;tico, estabilizador de pH e eliminador de mat&eacute;ria org&acirc;nica.Hidrosan Penta n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan Penta permanecem com &aacute;gua balanceada.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Primeiramente ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda de um Kit Teste. Ap&oacute;s ajustada a alcalinidade voc&ecirc; dever&aacute; ajustar a Dureza C&aacute;lcica entre 100 e 250 ppm utilizando o Kit teste (de tiras).&nbsp;</p>\r\n<p>\r\n	Mantenha o pH da &aacute;gua sempre entre 7,2 e 7,6.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 4 gramas de Hidrosan Penta por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan Penta em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio de um kit de testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s 3 ou 4 semanas, o cloro residual medido ao amanhecer dever&aacute; ser superior a 1ppm revelando a estabiliza&ccedil;&atilde;o da &aacute;gua, sendo poss&iacute;vel ent&atilde;o, a redu&ccedil;&atilde;o da clora&ccedil;&atilde;o para aproximadamente 1,5 gramas por metro c&uacute;bico de &aacute;gua, conforme descrito em: Dosagem de Rotina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE ROTINA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda do Kit Teste de Alcalinidade e o produto pH Est&aacute;vel.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 1,5 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan Penta em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina, ou se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual no dia seguinte com o aux&iacute;lio do Kit de testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE CHOQUE&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm, com a ajuda do Kit Teste de Alcalinidade e o produto pH Est&aacute;vel.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s longos per&iacute;odos sem clora&ccedil;&atilde;o ou quando a &aacute;gua estiver colorida, turva ou com odor desagrad&aacute;vel, ajuste o pH entre 7,2 e 7,6 e adicione 15 gramas de Hidrosan Penta por metro c&uacute;bico de &aacute;gua. Repita a opera&ccedil;&atilde;o se necess&aacute;rio. Utilize preferencialmente OxiAll para oxidar a mat&eacute;ria org&acirc;nica. OxiAll substitui com vantagens a super clora&ccedil;&atilde;o, reduzindo o consumo de cloro e melhorando a desinfec&ccedil;&atilde;o da piscina.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor residual de cloro for inferior a 3 ppm. Utilizando OxiAll para oxidar a mat&eacute;ria org&acirc;nica pode-se utilizar a piscina imediatamente. &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan-penta-900g', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(82, 'HIDROSAN PLUS 10KG', '0206201605089053903412..jpg', '<p>\r\n	HIDROSAN PLUS O Hidrosan n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan permanecem com &aacute;gua balanceada, enquanto as tratadas com outros produtos se estragam ap&oacute;s 2 ou 3 meses de aplica&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp;DESINFEC&Ccedil;&Atilde;O DE &Aacute;GUA DE PISCINAS&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por ser altamente sol&uacute;vel e possuir o pH neutro, Hidrosan &eacute; indicado para o tratamento de qualquer tipo de piscina.&nbsp;</p>\r\n<p>\r\n	O Hidrosan cont&eacute;m estabilizador de cloro que impede a decomposi&ccedil;&atilde;o do cloro residual da &aacute;gua pelo sol. A estabiliza&ccedil;&atilde;o do cloro ocorre ap&oacute;s 3 ou 4 semanas, quando o estabilizador contido no Hidrosan atinge a concentra&ccedil;&atilde;o de 25 a 30 ppm na &aacute;gua da piscina. Ao atingir esta concentra&ccedil;&atilde;o de estabilizador na &aacute;gua, o consumo de cloro se reduzir&aacute; &agrave; metade da dosagem inicial, ou menos, dependendo de fatores como: freq&uuml;&ecirc;ncia, grau de contamina&ccedil;&atilde;o, temperatura da &aacute;gua, etc.&nbsp;</p>\r\n<p>\r\n	O uso de Hidrosan mant&eacute;m o teor residual de cloro estabilizado entre 1 e 3 ppm durante todo o dia, protegendo a &aacute;gua de sua piscina, o que n&atilde;o ocorre com produtos sem estabilizantes.Hidrosan proporciona mais seguran&ccedil;a e mais economia.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM INICIAL PARA ESTABILIZA&Ccedil;&Atilde;O&nbsp;</p>\r\n<p>\r\n	Primeiro ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Mantenha o pH da &aacute;gua sempre entre 7,2 e 7,6.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 4 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio de um Kit de Testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s 3 ou 4 semanas, o cloro residual medido ao amanhecer dever&aacute; ser superior a 1ppm revelando a estabiliza&ccedil;&atilde;o da &aacute;gua, sendo poss&iacute;vel ent&atilde;o, a redu&ccedil;&atilde;o da clora&ccedil;&atilde;o para aproximadamente 1,5 gramas por metro c&uacute;bico de &aacute;gua, conforme descrito em: Dosagem de Rotina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE ROTINA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 1,5 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina, ou se preferir , distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual no dia seguinte com o aux&iacute;lio do KIT DE TESTES e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE CHOQUE&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s longos per&iacute;odos sem clora&ccedil;&atilde;o ou quando a &aacute;gua estiver colorida, turva ou com odor desagrad&aacute;vel, ajuste o pH entre 7,2 e 7,6 e adicione 15 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua. Repita a opera&ccedil;&atilde;o se necess&aacute;rio.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor residual de cloro for inferior a 3 ppm. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan-plus-10kg', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(83, 'HIDROSAN PLUS 2,5KG', '0206201605109790466444..jpg', '<p>\r\n	HIDROSAN PLUS O Hidrosan n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan permanecem com &aacute;gua balanceada, enquanto as tratadas com outros produtos se estragam ap&oacute;s 2 ou 3 meses de aplica&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DESINFEC&Ccedil;&Atilde;O DE &Aacute;GUA DE PISCINAS&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por ser altamente sol&uacute;vel e possuir o pH neutro, Hidrosan &eacute; indicado para o tratamento de qualquer tipo de piscina.&nbsp;</p>\r\n<p>\r\n	O Hidrosan cont&eacute;m estabilizador de cloro que impede a decomposi&ccedil;&atilde;o do cloro residual da &aacute;gua pelo sol. A estabiliza&ccedil;&atilde;o do cloro ocorre ap&oacute;s 3 ou 4 semanas, quando o estabilizador contido no Hidrosan atinge a concentra&ccedil;&atilde;o de 25 a 30 ppm na &aacute;gua da piscina. Ao atingir esta concentra&ccedil;&atilde;o de estabilizador na &aacute;gua, o consumo de cloro se reduzir&aacute; &agrave; metade da dosagem inicial, ou menos, dependendo de fatores como: freq&uuml;&ecirc;ncia, grau de contamina&ccedil;&atilde;o, temperatura da &aacute;gua, etc.&nbsp;</p>\r\n<p>\r\n	O uso de Hidrosan mant&eacute;m o teor residual de cloro estabilizado entre 1 e 3 ppm durante todo o dia, protegendo a &aacute;gua de sua piscina, o que n&atilde;o ocorre com produtos sem estabilizantes.Hidrosan proporciona mais seguran&ccedil;a e mais economia.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM INICIAL PARA ESTABILIZA&Ccedil;&Atilde;O&nbsp;</p>\r\n<p>\r\n	Primeiro ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Mantenha o pH da &aacute;gua sempre entre 7,2 e 7,6.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 4 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio de um Kit de Testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s 3 ou 4 semanas, o cloro residual medido ao amanhecer dever&aacute; ser superior a 1ppm revelando a estabiliza&ccedil;&atilde;o da &aacute;gua, sendo poss&iacute;vel ent&atilde;o, a redu&ccedil;&atilde;o da clora&ccedil;&atilde;o para aproximadamente 1,5 gramas por metro c&uacute;bico de &aacute;gua, conforme descrito em: Dosagem de Rotina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE ROTINA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 1,5 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina, ou se preferir , distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual no dia seguinte com o aux&iacute;lio do KIT DE TESTES e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM DE CHOQUE&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s longos per&iacute;odos sem clora&ccedil;&atilde;o ou quando a &aacute;gua estiver colorida, turva ou com odor desagrad&aacute;vel, ajuste o pH entre 7,2 e 7,6 e adicione 15 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua. Repita a opera&ccedil;&atilde;o se necess&aacute;rio.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor residual de cloro for inferior a 3 ppm. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan-plus-25kg', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(84, 'HIDROSAN PLUS 1KG', '0206201605123006172320..jpg', '<p>\r\n	HIDROSAN PLUS O Hidrosan n&atilde;o reduz a Alcalinidade Total da &aacute;gua nem o pH, por isso, piscinas tratadas com Hidrosan permanecem com &aacute;gua balanceada, enquanto as tratadas com outros produtos se estragam ap&oacute;s 2 ou 3 meses de aplica&ccedil;&atilde;o.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DESINFEC&Ccedil;&Atilde;O DE &Aacute;GUA DE PISCINAS&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Por ser altamente sol&uacute;vel e possuir o pH neutro, Hidrosan &eacute; indicado para o tratamento de qualquer tipo de piscina.&nbsp;</p>\r\n<p>\r\n	O Hidrosan cont&eacute;m estabilizador de cloro que impede a decomposi&ccedil;&atilde;o do cloro residual da &aacute;gua pelo sol. A estabiliza&ccedil;&atilde;o do cloro ocorre ap&oacute;s 3 ou 4 semanas, quando o estabilizador contido no Hidrosan atinge a concentra&ccedil;&atilde;o de 25 a 30 ppm na &aacute;gua da piscina. Ao atingir esta concentra&ccedil;&atilde;o de estabilizador na &aacute;gua, o consumo de cloro se reduzir&aacute; &agrave; metade da dosagem inicial, ou menos, dependendo de fatores como: freq&uuml;&ecirc;ncia, grau de contamina&ccedil;&atilde;o, temperatura da &aacute;gua, etc.&nbsp;</p>\r\n<p>\r\n	O uso de Hidrosan mant&eacute;m o teor residual de cloro estabilizado entre 1 e 3 ppm durante todo o dia, protegendo a &aacute;gua de sua piscina, o que n&atilde;o ocorre com produtos sem estabilizantes.Hidrosan proporciona mais seguran&ccedil;a e mais economia.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	MODO DE USAR&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	DOSAGEM INICIAL PARA ESTABILIZA&Ccedil;&Atilde;O&nbsp;</p>\r\n<p>\r\n	Primeiro ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Mantenha o pH da &aacute;gua sempre entre 7,2 e 7,6.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 4 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina. Se preferir, distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual ao amanhecer com aux&iacute;lio de um Kit de Testes e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s 3 ou 4 semanas, o cloro residual medido ao amanhecer dever&aacute; ser superior a 1ppm revelando a estabiliza&ccedil;&atilde;o da &aacute;gua, sendo poss&iacute;vel ent&atilde;o, a redu&ccedil;&atilde;o da clora&ccedil;&atilde;o para aproximadamente 1,5 gramas por metro c&uacute;bico de &aacute;gua, conforme descrito em: Dosagem de Rotina.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp; DOSAGEM DE ROTINA&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Adicione diariamente 1,5 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua sempre ao entardecer.&nbsp;</p>\r\n<p>\r\n	Dissolva Hidrosan em um recipiente pl&aacute;stico limpo com &aacute;gua e espalhe a solu&ccedil;&atilde;o sobre a superf&iacute;cie da piscina, ou se preferir , distribua os gr&acirc;nulos diretamente sobre a superf&iacute;cie da &aacute;gua.&nbsp;</p>\r\n<p>\r\n	Me&ccedil;a o cloro residual no dia seguinte com o aux&iacute;lio do KIT DE TESTES e aumente ou diminua a dosagem de forma a obter entre 1 e 3 ppm.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	&nbsp; DOSAGEM DE CHOQUE&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Ajuste a Alcalinidade Total da &aacute;gua entre 80 e 120 ppm.&nbsp;</p>\r\n<p>\r\n	Ap&oacute;s longos per&iacute;odos sem clora&ccedil;&atilde;o ou quando a &aacute;gua estiver colorida, turva ou com odor desagrad&aacute;vel, ajuste o pH entre 7,2 e 7,6 e adicione 15 gramas de Hidrosan por metro c&uacute;bico de &aacute;gua. Repita a opera&ccedil;&atilde;o se necess&aacute;rio.&nbsp;</p>\r\n<p>\r\n	S&oacute; reutilize a piscina quando o teor residual de cloro for inferior a 3 ppm. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'hidrosan-plus-1kg', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(85, 'HTG CLORO GRANULADO 10KG', '0206201605148294493324..jpg', '<p>\r\n	HTH CLORO GLANULADO &nbsp; Indicado para eliminar micro-organismos presentes na &aacute;gua da piscina, oxidar mat&eacute;rias org&acirc;nicas e dos metais nela dissolvidos. Ainda inibe os odores desagrad&aacute;veis e previne a transmiss&atilde;o de doen&ccedil;as infecciosas.</p>\r\n<p>\r\n	Dosagem: 4 g/m&sup3; (ou 14 g/m&sup3; para superclora&ccedil;&atilde;o).&nbsp;</p>\r\n<p>\r\n	Embalagem: Frasco de 10kg &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	INSTRU&Ccedil;&Otilde;ES DE USO&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	1. Dissolva o produto em um balde limpo com &aacute;gua da pr&oacute;pria piscina. Aplique 4 g de hth&reg; Cloro Granulado para cada 1000 litros de &aacute;gua. &nbsp;&nbsp;</p>\r\n<p>\r\n	2. Despeje o produto ao longo das laterais. &nbsp;&nbsp;</p>\r\n<p>\r\n	3. Filtre diariamente por um per&iacute;odo m&iacute;nimo de 6 horas. &nbsp;&nbsp;</p>\r\n<p>\r\n	4. Somente permita a entrada de pessoas na piscina 1 hora ap&oacute;s a aplica&ccedil;&atilde;o. &nbsp; &nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'htg-cloro-granulado-10kg', 79, 'HIDROALL', NULL, NULL, NULL, NULL, 19),
(86, 'POOL CLEAN UVC', '0206201605206032210033..jpg', '<p>\r\n	A luz ultravioleta C de alto poder germicida acaba at&eacute; com protozo&aacute;rios mais resistentes (como a gi&aacute;rdia) e ajuda a evitar outras doen&ccedil;as (como as frieiras, f&aacute;ceis de se pegar em banhos de piscinas). Fungos, bact&eacute;rias e algas s&atilde;o eliminados, assim como o alto consumo de cloro para tratamento. Este tipo de tratamento, com luz ultravioleta, &eacute; bastante utilizado em em processos de tratamento de &aacute;gua de ind&uacute;strias aliment&iacute;cias, cosm&eacute;ticas e farmac&ecirc;uticas.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Com Pool Clean, a redu&ccedil;&atilde;o do cloro &eacute; de quase 100% nas piscinas residenciais e de quase 75% nas p&uacute;blicas.&nbsp;</p>\r\n<p>\r\n	Al&eacute;m disso, o custo de manuten&ccedil;&atilde;o &eacute; baixo, sendo necess&aacute;ria a troca da l&acirc;mpada UVC uma vez ao ano. J&aacute; a limpeza &eacute; bastante simples, podendo ser feita a cada 3 meses em m&eacute;dia. &nbsp;&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p>\r\n	Heliossol Energia Para a Vida!</p>', '', '', '', 'SIM', 0, 'pool-clean-uvc', 79, 'POOL CLEAN', NULL, NULL, NULL, NULL, 20);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'index'),
(2, 'Empresa', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e   Elétrico.', 'descriptionA Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e   residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento   com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro   civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos   clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua   família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'empresa'),
(3, 'Serviços', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'servicos'),
(15, 'Notícias', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'noticias'),
(16, 'Fale conosco', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'fale-conosco'),
(17, 'Trabalhe conosco', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'trabalhe-conosco'),
(18, 'Produtos', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'produtos'),
(19, 'Orçamentos', 'Heliossol Aquecedor Solar Brasília - DF, Aquecedor Para Piscinas, Residencial, Aquecedor a Gás e Elétrico.', 'A Heliossol trabalha com a instalação e venda de aquecedor solar e elétrico para piscinas e residencias e é a primeira empresa de energia solar de Brasília com o verdadeiro comprometimento com o meio ambiente e a sustentabilidade. A empresa conta com engenheiro agrônomo, engenheiro civil, colaboração de ambientalistas e ecologistas. Desta forma conseguimos oferecer para nossos clientes não só preço, mas também soluções ecologicamente corretas para o dia a dia da sua família, com produtos de altíssima qualidade e eficiência e que respeitam o meio ambiente.', 'Aquecedor Solar, Aquecedor Solar Brasília DF, Aquecedor Eletrico, Aquecedor Eletrico Brasília DF, Aquecedor à Gás, Aquecedor a Gas Brasília DF, Aquecedor Piscinas, Aquecedor DF, Aquecedores Brasilia DF, Aquecedores, Aquecedor Residencial Brasilia DF, Aquecimento Solar de Piscina, Aquecimento de Piscinas Brasília DF, Aquecimento de Banheiras, Aquecedor Banheiras, Aquecimento Elétrico Para Piscinas, Aquecimento à Gás Para Piscinas, Energia Solar Brasilia DF, Placas de Energia Solar Brasilia DF, Coletor de Energia Solar Brasilia, DF, Construção de Piscinas, construao de pisicina, Venda e Instalação, Orçamento e Preço de Aquecedores Brasília DF', 'SIM', NULL, 'orcamentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`) VALUES
(45, 'CONSTRUÇÃO DE PISCINAS', '<p style="text-align: justify;">\r\n	As Piscinas Heliossol s&atilde;o adaptadas aos seus sonhos. Oferecemos m&atilde;o-de-obra especiliazada na constru&ccedil;&atilde;o de piscinas em fibra, de vinil ou em azulejo e pastilhas em resid&ecirc;ncias, academias e hot&eacute;is, sempre estar&atilde;o valorizando o seu patrim&ocirc;nio com a garantia dos melhores produtos empregados na sua constru&ccedil;&atilde;o. Oferecemos a voc&ecirc; conforto, bom gosto e qualidade utilizando alta tecnologia consagrada nos EUA, Canad&aacute; e Europa.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	A Heliossol oferece a op&ccedil;&atilde;o de piscinas em fibra que j&aacute; vem pronta e por ser desse tipo, n&atilde;o necessita de levantar paredes de alvenaria, pois seu sistema de instala&ccedil;&atilde;o &eacute; bem r&aacute;pido.</p>\r\n<p style="text-align: justify;">\r\n	As piscinas de fibra s&atilde;o de f&aacute;cil manuten&ccedil;&atilde;o.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nas constru&ccedil;&otilde;es de piscinas em vinil utilizamos a flexibilidade que somente o vinil pode oferecer, as piscinas sempre ter&atilde;o um formato, estilo ou modelo adequado ao seu gosto e or&ccedil;amento.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Nossas piscinas de azulejo e pastilhas obedecem todas as normas de constru&ccedil;&atilde;o estabelecidas pelo mercado. Apesar de ser a mais tradicional entre os tipos de piscinas, &eacute; constru&iacute;da com um sistema moderno, onde se reduz custos e aumenta a rapidez na constru&ccedil;&atilde;o de sua piscina.</p>\r\n<p style="text-align: justify;">\r\n	&nbsp;</p>\r\n<p style="text-align: justify;">\r\n	Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n<p style="text-align: justify;">\r\n	Heliossol Energia Para a Vida!</p>', '0206201611174324523823..jpg', 'SIM', NULL, 'construcao-de-piscinas', '', '', '', 0, '', '', '0206201611176686009934.png'),
(47, 'AQUECIMENTO SOLAR', '<p>\r\n	Todo projeto de energia solar come&ccedil;a com uma conversa r&aacute;pida para ver se solar &eacute; ideal para voc&ecirc;. Vamos discutir o seu uso de energia e dar uma olhada em seu telhado e sua resid&ecirc;ncia para que possamos preparar um or&ccedil;amento sem compromisso personalizado.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Uma vez que temos as medi&ccedil;&otilde;es, os nossos engenheiros especialistas ir&aacute; projetar um sistema de energia solar com base nas dimens&otilde;es da sua casa e suas necessidades de energia.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A maioria das instala&ccedil;&otilde;es de sistemas de pain&eacute;is solares levam apenas um &uacute;nico dia. Vamos encontrar um dia que funciona melhor para voc&ecirc;. Os coletores solares para piscinas podem ser instalados em edifica&ccedil;&otilde;es novas ou antigas, de prefer&ecirc;ncia, no n&iacute;vel mais elevado (telhado), ou ser adaptado em outro lugar da resid&ecirc;ncia. Caso o telhado n&atilde;o possa ser utilizado, h&aacute; possibilidade de construir uma base para a instala&ccedil;&atilde;o das placas solares em qualquer local de sua casa.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<div>\r\n	<p>\r\n		Fa&ccedil;a um or&ccedil;amento Online, estamos sempre prontos para atend&ecirc;-los.</p>\r\n	<p>\r\n		Heliossol Energia Para a Vida!</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>', '0206201605328165051005..jpg', 'SIM', NULL, 'aquecimento-solar', '', '', '', 0, '', '', '0206201605327691842909.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_subcategorias_produtos`
--

CREATE TABLE `tb_subcategorias_produtos` (
  `idsubcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `id_categoriaproduto` int(11) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_subcategorias_produtos`
--

INSERT INTO `tb_subcategorias_produtos` (`idsubcategoriaproduto`, `titulo`, `id_categoriaproduto`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'PISCINAS DE VINIL', 77, 'SIM', NULL, 'piscinas-de-vinil'),
(3, 'PISCINAS DE FIBRA', 77, 'SIM', NULL, 'piscinas-de-fibra'),
(4, 'PISCINAS EM PASTILHAS', 77, 'SIM', NULL, 'piscinas-em-pastilhas'),
(5, 'AQUECEDORES PARA PISCINAS', 75, 'SIM', NULL, 'aquecedores-para-piscinas'),
(6, 'AQUECIMENTO RESIDENCIAL', 75, 'SIM', NULL, 'aquecimento-residencial'),
(7, 'RESERVATÓRIOS', 75, 'SIM', NULL, 'reservatorios'),
(8, 'PLACAS COLETORAS', 75, 'SIM', NULL, 'placas-coletoras'),
(9, 'BOMBAS DE CALOR', 74, 'SIM', NULL, 'bombas-de-calor'),
(10, 'CONTROLADORES', 74, 'SIM', NULL, 'controladores'),
(11, 'BOMBAS DE CIRCULAÇÃO', 76, 'SIM', NULL, 'bombas-de-circulacao'),
(12, 'SISTEMA PRESSURIZADOR', 76, 'SIM', NULL, 'sistema-pressurizador'),
(13, 'FILTROS E BOMBAS', 78, 'SIM', NULL, 'filtros-e-bombas'),
(14, 'ASPIRADORES', 78, 'SIM', NULL, 'aspiradores'),
(15, 'CLORADORES', 78, 'SIM', NULL, 'cloradores'),
(16, 'ESCOVAS', 78, 'SIM', NULL, 'escovas'),
(17, 'ILUMINAÇÃO', 78, 'SIM', NULL, 'iluminacao'),
(18, 'ESCADAS', 78, 'SIM', NULL, 'escadas'),
(19, 'PRODUTOS QUÍMICOS', 79, 'SIM', NULL, 'produtos-quimicos'),
(20, 'ULTRAVIOLETA', 79, 'SIM', NULL, 'ultravioleta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  ADD PRIMARY KEY (`idcomentarioproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  ADD PRIMARY KEY (`idespecificacao`);

--
-- Indexes for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  ADD PRIMARY KEY (`idgaleriaportifolio`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  ADD PRIMARY KEY (`idgaleriaempresa`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  ADD PRIMARY KEY (`idloja`);

--
-- Indexes for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  ADD PRIMARY KEY (`idnoticia`);

--
-- Indexes for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  ADD PRIMARY KEY (`idportfolio`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- Indexes for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  ADD PRIMARY KEY (`idsubcategoriaproduto`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_comentarios_produtos`
--
ALTER TABLE `tb_comentarios_produtos`
  MODIFY `idcomentarioproduto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_especificacoes`
--
ALTER TABLE `tb_especificacoes`
  MODIFY `idespecificacao` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_galerias_portifolios`
--
ALTER TABLE `tb_galerias_portifolios`
  MODIFY `idgaleriaportifolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
--
-- AUTO_INCREMENT for table `tb_galeria_empresa`
--
ALTER TABLE `tb_galeria_empresa`
  MODIFY `idgaleriaempresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=489;
--
-- AUTO_INCREMENT for table `tb_lojas`
--
ALTER TABLE `tb_lojas`
  MODIFY `idloja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_noticias`
--
ALTER TABLE `tb_noticias`
  MODIFY `idnoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `tb_portfolios`
--
ALTER TABLE `tb_portfolios`
  MODIFY `idportfolio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `tb_subcategorias_produtos`
--
ALTER TABLE `tb_subcategorias_produtos`
  MODIFY `idsubcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
